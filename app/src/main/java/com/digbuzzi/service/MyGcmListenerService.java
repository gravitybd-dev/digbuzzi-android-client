package com.digbuzzi.service;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v7.app.NotificationCompat;
import android.util.Log;
import android.widget.RemoteViews;

import com.google.android.gms.gcm.GcmListenerService;

import com.digbuzzi.R;
import com.digbuzzi.app.AppConfig;
import com.digbuzzi.event.NotificationEvent;
import com.digbuzzi.ui.NavDrawerActivity;
import com.squareup.picasso.Picasso;

import de.greenrobot.event.EventBus;

public class MyGcmListenerService extends GcmListenerService {

    private static final String TAG = "MyGcmListenerService";
    private SharedPreferences preferences;
    private SharedPreferences sessionPreferences;
    private boolean isForeground;

    @Override
    public void onCreate() {
        super.onCreate();
        preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        preferences.registerOnSharedPreferenceChangeListener(sharedPreferenceChangeListener);
        sessionPreferences = getSharedPreferences(AppConfig.SHARED_PREF_NAME, MODE_PRIVATE);
    }

    /**
     * Called when message is received.
     *
     * @param from SenderID of the sender.
     * @param data Data bundle containing message data as key/value pairs.
     *             For Set of keys use data.keySet().
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(String from, Bundle data) {
        preferences.edit()
                .putInt(AppConfig.NOTIFICATION_COUNTER,
                        preferences.getInt(AppConfig.NOTIFICATION_COUNTER, 0) + 1).apply();

        if (sessionPreferences.getLong(AppConfig.PREF_EXPIRATION_TIME, 0) < System.currentTimeMillis()) return;

        if (isForeground) {
            EventBus.getDefault().post(new NotificationEvent(data));
            return;
        }

        String message = data.getString("text");
        Log.d(TAG, "From: " + from);
        Log.d(TAG, "Message: " + message);

        if (from.startsWith("/topics/")) {
            // message received from some topic.
        } else {
            // normal downstream message.
        }

        // [START_EXCLUDE]
        /**
         * Production applications would usually process the message here.
         * Eg: - Syncing with server.
         *     - Store message in local database.
         *     - Update UI.
         */

        /**
         * In some cases it may be useful to show a notification indicating to the user
         * that a message was received.
         */
        sendNotification(data);
        // [END_EXCLUDE]
    }
    // [END receive_message]

    /**
     * Create and show a simple notification containing the received GCM message.
     *
     * @param bundle GCM message received.
     */
    private void sendNotification(Bundle data) {
        Intent intent = new Intent(this, NavDrawerActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        String message = data.getString("text");
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher);
        NotificationCompat.Builder notificationBuilder = (NotificationCompat.Builder) new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_small_icon_notification)
                .setLargeIcon(bitmap)
                .setContentTitle("Digbuzzi")
                .setContentText(message)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        final Notification notification = notificationBuilder.build();
        final int notifId = (int) System.currentTimeMillis();

        notificationManager.notify(notifId, notification);

        final RemoteViews contentView = notification.contentView;
        final int iconPlacingId = android.R.id.icon;

        final String participantAvatar = data.getString("participentAvatar");
        new Handler(getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                Picasso.with(getApplicationContext()).load(participantAvatar).into(contentView, iconPlacingId, notifId, notification);
            }
        });
    }

    @Override
    public void onDestroy() {
        preferences.unregisterOnSharedPreferenceChangeListener(sharedPreferenceChangeListener);
        super.onDestroy();
    }

    private SharedPreferences.OnSharedPreferenceChangeListener sharedPreferenceChangeListener = new SharedPreferences.OnSharedPreferenceChangeListener() {
        @Override
        public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
            if (key.equals(AppConfig.IS_FOREGROUND)) {
                isForeground = sharedPreferences.getBoolean(AppConfig.IS_FOREGROUND, false);
            }
        }
    };
}
