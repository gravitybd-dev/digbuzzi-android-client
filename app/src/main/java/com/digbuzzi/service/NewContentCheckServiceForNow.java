package com.digbuzzi.service;

import com.google.android.gms.gcm.GcmTaskService;
import com.google.android.gms.gcm.TaskParams;

import com.digbuzzi.event.NewContentAvailableEvent;
import de.greenrobot.event.EventBus;

public class NewContentCheckServiceForNow extends GcmTaskService {

    @Override
    public int onRunTask(TaskParams taskParams) {
        EventBus.getDefault().post(new NewContentAvailableEvent());
        return 0;
    }
}
