package com.digbuzzi.service;

import android.app.IntentService;
import android.content.Intent;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by Joker on 4/4/16.
 */
public class CacheNowFeedService extends IntentService {
    private static final String TAG = CacheNowFeedService.class.getSimpleName();
    public static final String NOW_JSON = "com.digbuzzi.now_json";
    public static final String NOW_CACHE_FILE_NAME = "now_feed.json";

    public CacheNowFeedService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        String nowJsonFeed = intent.getStringExtra(NOW_JSON);
        save(nowJsonFeed);
    }

    private void save(String nowJsonFeed) {
        FileOutputStream outputStream = null;
        try {
            outputStream = openFileOutput(NOW_CACHE_FILE_NAME, MODE_PRIVATE);
            outputStream.write(nowJsonFeed.getBytes());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (outputStream != null) {
                try {
                    outputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
