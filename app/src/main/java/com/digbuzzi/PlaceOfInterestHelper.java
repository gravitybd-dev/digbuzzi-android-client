package com.digbuzzi;

import android.content.Context;

import com.digbuzzi.app.AppConfig;
import com.digbuzzi.model.PlaceOfInterestGroup;
import com.digbuzzi.model.PlaceOfInterestType;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by U on 7/20/2016.
 */

public class PlaceOfInterestHelper {
    public static String getJSONStringFromAssetsByFileName(Context context, String fileName) throws IOException {
        return IOUtils.toString(context.getAssets().open(fileName));
    }

    public static List<PlaceOfInterestGroup> deserializeListFromJSONString(Gson gson, String json) {
        return gson.fromJson(json, new TypeToken<List<PlaceOfInterestGroup>>(){}.getType());
    }

    public static String choosePinIconForMapAccordingToGroup(List<PlaceOfInterestGroup> groups, String placeType) {
        for (int i = 0, size = groups.size(); i < size; i++) {
            PlaceOfInterestGroup group = groups.get(i);
            if (group.hasPlaceType(placeType))
                return group.getMapImageName();
        }

        throw new RuntimeException("Sorry, \"" + placeType + "\"" + " is unavailable. Please, check the \"place_types.json\" in \"assets\" folder, if it is present or not.");
    }

    public static String groupTypeOfPlaceType(List<PlaceOfInterestGroup> groups, String placeType) {
        for (int i = 0, size = groups.size(); i < size; i++) {
            PlaceOfInterestGroup group = groups.get(i);
            if (group.hasPlaceType(placeType))
                return group.getPlaceGroupType();
        }

        throw new RuntimeException("Sorry, \"" + placeType + "\"" + " is unavailable. Please, check the \"place_types.json\" in \"assets\" folder, if it is present or not.");
    }

    public static List<PlaceOfInterestGroup> loadPlaceGroups(Context context) {
        try {
            String json = PlaceOfInterestHelper.getJSONStringFromAssetsByFileName(
                    context, AppConfig.PLACE_OF_INTEREST_JSON_FILE_NAME);
            return PlaceOfInterestHelper.deserializeListFromJSONString(new Gson(), json);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new ArrayList<>();
    }

    public static String getImageNameForPlaceType(List<PlaceOfInterestGroup> groups, String placeType) {
        for (int i = 0, size = groups.size(); i < size; i++) {
            List<PlaceOfInterestType> groupMembers = groups.get(i).getPlaceGroupMembers();
            for (int j = 0, memberSize = groupMembers.size(); j < memberSize; j++) {
                if (groupMembers.get(j).getPlaceType().equals(placeType))
                    return groupMembers.get(j).getImageName();
            }
        }

        throw new RuntimeException("Sorry, \"" + placeType + "\"" + " is unavailable. Please, check the \"place_types.json\" in \"assets\" folder, if it is present or not.");
    }
}
