package com.digbuzzi.Util;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Point;
import android.provider.Settings;

import com.digbuzzi.R;

/**
 * Created by Joker on 2/6/16.
 */
public class Utils {
    public static int getToolbarHeight(Context context) {
        final TypedArray styledAttributes = context.getTheme().obtainStyledAttributes(
                new int[]{R.attr.actionBarSize});
        int toolbarHeight = (int) styledAttributes.getDimension(0, 0);
        styledAttributes.recycle();

        return toolbarHeight;
    }

    public static String appId(Context context) {
        return Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    public static Point getCalculatedWidthHeight(int actualWidth, int actualHeight, int expectedWidth, int expectedHeight) {
        int bound_width = expectedWidth;
        int bound_height = expectedHeight;
        int new_width = actualWidth;
        int new_height = actualHeight;

        // first check if we need to scale width
        if (actualWidth > bound_width) {
            //scale width to fit
            new_width = bound_width;
            //scale height to maintain aspect ratio
            new_height = (new_width * actualHeight) / actualWidth;
        }

        // then check if we need to scale even with the new height
        if (new_height > bound_height) {
            //scale height to fit instead
            new_height = bound_height;
            //scale width to maintain aspect ratio
            new_width = (new_height * actualWidth) / actualHeight;
        }
        return new Point(new_width, new_height);
    }
}
