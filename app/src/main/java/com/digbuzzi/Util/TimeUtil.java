package com.digbuzzi.Util;

import android.text.format.DateUtils;

/**
 * Created by U on 1/5/2016.
 */
public class TimeUtil {
    public static CharSequence getElapsedTime(long time) {
        CharSequence timeAgo = DateUtils.getRelativeTimeSpanString(
                time, System.currentTimeMillis(), DateUtils.SECOND_IN_MILLIS);
        return timeAgo;
    }
}
