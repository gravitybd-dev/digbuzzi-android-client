package com.digbuzzi.Util;

import android.content.pm.PackageManager;

import com.digbuzzi.app.AppConstants;

/**
 * Created by anwarshahriar on 9/13/16.
 */
public class PlaceOfInterestUtil {
    public static boolean isGoogleMapInstalled(PackageManager packageManager) {
        try {
            packageManager.getPackageInfo(AppConstants.GOOGLE_MAP_APP_PACKAGE_NAME, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }
}
