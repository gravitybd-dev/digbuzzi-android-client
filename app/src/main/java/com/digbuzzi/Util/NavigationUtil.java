package com.digbuzzi.Util;

import android.content.Context;
import android.content.Intent;

import com.digbuzzi.ui.ProfileActivity;

/**
 * Created by Joker on 2/6/16.
 */
public class NavigationUtil {
    public static void openProfileActivity(Context context, String userId) {
        Intent intent = new Intent(context, ProfileActivity.class);
        intent.putExtra("userId", userId);
        context.startActivity(intent);
    }
}
