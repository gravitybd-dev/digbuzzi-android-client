package com.digbuzzi.Util;

import android.os.PatternMatcher;

import com.digbuzzi.app.AppConfig;

import java.util.regex.Pattern;

public class UserUtil {
    private static Pattern usernamePatternMatcher = Pattern.compile(AppConfig.USERNAME_REGEX, Pattern.CASE_INSENSITIVE | Pattern.UNICODE_CASE);

    public static boolean validUsername(String username) {
        boolean matchFound = usernamePatternMatcher.matcher(username).find();
        return matchFound && username.length() >= AppConfig.USERNAME_MIN_LENGTH;
    }
}
