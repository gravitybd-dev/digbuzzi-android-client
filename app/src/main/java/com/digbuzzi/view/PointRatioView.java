package com.digbuzzi.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;

import com.digbuzzi.R;

/**
 * TODO: document your custom view class.
 */
public class PointRatioView extends View {
    private int mCurrentPoint;
    private int mTotalPoint;
    private int mRemainingColor;
    private int mFilledColor;

    private Paint mRemainingPaint;
    private Paint mFilledPaint;

    public PointRatioView(Context context) {
        super(context);
        init(null, 0);
    }

    public PointRatioView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs, 0);
    }

    public PointRatioView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(attrs, defStyle);
    }

    private void init(AttributeSet attrs, int defStyle) {
        // Load attributes
        final TypedArray a = getContext().obtainStyledAttributes(
                attrs, R.styleable.PointRatioView, defStyle, 0);

        mCurrentPoint = a.getInteger(R.styleable.PointRatioView_currentPoint, 0);
        mTotalPoint = a.getInteger(R.styleable.PointRatioView_totalPoint, 100);

        mRemainingColor = a.getColor(R.styleable.PointRatioView_remainingColor, Color.WHITE);
        mFilledColor = a.getColor(R.styleable.PointRatioView_filledColor, Color.GRAY);

        a.recycle();

        mRemainingPaint = new Paint();
        mRemainingPaint.setColor(mRemainingColor);

        mFilledPaint = new Paint();
        mFilledPaint.setColor(mFilledColor);
    }

    public void setCurrentPoint(int currentPoint) {
        mCurrentPoint = currentPoint;
        invalidate();
    }

    public void setTotalPoint(int totalPoint) {
        mTotalPoint = totalPoint;
        invalidate();
    }

    public void setRemainingColor(int remainingColor) {
        mRemainingColor = remainingColor;
        invalidate();
    }

    public void setFilledColor(int filledColor) {
        mFilledColor = filledColor;
        invalidate();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        int filledPercent = (mCurrentPoint * 100) / mTotalPoint;
        float filledWidth = (filledPercent / 100f) * getMeasuredWidth();
        Rect filledRect = new Rect(0, 0, (int) filledWidth, getMeasuredHeight());
        Rect remainingRect = new Rect((int) filledWidth, 0, getRight(), getMeasuredHeight());

        canvas.drawRect(filledRect, mFilledPaint);
        canvas.drawRect(remainingRect, mRemainingPaint);
    }
}
