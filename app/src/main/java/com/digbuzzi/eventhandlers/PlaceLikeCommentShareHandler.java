package com.digbuzzi.eventhandlers;

import com.digbuzzi.model.restmodel.PlaceOfInterest;

/**
 * Created by anwarshahriar on 9/9/16.
 */
public class PlaceLikeCommentShareHandler {
    public void likePressed(PlaceOfInterest placeOfInterest) {}
    public void likeCountPressed(PlaceOfInterest placeOfInterest) {}
    public void commentPressed(PlaceOfInterest placeOfInterest) {}
    public void sharePressed(PlaceOfInterest placeOfInterest) {}
}
