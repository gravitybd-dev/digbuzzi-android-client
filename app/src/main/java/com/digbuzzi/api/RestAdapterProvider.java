package com.digbuzzi.api;

import retrofit.RestAdapter;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Joker on 1/2/16.
 */
public class RestAdapterProvider {
    private static final RestAdapterProvider instance = new RestAdapterProvider();

    private final RestApi restApiForRetrofit1 = new RestAdapter.Builder()
                                                        .setEndpoint(ApiConfig.BASE_URL)
                                                        .build()
                                                        .create(RestApi.class);

    private final RestApi restApiForRetrofit2 = new Retrofit.Builder()
                                                        .baseUrl(ApiConfig.BASE_URL)
                                                        .addConverterFactory(GsonConverterFactory.create())
                                                        .build()
                                                        .create(RestApi.class);

    private RestAdapterProvider() {}

    public static RestAdapterProvider getProvider() {
        return instance;
    }

    public RestApi getRestApiForRetrofit1() {
        return restApiForRetrofit1;
    }
    public RestApi getRestApiForRetrofit2() {
        return restApiForRetrofit2;
    }
}
