package com.digbuzzi.api;



import java.util.List;

import com.digbuzzi.model.PlaceOfInterestType;
import com.digbuzzi.model.restmodel.AuthResponse;
import com.digbuzzi.model.restmodel.Comment;
import com.digbuzzi.model.restmodel.DeviceRegisterUnregisterRequest;
import com.digbuzzi.model.restmodel.FollowPersonModel;
import com.digbuzzi.model.restmodel.FollowUnfollowRequest;
import com.digbuzzi.model.restmodel.FollowUnfollowResponse;
import com.digbuzzi.model.restmodel.ForgetPasswordResponse;
import com.digbuzzi.model.restmodel.GenericResponse;
import com.digbuzzi.model.restmodel.Heat;
import com.digbuzzi.model.restmodel.Leader;
import com.digbuzzi.model.restmodel.LeaderBoard;
import com.digbuzzi.model.restmodel.Like;
import com.digbuzzi.model.restmodel.LogInRequest;
import com.digbuzzi.model.restmodel.Notification;
import com.digbuzzi.model.restmodel.PlaceOfInterest;
import com.digbuzzi.model.restmodel.Profile;
import com.digbuzzi.model.restmodel.RegisterRequest;
import com.digbuzzi.model.restmodel.RequestAddComment;
import com.digbuzzi.model.restmodel.LikeDislikeRequest;
import com.digbuzzi.model.restmodel.ShoutFeed;
import com.digbuzzi.model.restmodel.Location;
import com.digbuzzi.model.restmodel.UpdatedUser;
import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.HEAD;
import retrofit.http.Header;
import retrofit.http.Headers;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Part;
import retrofit.http.Path;
import retrofit.http.Query;
import retrofit.mime.TypedFile;
import retrofit2.Call;

/**
 * Created by Joker on 1/2/16.
 */
public interface RestApi {


    @POST("/user/register")
    @Headers({
        "version-name:aunthazel",
        "version-number:v1"
    })
    void register(@Body RegisterRequest registerRequest, Callback<AuthResponse> responseCallback);

    @POST("/user/login")
    @Headers({
            "version-name:aunthazel",
            "version-number:v1"
    })
    void login(@Body LogInRequest logInRequest, Callback<AuthResponse> responseCallback);

    @Multipart
    @POST("/shouts/add")
    @Headers({
            "version-name:aunthazel",
            "version-number:v1"
    })
    void addShout(
            @Header("Authorization") String ApiToken,
            @Part("attachments") TypedFile file,
            @Part("shoutText") String ShoutText,
            @Part("location") Location location,
            @Part("time") Long Times,
            @Part("trafficCondition") String TrafficCondition,
            Callback<ShoutFeed> callback);

    @Multipart
    @POST("/places/add")
    @Headers({
            "version-name:aunthazel",
            "version-number:v1"
    })
    void addPlace(
            @Header("Authorization") String ApiToken,
            @Part("placeTitle") String placeTitle,
            @Part("placeDescription") String placeDescription,
            @Part("placeTypes") String placeTypes,
            @Part("attachments") TypedFile file,
            @Part("location") Location location,
            Callback<PlaceOfInterest> callback);

    @GET("/shouts/get")
    @Headers({
            "version-name:aunthazel",
            "version-number:v1"
    })
    void allShouts(@Header("Authorization") String token, @Query("offset") int offset, @Query("count") int count, Callback<List<ShoutFeed>> callback);

    @GET("/shouts/shout")
    @Headers({
            "version-name:aunthazel",
            "version-number:v1"
    })
    void singleBuzz(@Header("Authorization") String token, @Query("id") String buzzId, Callback<ShoutFeed> callback);

    @GET("/shouts/getsharedshout")
    @Headers({
            "version-name:aunthazel",
            "version-number:v1"
    })
    void singleSharedBuzz(@Header("Authorization") String token, @Query("sharedLink") String sharedParam, Callback<ShoutFeed> callback);

    @GET("/shouts/getusershouts")
    @Headers({
            "version-name:aunthazel",
            "version-number:v1"
    })
    void getUserShouts(@Header("Authorization") String token, @Query("userId") String userId, @Query("offset") int offset, @Query("count") int count, Callback<List<ShoutFeed>> callback);

    @GET("/shouts/getfollowershouts")
    @Headers({
            "version-name:aunthazel",
            "version-number:v1"
    })
    void followerShouts(@Header("Authorization") String token, @Query("offset") int offset, @Query("count") int count, Callback<List<ShoutFeed>> callback);

    @GET("/user/followers")
    @Headers({
            "version-name:aunthazel",
            "version-number:v1"
    })
    void followers(@Header("Authorization") String token, @Query("userId") String userId, @Query("offset") int offset, @Query("count") int count, Callback<List<FollowPersonModel>> callback);

    @GET("/user/followees")
    @Headers({
            "version-name:aunthazel",
            "version-number:v1"
    })
    void following(@Header("Authorization") String token, @Query("userId") String userId, @Query("offset") int offset, @Query("count") int count, Callback<List<FollowPersonModel>> callback);

    @POST("/shout/{id}/likes/addorremove")
    @Headers({
            "version-name:aunthazel",
            "version-number:v1"
    })
    void likeDislike(@Header("Authorization") String token, @Path("id") String id, @Body LikeDislikeRequest requestLike, Callback<Like> callback);

    @POST("/place/{id}/likes/addorremove")
    @Headers({
            "version-name:aunthazel",
            "version-number:v1"
    })
    void likeDislikePlace(@Header("Authorization") String token, @Path("id") String id, @Body LikeDislikeRequest requestLike, Callback<Like> callback);

    @GET("/shout/{shoutId}/likes")
    @Headers({
            "version-name:aunthazel",
            "version-number:v1"
    })
    void likes(@Header("Authorization") String token, @Path("shoutId") String id, Callback<List<Like>> callback);

    @GET("/place/{id}/likes")
    @Headers({
            "version-name:aunthazel",
            "version-number:v1"
    })
    void placeLikes(@Header("Authorization") String token, @Path("id") String id, Callback<List<Like>> callback);

    @GET("/shout/{id}/comments")
    @Headers({
            "version-name:aunthazel",
            "version-number:v1"
    })
    void comments(@Header("Authorization") String token, @Path("id") String id, @Query("skip") int offset, @Query("limit") int count, Callback<List<Comment>> callback);

    @GET("/place/{id}/comments")
    @Headers({
            "version-name:aunthazel",
            "version-number:v1"
    })
    void placeComments(@Header("Authorization") String token, @Path("id") String id, @Query("skip") int offset, @Query("limit") int count, Callback<List<Comment>> callback);

    @POST("/shout/{id}/comments/add")
    @Headers({
            "version-name:aunthazel",
            "version-number:v1"
    })
    void addComment(@Header("Authorization") String token, @Path("id") String id, @Body RequestAddComment comment, Callback<Comment> callback);

    @POST("/place/{id}/comments/add")
    @Headers({
            "version-name:aunthazel",
            "version-number:v1"
    })
    void addCommentForPlace(@Header("Authorization") String token, @Path("id") String id, @Body RequestAddComment comment, Callback<Comment> callback);

    @GET("/map/get")
    @Headers({
            "version-name:aunthazel",
            "version-number:v1"
    })
    void getHeatData(@Query("lat") double lat, @Query("lon") double lon, @Query("rad") double radius, Callback<List<Heat>> callback);

    @GET("/places/nearbyplaces")
    @Headers({
            "version-name:aunthazel",
            "version-number:v1"
    })
    void getPlacesOfInterest(@Header("Authorization") String token, @Query("lat") double lat, @Query("lon") double lon, @Query("rad") double radius, Callback<List<PlaceOfInterest>> callback);

    @GET("/places/place")
    @Headers({
            "version-name:aunthazel",
            "version-number:v1"
    })
    void getPlaceOfInterestById(@Header("Authorization") String token, @Query("id") String placeId, Callback<PlaceOfInterest> callback);


    @GET("/user")
    @Headers({
            "version-name:aunthazel",
            "version-number:v1"
    })
    void getUserData(@Header("Authorization") String token, @Query("userId") String id, Callback<Profile> callback);

    @POST("/user/follow")
    @Headers({
            "version-name:aunthazel",
            "version-number:v1"
    })
    void follow(@Header("Authorization") String token, @Body FollowUnfollowRequest request, Callback<FollowUnfollowResponse> callback);

    @POST("/user/unfollow")
    @Headers({
            "version-name:aunthazel",
            "version-number:v1"
    })
    void unfollow(@Header("Authorization") String token, @Body FollowUnfollowRequest request, Callback<FollowUnfollowResponse> callback);

    @Multipart
    @PUT("/user/update")
    @Headers({
            "version-name:aunthazel",
            "version-number:v1"
    })
    void updateUser(

            @Header("Authorization") String ApiToken,
            @Part("attachments") TypedFile file,
            @Part("name") String name,
            @Part("password") String userPasswordNew,
            @Part("oldPassword") String currentPassword,
            @Part("email") String userEmail,
            @Part("bio") String bio,
            Callback<UpdatedUser> callback);

    @GET("/user/renewpassword")
    @Headers({
            "version-name:aunthazel",
            "version-number:v1"
    })
    void forgetPass(@Query("userEmail") String userEmail, Callback<ForgetPasswordResponse> callback);

    @GET("/shouts/nearbybuzz")
    @Headers({
            "version-name:aunthazel",
            "version-number:v1"
    })
    void nearbyBuzz(@Header("Authorization") String token, @Query("lat") double lat, @Query("lon") double lon, @Query("rad") double rad, Callback<List<ShoutFeed>> callback);

    @GET("/user/leaderboard")
    @Headers({
            "version-name:aunthazel",
            "version-number:v1"
    })
    void leaderboard(@Header("Authorization") String token, Callback<LeaderBoard> callback);

    @GET("/user/leaders/get")
    @Headers({
            "version-name:aunthazel",
            "version-number:v1"
    })
    void leaders(@Header("Authorization") String token, @Query("offset") int offset, @Query("count") int count, Callback<List<Leader>> callback);

    @GET("/user/getnotification")
    @Headers({
            "version-name:aunthazel",
            "version-number:v1"
    })
    void userNotification(@Header("Authorization") String token, @Query("offset") int offset, @Query("count") int count, Callback<List<Notification>> callback);

    @retrofit2.http.POST("device/register")
    @retrofit2.http.Headers({
            "version-name:aunthazel",
            "version-number:v1"
    })
    Call<GenericResponse> registerDevice(@retrofit2.http.Header("Authorization") String token, @retrofit2.http.Body DeviceRegisterUnregisterRequest request);

    @retrofit2.http.POST("device/remove")
    @retrofit2.http.Headers({
            "version-name:aunthazel",
            "version-number:v1"
    })
    Call<GenericResponse> removeDevice(@retrofit2.http.Header("Authorization") String token, @retrofit2.http.Body DeviceRegisterUnregisterRequest request);
}
