package com.digbuzzi.repo;

import com.digbuzzi.model.restmodel.ShoutFeed;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by anwarshahriar on 6/3/2016.
 * Repository to store the buzzes used around the entire application
 */

public class BuzzRepository {
    /**
     * Static instance to hold the reference of this repository
     */
    private static BuzzRepository instance;

    /**
     * Map that sores unique buzz against the buzzId
     * Map<BuzzId, Buzz>
     */
    private Map<String, TaggableBuzz> buzzFeeds;

    /**
     * private constructor to conform to Singleton pattern
     */
    private BuzzRepository() {
        buzzFeeds = new HashMap<>();
    }

    /**
     * Method for getting instance of this repository
     * @return - A single instance of this repository for the entire lifespan
     */
    public static BuzzRepository getInstance() {
        if (instance == null) {
            synchronized (BuzzRepository.class) {
                if (instance == null) {
                    instance = new BuzzRepository();
                }
            }
        }

        return instance;
    }

    /**
     * Save a single buzz to the repository
     * @param tag - A string to tag a buzz to identify it for later
     * @param buzz - The buzz needs to be saved
     */
    public void saveBuzz(String tag, ShoutFeed buzz) {
        String buzzId = buzz.getShoutId();
        TaggableBuzz existingBuzz = buzzFeeds.get(buzzId);
        if (existingBuzz != null) {
            existingBuzz.addTag(tag);
            existingBuzz.setFeed(buzz);
        } else {
            TaggableBuzz taggableBuzz = new TaggableBuzz(buzz);
            taggableBuzz.addTag(tag);
            buzzFeeds.put(buzzId, taggableBuzz);
        }
    }

    /**
     * Save a list of buzz to the repository
     * @param tag - A string to tag a buzz to identify it for later
     * @param buzzes - List of buzzes to be saved
     */
    public void saveBuzzes(String tag, List<ShoutFeed> buzzes) {
        for (ShoutFeed buzz : buzzes) {
            saveBuzz(tag, buzz);
        }
    }

    public ShoutFeed getBuzz(String buzzId) {
        TaggableBuzz buzz = buzzFeeds.get(buzzId);
        return buzz == null ? null : buzz.getFeed();
    }

    @SuppressWarnings("WeakerAccess")
    public void removeBuzz(String tag, String buzzId) {
        TaggableBuzz existingBuzz = buzzFeeds.get(buzzId);
        if (existingBuzz != null && existingBuzz.hasTag(tag)) {
            existingBuzz.removeTag(tag);
        }
    }

    public void removeBuzzes(String tag) {
        for (String buzzId : buzzFeeds.keySet()) {
            removeBuzz(tag, buzzId);
        }
    }

    public List<ShoutFeed> getBuzzes(String tag) {
        List<ShoutFeed> feeds = new ArrayList<>();
        for (String buzzId : buzzFeeds.keySet()) {
            TaggableBuzz buzz = buzzFeeds.get(buzzId);
            if (buzz.hasTag(tag)) {
                feeds.add(buzz);
            }
        }

        Collections.sort(feeds, new TimeStampDescendingComparator());
        return feeds;
    }

    @SuppressWarnings("unused")
    public List<ShoutFeed> getFirst(String tag, int itemCount) {
        List<ShoutFeed> feeds = getBuzzes(tag);
        int endIndex = itemCount;
        endIndex = endIndex > feeds.size() ? feeds.size() : endIndex;
        return feeds.subList(0, endIndex);
    }

    @SuppressWarnings("unused")
    public List<ShoutFeed> getLast(String tag, int itemCount) {
        List<ShoutFeed> feeds = getBuzzes(tag);
        int startIndex = feeds.size() - itemCount;
        startIndex = startIndex < 0 ? 0 : startIndex;
        return feeds.subList(startIndex, feeds.size());
    }

    private static class TimeStampDescendingComparator implements Comparator<ShoutFeed> {

        @Override
        public int compare(ShoutFeed lhs, ShoutFeed rhs) {
            long lhsTimestamp = lhs.getTime();
            long rhsTimestamp = rhs.getTime();
            if (lhsTimestamp > rhsTimestamp) {
                return -1;
            } else if (lhsTimestamp < rhsTimestamp) {
                return 1;
            } else {
                return 0;
            }
        }
    }

    public void clear() {
        buzzFeeds.clear();
    }
}