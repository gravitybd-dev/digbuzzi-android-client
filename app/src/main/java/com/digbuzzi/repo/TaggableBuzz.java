package com.digbuzzi.repo;

import com.digbuzzi.model.restmodel.Comment;
import com.digbuzzi.model.restmodel.Location;
import com.digbuzzi.model.restmodel.ShoutFeed;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by anwarshahriar on 6/3/2016.
 * <p>This class is a wrapper around actual buzz
 * and add extra tag feature to the buzz</p>
 */

class TaggableBuzz extends ShoutFeed {
    private ShoutFeed mFeed;
    private Set<String> mTags;

    TaggableBuzz(ShoutFeed feed) {
        mFeed = feed;
        mTags = new HashSet<>(4);
    }

    void setFeed(ShoutFeed feed) {
        mFeed = feed;
    }

    ShoutFeed getFeed() {
        return mFeed;
    }

    void addTag(String tag) {
        mTags.add(tag);
    }

    void removeTag(String tag) {
        mTags.remove(tag);
    }

    boolean hasTag(String tag) {
        return mTags.contains(tag);
    }

    @Override
    public void setUserName(String userName) {
        mFeed.setUserName(userName);
    }

    @Override
    public String getUserName() {
        return mFeed.getUserName();
    }

    @Override
    public void setCommentCount(int commentCount) {
        mFeed.setCommentCount(commentCount);
    }

    @Override
    public int getCommentCount() {
        return mFeed.getCommentCount();
    }

    @Override
    public void setLikeCount(int likeCount) {
        mFeed.setLikeCount(likeCount);
    }

    @Override
    public int getLikeCount() {
        return mFeed.getLikeCount();
    }

    @Override
    public void setLikedByUser(boolean likedByUser) {
        mFeed.setLikedByUser(likedByUser);
    }

    @Override
    public boolean isLikedByUser() {
        return mFeed.isLikedByUser();
    }

    @Override
    public void setSharableLink(String sharableLink) {
        mFeed.setSharableLink(sharableLink);
    }

    @Override
    public String getSharableLink() {
        return mFeed.getSharableLink();
    }

    @Override
    public void setShareCount(int shareCount) {
        mFeed.setShareCount(shareCount);
    }

    @Override
    public int getShareCount() {
        return mFeed.getShareCount();
    }

    @Override
    public String getName() {
        return mFeed.getName();
    }

    @Override
    public List<Comment> getComments() {
        return mFeed.getComments();
    }

    @Override
    public List<String> getAttachments() {
        return mFeed.getAttachments();
    }

    @Override
    public Location getLocation() {
        return mFeed.getLocation();
    }

    @Override
    public long getTime() {
        return mFeed.getTime();
    }

    @Override
    public String getPhoto() {
        return mFeed.getPhoto();
    }

    @Override
    public String getShoutId() {
        return mFeed.getShoutId();
    }

    @Override
    public String getShoutText() {
        return mFeed.getShoutText();
    }

    @Override
    public String getTrafficCondition() {
        return mFeed.getTrafficCondition();
    }

    @Override
    public String getUserId() {
        return mFeed.getUserId();
    }
}