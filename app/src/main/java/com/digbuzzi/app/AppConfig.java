package com.digbuzzi.app;

/**
 * Created by Joker on 1/1/16.
 */
public class AppConfig {
    public static final String SHARED_PREF_NAME = "com.digbuzzi.APP_PREF";
    public static final int FEED_COUNT_PER_REQUEST = 5;
    public static final int NOTIFICATION_COUNT_PER_REQUEST = 10;
    public static final int VISIBLE_THRESHOLD = 3;
    public static final int LEADER_COUNT_PER_REQUEST = 10;
    public static final long NEW_CONTENT_PULLING_PERIOD = 10L;
    public static final double DEFAULT_SEARCH_RADIUS = 3000;

    //preferences
    public static final String SENT_TOKEN_TO_SERVER = "sentTokenToServer";
    public static final String REGISTRATION_COMPLETE = "registrationComplete";

    public static final String IS_FOREGROUND = "is_digbuzzi_foreground";
    public static final String NOTIFICATION_COUNTER = "digbuzzi_notification_counter";

    public static final String PREF_EXPIRATION_TIME = "com.digbuzzi.expiration_time";
    public static final long EXPIRATION_THRESHOLD = 2 * 24 * 60 * 60 * 1000;
    public static final long APP_UNUSED_PERIOD_TO_NOTIFY_USER = 24 * 60 * 60 * 1000L;

    // Image Dimension
    public static final int DIMENSION_WIDTH = 940;
    public static final int DIMENSION_HEIGHT = 788;

    public static final int PROFILE_PHOTO_DIMENSION_WIDTH = 256;
    public static final int PROFILE_PHOTO_DIMENSION_HEIGHT = 256;

    public static final int IMAGE_QUALITY = 90;

    // ------- Map defaut configurations -----------------------------------------------------------
    public static final float DEFAULT_MAP_ZOOM_LEVEL = 12f;

    // ------- Map preference keys -----------------------------------------------------------------
    public static final String LAST_LOCATION_LATITUDE = "com.digbuzzi.last_location_latitude";
    public static final String LAST_LOCATION_LONGITUDE = "com.digbuzzi.last_location_longitude";
    public static final String LAST_LOCATION_ZOOM_LEVEL = "com.digbuzzi.last_location_zoom_level";
    public static final String POINT_REWARD_CONFIRMATION_FOR_FOLLOW_OTHER_USER_ENABLED = "com.digbuzzi.point_reward_confirmation_for_follow_other_user_enabled";
    public static final String POINT_REWARD_CONFIRMATION_FOR_POSTING_BUZZ_ENABLED = "com.digbuzzi.point_reward_confirmation_for_posting_buzz_enabled";
    public static final String POINT_REWARD_CONFIRMATION_FOR_PLACE_POSTED_ENABLED = "com.digbuzzi.point_reward_confirmation_for_place_posted_enabled";

    // ------- Constants ---------------------------------------------------------------------------
    public static final String PLACE_OF_INTEREST_JSON_FILE_NAME = "place_types.json";

    public static final String USERNAME_REGEX = "^[a-z]([.]?[a-z0-9]+)+$";
    public static final int USERNAME_MIN_LENGTH = 6;
}