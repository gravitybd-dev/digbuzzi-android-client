package com.digbuzzi.app;

import android.app.Application;
import android.content.SharedPreferences;
import android.renderscript.Double2;
import android.support.multidex.MultiDex;

import com.digbuzzi.model.MapCondition;
import com.facebook.drawee.backends.pipeline.Fresco;

import com.digbuzzi.model.User;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.StreetViewPanoramaCamera;

/**
 * Created by Joker on 1/1/16.
 */
public class DigbuzziApp extends Application {
    SharedPreferences preferences;

    @Override
    public void onCreate() {
        super.onCreate();
        Fresco.initialize(getApplicationContext());
        MultiDex.install(this);
        preferences = getSharedPreferences(AppConfig.SHARED_PREF_NAME, MODE_PRIVATE);
        User.getInstance().prepare(preferences);

    }

    public void saveLastMapCondition(MapCondition mapCondition) {
        if (mapCondition.isUsableForMap()) {
            preferences.edit()
                    .putString(AppConfig.LAST_LOCATION_LATITUDE, String.valueOf(mapCondition.getLastLookedPosition().latitude))
                    .putString(AppConfig.LAST_LOCATION_LONGITUDE, String.valueOf(mapCondition.getLastLookedPosition().longitude))
                    .putFloat(AppConfig.LAST_LOCATION_ZOOM_LEVEL, mapCondition.getLastLookedZoomLevel())
                    .apply();
        }
    }

    public MapCondition getLastMapCondition() {
        double lastLatitude = Double.parseDouble(preferences.getString(AppConfig.LAST_LOCATION_LATITUDE, "0"));
        double lastLongitude = Double.parseDouble(preferences.getString(AppConfig.LAST_LOCATION_LONGITUDE, "0"));
        float lastZoomLevel = preferences.getFloat(AppConfig.LAST_LOCATION_ZOOM_LEVEL, 0);

        if (lastLatitude != 0 && lastLongitude != 0 && lastZoomLevel != 0) {
            return new MapCondition(new LatLng(lastLatitude, lastLongitude), lastZoomLevel);
        }

        return null;
    }

    public boolean isPointRewardConfirmationForFollowOtherUserEnabled() {
        return preferences.getBoolean(AppConfig.POINT_REWARD_CONFIRMATION_FOR_FOLLOW_OTHER_USER_ENABLED, true);
    }

    public boolean isPointRewardConfirmationForPostingBuzzEnabled() {
        return preferences.getBoolean(AppConfig.POINT_REWARD_CONFIRMATION_FOR_POSTING_BUZZ_ENABLED, true);
    }

    public boolean isPointRewardConfirmationForPlacePostedEnabled() {
        return preferences.getBoolean(AppConfig.POINT_REWARD_CONFIRMATION_FOR_PLACE_POSTED_ENABLED, true);
    }

    public void setPointRewardConfirmationForFollowOtherUserEnabled(boolean enable) {
        preferences.edit().putBoolean(AppConfig.POINT_REWARD_CONFIRMATION_FOR_FOLLOW_OTHER_USER_ENABLED, enable).apply();
    }

    public void setPointRewardConfirmationForPostingBuzzEnabled(boolean enable) {
        preferences.edit().putBoolean(AppConfig.POINT_REWARD_CONFIRMATION_FOR_POSTING_BUZZ_ENABLED, enable).apply();
    }

    public void setPointRewardConfirmationForPlacePostedEnabled(boolean enable) {
        preferences.edit().putBoolean(AppConfig.POINT_REWARD_CONFIRMATION_FOR_PLACE_POSTED_ENABLED, enable).apply();
    }

    public void resetUserPreferences() {
        preferences.edit().clear().apply();
    }
}