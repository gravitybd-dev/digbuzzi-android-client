package com.digbuzzi;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.digbuzzi.model.User;
import com.digbuzzi.ui.NavDrawerActivity;
import com.digbuzzi.ui.SignUpLogInActivity;
import com.google.firebase.crash.FirebaseCrash;

public class PrimaryNavigatorActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);

        Intent intent;

        if (User.getInstance().getAccessToken().isEmpty()) {
            intent = new Intent(this, SignUpLogInActivity.class);
        } else {
            intent = new Intent(this, NavDrawerActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        }
        startActivity(intent);
        finish();
    }
}
