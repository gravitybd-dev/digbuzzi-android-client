package com.digbuzzi.ui;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.List;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.digbuzzi.R;
import com.digbuzzi.adapter.FollowerFollowingAdapter;
import com.digbuzzi.api.RestAdapterProvider;
import com.digbuzzi.model.User;
import com.digbuzzi.model.restmodel.FollowPersonModel;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

@SuppressWarnings("WeakerAccess")
public class FollowerFollowingActivity extends BaseNotificationEnabledActivity {

    public static final String KEY_TITLE = "title";
    public static final String KEY_USERID = "user_id";
    public static final String KEY_FOLLOWING_LIST = "isFollowingList";

    @Bind(R.id.toolbar)
    Toolbar toolbar;

    @Bind(R.id.rv)
    RecyclerView rv;

    @Bind(R.id.progressBar)
    ProgressBar progressBar;

    @Bind(R.id.statusContainer)
    View statusContainer;

    @Bind(R.id.tvStatus)
    TextView tvStatus;

    @Bind(R.id.btnRetry)
    Button btnRetry;

    List<FollowPersonModel> likes;
    FollowerFollowingAdapter adapter;

    String userId;
    boolean isFollowingList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_follower_following);
        ButterKnife.bind(this);

        toolbar.setTitle(getIntent().getStringExtra(KEY_TITLE));
        setSupportActionBar(toolbar);

        userId = getIntent().getStringExtra(KEY_USERID);
        isFollowingList = getIntent().getBooleanExtra(KEY_FOLLOWING_LIST, false);

        likes = new ArrayList<>();

        LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rv.setLayoutManager(layoutManager);

        adapter = new FollowerFollowingAdapter(this, likes);
        rv.setAdapter(adapter);

        loadLikes();
    }

    @Override
    protected void onNotification(Bundle data) {

    }

    @Override
    protected void onNotificationCounter(int counter) {

    }

    private void loadLikes() {
        statusContainer.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);

        if (isFollowingList) {
            loadFollowings();
        } else {
            loadFollowers();
        }
    }

    private void loadFollowers() {
        RestAdapterProvider.getProvider().getRestApiForRetrofit1().followers(User.getInstance().getAccessToken(), userId, 0, Integer.MAX_VALUE, new Callback<List<FollowPersonModel>>() {
            @Override
            public void success(List<FollowPersonModel> likes, Response response) {
                progressBar.setVisibility(View.GONE);

                if (likes.size() == 0) {
                    statusContainer.setVisibility(View.VISIBLE);
                    tvStatus.setText(R.string.no_followers_found);
                    btnRetry.setVisibility(View.GONE);

                    return;
                }

                FollowerFollowingActivity.this.likes.addAll(likes);
                adapter.notifyDataSetChanged();
            }

            @Override
            public void failure(RetrofitError error) {
                progressBar.setVisibility(View.GONE);
                statusContainer.setVisibility(View.VISIBLE);
                tvStatus.setText(R.string.server_data_load_error);
            }
        });
    }

    private void loadFollowings() {
        RestAdapterProvider.getProvider().getRestApiForRetrofit1().following(User.getInstance().getAccessToken(), userId, 0, Integer.MAX_VALUE, new Callback<List<FollowPersonModel>>() {
            @Override
            public void success(List<FollowPersonModel> likes, Response response) {
                progressBar.setVisibility(View.GONE);

                if (likes.size() == 0) {
                    statusContainer.setVisibility(View.VISIBLE);
                    tvStatus.setText(R.string.following_nobody_msg);
                    btnRetry.setVisibility(View.GONE);

                    return;
                }

                FollowerFollowingActivity.this.likes.addAll(likes);
                adapter.notifyDataSetChanged();
            }

            @Override
            public void failure(RetrofitError error) {
                progressBar.setVisibility(View.GONE);
                statusContainer.setVisibility(View.VISIBLE);
                tvStatus.setText(R.string.server_data_load_error);
            }
        });
    }

    @OnClick(R.id.btnRetry)
    public void retryToLoadLikes() {
        loadLikes();
    }

}