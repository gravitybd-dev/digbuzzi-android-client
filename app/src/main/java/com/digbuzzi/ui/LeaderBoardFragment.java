package com.digbuzzi.ui;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.drawee.view.SimpleDraweeView;
import com.google.android.gms.appinvite.AppInviteInvitation;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.digbuzzi.R;
import com.digbuzzi.Util.StringUtils;
import com.digbuzzi.adapter.LeaderBoardAdapter;
import com.digbuzzi.api.RestAdapterProvider;
import com.digbuzzi.api.RestApi;
import com.digbuzzi.model.User;
import com.digbuzzi.model.restmodel.Leader;
import com.digbuzzi.model.restmodel.LeaderBoard;
import com.digbuzzi.view.PointRatioView;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class LeaderBoardFragment extends Fragment {

    private static final int APP_INVITE_REQUEST_CODE = 99;

    RestApi restApi = RestAdapterProvider.getProvider().getRestApiForRetrofit1();

    @Bind(R.id.ivPropic) SimpleDraweeView ivPropic;
    @Bind(R.id.tvPosition) TextView tvPosition;
    @Bind(R.id.tvUserName) TextView tvUserName;
    @Bind(R.id.prBar) PointRatioView prBar;
    @Bind(R.id.tvStatus) TextView tvStatus;
    @Bind(R.id.rvLeaders) RecyclerView rvLeaders;
    @Bind(R.id.actionPanel) View actionPanel;
    @Bind(R.id.userLeader) View userLeader;
    @Bind(R.id.loading) View loading;
    @Bind(R.id.leadersPanel) View leadersPanel;
    @Bind(R.id.refreshPanel) View refreshPanel;
    @Bind(R.id.invite) FloatingActionButton invite;

    LeaderBoardAdapter adapter;
    List<Leader> leaders;

    Leader currentUser;

    public LeaderBoardFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_leader_board, container, false);
        ButterKnife.bind(this, view);

        LinearLayoutManager manager = new LinearLayoutManager(getContext());
        manager.setOrientation(LinearLayoutManager.VERTICAL);
        leaders = new ArrayList<>();
        adapter = new LeaderBoardAdapter(getContext(), leaders);

        rvLeaders.setLayoutManager(manager);
        rvLeaders.setAdapter(adapter);

        resetUi();

        fetchLeaderBoard();

        return view;
    }

    private void fetchLeaderBoard() {
        restApi.leaderboard(User.getInstance().getAccessToken(), new Callback<LeaderBoard>() {
            @Override
            public void success(LeaderBoard leaderBoard, Response response) {
                if (response.getStatus() == 200) {
                    loading.setVisibility(View.INVISIBLE);
                    userLeader.setVisibility(View.VISIBLE);
                    leadersPanel.setVisibility(View.VISIBLE);
                    invite.setVisibility(View.VISIBLE);

                    if (leaderBoard.getLeaders() != null && leaderBoard.getLeaders().size() == 1) {
                        leaderBoard.getLeaders().remove(0);
                    }

                    if (leaderBoard.getLeaders().size() == 0) {
                        actionPanel.setVisibility(View.VISIBLE);
                    }

                    currentUser = leaderBoard.getUser();
                    showCurrentUserData(currentUser);
                    leaders.addAll(leaderBoard.getLeaders());
                    adapter.notifyDataSetChanged();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                loading.setVisibility(View.INVISIBLE);
                refreshPanel.setVisibility(View.VISIBLE);
                invite.setVisibility(View.VISIBLE);
            }
        });
    }

    private void showCurrentUserData(Leader currentUser) {
        if (currentUser == null) {
            Toast.makeText(getContext(), "Couldn't get the leader board", Toast.LENGTH_LONG).show();
            return;
        }

        ivPropic.setImageURI(Uri.parse(currentUser.getPhoto()));
        tvUserName.setText(TextUtils.isEmpty(currentUser.getName()) ? currentUser.getUserName() : currentUser.getName());

        int totalPoint = ((currentUser.getPoint() / 100) + 1) * 100;

        prBar.setTotalPoint(totalPoint);
        prBar.setCurrentPoint(currentUser.getPoint());

        tvStatus.setText(String.valueOf(currentUser.getPoint() + "/" + totalPoint));
        tvPosition.setText(StringUtils.ordinal(currentUser.getRank()));
    }

    @OnClick(R.id.btnRefresh)
    public void refresh() {
        resetUi();
        fetchLeaderBoard();
    }

    private void resetUi() {
        invite.setVisibility(View.INVISIBLE);
        userLeader.setVisibility(View.INVISIBLE);
        leadersPanel.setVisibility(View.INVISIBLE);
        refreshPanel.setVisibility(View.INVISIBLE);
        actionPanel.setVisibility(View.INVISIBLE);
        loading.setVisibility(View.VISIBLE);
    }

    @OnClick({R.id.tvInviteFriends, R.id.invite})
    public void inviteFriends() {
        String message = getString(R.string.app_invite_message);

        if (currentUser != null) {
            message = "I help this community and by helping I scored " + currentUser.getPoint()
                    + ", you can compete me too";
        }

        Intent intent = new AppInviteInvitation.IntentBuilder(getString(R.string.app_invite_title))
                .setMessage(message)
                .build();
        startActivityForResult(intent, APP_INVITE_REQUEST_CODE);
    }

    @OnClick(R.id.tvFollowOthers)
    public void followOthers() {
        Intent intent = new Intent(getContext(), FollowLeadersActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.ivScorePolicy)
    public void showScorePolicy() {
        startActivity(new Intent(getActivity(), ScorePolicyActivity.class));
    }
}