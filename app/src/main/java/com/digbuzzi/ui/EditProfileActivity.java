package com.digbuzzi.ui;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.digbuzzi.Util.PermissionUtils;
import com.digbuzzi.app.AppConfig;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.view.SimpleDraweeView;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.Bind;
import butterknife.ButterKnife;
import com.digbuzzi.R;
import com.digbuzzi.api.RestAdapterProvider;
import com.digbuzzi.model.User;
import com.digbuzzi.model.restmodel.Profile;
import com.digbuzzi.model.restmodel.UpdatedUser;
import com.facebook.imagepipeline.core.ImagePipeline;

import butterknife.OnClick;
import id.zelory.compressor.Compressor;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedFile;

public class EditProfileActivity extends BaseNotificationEnabledActivity {
    public static final String TAG = EditProfileActivity.class.getSimpleName();

    @Bind(R.id.ivSignInSatus)
    ProgressBar ivSignInSatus;

    @Bind(R.id.ivPropic)
    SimpleDraweeView ivPropic;

    @Bind(R.id.tvUserName)
    TextView tvUserName;

    @Bind(R.id.etName)
    EditText etName;

    @Bind(R.id.etBio)
    EditText etBio;

    @Bind(R.id.etEmail)
    EditText etEmail;

    @Bind(R.id.etCurrentPassword)
    EditText etCurrentPassword;

    @Bind(R.id.etPassword)
    EditText etPassword;

    @Bind(R.id.etConfirmPassword)
    EditText etConfirmPassword;

    @Bind(R.id.tvErrorEmail)
    View tvErrorEmail;

    @Bind(R.id.tvErrorCurrentPassword)
    View tvErrorCurrentPassword;

    @Bind(R.id.tvErrorNewPassword)
    View tvErrorNewPassword;

    @Bind(R.id.tvErrorConfirmPassword)
    View tvErrorConfirmPassword;

    File image;
    TypedFile typedImage;


    private Profile mProfile;

    private static final int IMAGE_PICKER_REQUEST = 1;
    private static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 2;

    @SuppressWarnings("ConstantConditions")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ivSignInSatus.setVisibility(View.GONE);

        fetchProfileInfo();


        tvErrorEmail.setVisibility(View.GONE);
        tvErrorCurrentPassword.setVisibility(View.GONE);
        tvErrorNewPassword.setVisibility(View.GONE);
        tvErrorConfirmPassword.setVisibility(View.GONE);
    }

    @Override
    protected void onNotification(Bundle data) {

    }

    @Override
    protected void onNotificationCounter(int counter) {

    }

    @OnClick(R.id.ivPropic)
    public void pickPhoto(){
        if (PermissionUtils.hasPermission(getApplicationContext(), Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
            photoPickerIntent.setType("image/*");
            startActivityForResult(photoPickerIntent, IMAGE_PICKER_REQUEST);
        } else {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.READ_EXTERNAL_STORAGE) ||
                    ActivityCompat.shouldShowRequestPermissionRationale(this,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                Toast.makeText(EditProfileActivity.this,
                        "We need this permission to update profile picture",
                        Toast.LENGTH_SHORT)
                        .show();
            } else {
                ActivityCompat.requestPermissions(EditProfileActivity.this,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    pickPhoto();
                } else {
                    Toast.makeText(EditProfileActivity.this, "Sorry, we cannot update profile image without your permission", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if(requestCode == IMAGE_PICKER_REQUEST){
                try {
                    pickPic(data);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    @SuppressWarnings("deprecation")
    private void pickPic(Intent intent) throws IOException {
        String[] filePathColumn = { MediaStore.Images.Media.DATA };

        // Get the cursor
        Cursor cursor = getContentResolver().query(intent.getData(),
                filePathColumn, null, null, null);

        if (cursor == null) return;

        // Move to first row
        cursor.moveToFirst();

        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
        String imgDecodableString = cursor.getString(columnIndex);
        cursor.close();
        showAndCompressImage(imgDecodableString);
    }

    private void showAndCompressImage(String filePath) {
        image = new File(filePath);
        Bitmap compressedBitmap = new Compressor.Builder(this)
                .setMaxWidth(AppConfig.PROFILE_PHOTO_DIMENSION_WIDTH)
                .setMaxHeight(AppConfig.PROFILE_PHOTO_DIMENSION_HEIGHT)
                .setCompressFormat(Bitmap.CompressFormat.JPEG)
                .setQuality(100)
                .build()
                .compressToBitmap(image);

        FileOutputStream out = null;
        try {
            out = new FileOutputStream(image);
            compressedBitmap.compress(Bitmap.CompressFormat.WEBP, AppConfig.IMAGE_QUALITY, out);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        ivPropic.setImageURI(Uri.parse("file://" + filePath));
        Log.d(TAG, "After compression: " + compressedBitmap.getByteCount());
    }

    private void fetchProfileInfo() {
        RestAdapterProvider.getProvider().getRestApiForRetrofit1()
                .getUserData(User.getInstance().getAccessToken(), User.getInstance().getUserId(), new Callback<Profile>() {
                    @Override
                    public void success(Profile profile, Response response) {
                        mProfile = profile;

                        tvUserName.setText(mProfile.getUserName());
                        etName.setText(mProfile.getName());
                        ivPropic.setImageURI(Uri.parse(mProfile.getPhoto()));
                        etEmail.setText(mProfile.getEmail());
                        etBio.setText(mProfile.getBio());
                        Log.d("Log", mProfile.getPhoto());
                    }

                    @Override
                    public void failure(RetrofitError error) {

                    }
                });
    }

    @OnClick({R.id.tvUpdateInfo, R.id.tvUpdatePass})
    public void onUpdateClick(View v) {

        String name = etName.getText().toString();
        String userEmail = etEmail.getText().toString();
        String currentPassword = etCurrentPassword.getText().toString();
        String userPasswordNew = etPassword.getText().toString();
        String userConfirmPassword = etConfirmPassword.getText().toString();
        String bio = etBio.getText().toString();


        if (userEmail.trim().isEmpty() || !Patterns.EMAIL_ADDRESS.matcher(userEmail).matches()) {
            tvErrorEmail.setVisibility(View.VISIBLE);
            return;
        } else {
            tvErrorEmail.setVisibility(View.GONE);
        }

        if (v.getId() == R.id.tvUpdatePass) {
            if(currentPassword.isEmpty() || currentPassword.length() < 6){
                tvErrorCurrentPassword.setVisibility(View.VISIBLE);
                return;
            } else {
                tvErrorCurrentPassword.setVisibility(View.GONE);
            }

            if (userPasswordNew.isEmpty() && userConfirmPassword.isEmpty()) {
                tvErrorConfirmPassword.setVisibility(View.VISIBLE);
                return;
            }

            if (!userPasswordNew.isEmpty() || !userConfirmPassword.isEmpty()) {
                if (!userPasswordNew.equals(userConfirmPassword)) {
                    tvErrorConfirmPassword.setVisibility(View.VISIBLE);
                    return;
                } else {
                    tvErrorConfirmPassword.setVisibility(View.GONE);
                }

                if (userPasswordNew.length() < 6) {
                    tvErrorNewPassword.setVisibility(View.VISIBLE);
                    return;
                } else {
                    tvErrorNewPassword.setVisibility(View.GONE);
                }
            }
        } else {
            currentPassword = null;
            userPasswordNew = null;
        }

        tvErrorNewPassword.setVisibility(View.GONE);
        tvErrorConfirmPassword.setVisibility(View.GONE);

        userPasswordNew = (userPasswordNew == null || userPasswordNew.isEmpty()) ? null : userPasswordNew;
        updateRequest(name, userEmail, currentPassword, userPasswordNew, bio);
    }

    private void updateRequest(String name, String userEmail, String currentPassword, String userPasswordNew, String bio) {

        ivSignInSatus.setVisibility(View.VISIBLE);

        String ApiToken = User.getInstance().getAccessToken();
        if(image == null){
            typedImage = null;
        }else {
            Log.d(TAG, "Size: " + image.length());
            typedImage = new TypedFile("multipart/form-data", image);
        }

        RestAdapterProvider.getProvider().getRestApiForRetrofit1().updateUser(ApiToken, typedImage, name, userPasswordNew, currentPassword, userEmail, bio, new Callback<UpdatedUser>() {
            @Override
            public void success(UpdatedUser updatedUser, Response response) {
                ivSignInSatus.setVisibility(View.GONE);
                mProfile = updatedUser.getProfile();
                mProfile.setEmail(mProfile.getEmail());
                User.getInstance().setAccessToken(updatedUser.getUpdatedToken());
                Log.d("Log", mProfile.getEmail() + updatedUser.getUpdatedToken());
                clearPreviousProfileUriFromCache();
                finish();
            }

            @Override
            public void failure(RetrofitError error) {
                Log.d("Log", error.toString());
                ivSignInSatus.setVisibility(View.GONE);
                Toast.makeText(getApplicationContext(), "Something went wrong! Please, try again", Toast.LENGTH_LONG).show();
            }
        });
    }

    private void clearPreviousProfileUriFromCache() {
        ImagePipeline imagePipeline = Fresco.getImagePipeline();
        Uri uri = Uri.parse(mProfile.getPhoto());
        imagePipeline.evictFromCache(uri);
    }

}