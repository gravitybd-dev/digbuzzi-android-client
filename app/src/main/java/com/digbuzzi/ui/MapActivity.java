package com.digbuzzi.ui;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.PersistableBundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.FloatingActionButton;
import android.support.graphics.drawable.VectorDrawableCompat;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.util.LruCache;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.TextView;

import com.digbuzzi.PlaceOfInterestHelper;
import com.digbuzzi.app.AppConfig;
import com.digbuzzi.app.DigbuzziApp;
import com.digbuzzi.model.MapCondition;
import com.digbuzzi.model.PlaceOfInterestGroup;
import com.digbuzzi.model.restmodel.PlaceOfInterest;
import com.digbuzzi.ui.fragment.BottomFeedDialogFragment;
import com.digbuzzi.ui.fragment.BottomPlaceDialogFragment;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.AutocompletePrediction;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.TileOverlay;
import com.google.android.gms.maps.model.TileOverlayOptions;
import com.google.android.gms.maps.model.VisibleRegion;
import com.google.maps.android.SphericalUtil;
import com.google.maps.android.heatmaps.Gradient;
import com.google.maps.android.heatmaps.HeatmapTileProvider;
import com.google.maps.android.heatmaps.WeightedLatLng;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import butterknife.Bind;
import butterknife.ButterKnife;
import com.digbuzzi.R;
import com.digbuzzi.adapter.PlaceAutocompleteAdapter;
import com.digbuzzi.api.RestAdapterProvider;
import com.digbuzzi.api.RestApi;
import com.digbuzzi.model.User;
import com.digbuzzi.model.restmodel.Heat;
import com.digbuzzi.trafficmap.TouchableWrapper;
import com.google.maps.android.ui.IconGenerator;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class MapActivity extends BaseNotificationEnabledActivity implements OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,
        TouchableWrapper.UpdateMapAfterUserInterection {
    LruCache<String, BitmapDescriptor> placeTypeBitmapDescriptor;

    private static final String TAG = MapActivity.class.getSimpleName();

    private static final long DELAY_AFTER_GESTURE = 800L;
    // Request code to use when launching the resolution activity
    private static final int REQUEST_RESOLVE_ERROR = 1001;
    private static final int REQUEST_CODE_LOCATION = 2;
    // Unique tag for the error dialog fragment
    private static final String DIALOG_ERROR = "dialog_error";
    // Bool to track whether the app is already resolving an error
    private boolean mResolvingError = false;
    private static final String STATE_RESOLVING_ERROR = "resolving_error";

    private static final String PLACE_OF_INTEREST_ID_PREFIX = "place_of_interest";

    GoogleApiClient mGoogleApiClient;
    GoogleMap map;
    boolean isGoogleApiClientConnected;

    @Bind(R.id.autocomplete_places)
    AutoCompleteTextView mAutocompleteView;

    private PlaceAutocompleteAdapter mAdapter;
    private Handler mHandler;

    private RestApi restApi;

    private HeatmapTileProvider mProvider;
    private TileOverlay mOverlay;

    LatLng lastCenteredLatLng;
    float lastZoomLevel = 0;

    GoogleMap.OnMarkerClickListener markerClickListener = new GoogleMap.OnMarkerClickListener() {
        @Override
        public boolean onMarkerClick(Marker marker) {
            String shoutId = markerIdVsShoutIdMap.get(marker.getId());
            if (shoutId != null) {
                Log.i(TAG, "Marker shoutId: " + shoutId);

                ArrayList<String> ids = new ArrayList<>();
                ids.add(shoutId);

                BottomFeedDialogFragment fragment = BottomFeedDialogFragment.newInstance(ids);
                fragment.show(getSupportFragmentManager(), BottomFeedDialogFragment.TAG);
            } else {
                String placeId = markerIdVsPlaceIdMap.get(marker.getId());
                Log.i(TAG, "Marker placeId: " + placeId);

                BottomPlaceDialogFragment fragment = BottomPlaceDialogFragment.newInstance(placeId);
                fragment.show(getSupportFragmentManager(), BottomPlaceDialogFragment.TAG);
            }
            //fragment.loadData();

            return true;
        }
    };

    /**
     * Indicate a shout is already added to the GoogleMap or not
     */
    Set<String> shoutIdSet = new HashSet<>();

    /**
     * Indicate a place is already added to the GoogleMap or not
     */
    Set<String> placeIdSet = new HashSet<>();

    /**
     * Store shoutId value against a markerId key
     */
    Map<String, String> markerIdVsShoutIdMap = new HashMap<>();

    /**
     * Store placeId value against a markerId key
     */
    Map<String, String> markerIdVsPlaceIdMap = new HashMap<>();

    List<PlaceOfInterestGroup> groups;

    int[] colors = {
            Color.GREEN,
            Color.YELLOW,
            Color.RED
    };

    BitmapDescriptor high, low, medium;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
        ButterKnife.bind(this);

        restApi = RestAdapterProvider.getProvider().getRestApiForRetrofit1();

        mResolvingError = savedInstanceState != null
                && savedInstanceState.getBoolean(STATE_RESOLVING_ERROR, false);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (TextUtils.isEmpty(User.getInstance().getAccessToken())) {
                    Intent intent = new Intent(MapActivity.this, SignUpLogInActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    return;
                }
                startActivity(new Intent(MapActivity.this, PlaceOfInterestActivity.class));
            }
        });

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addApi(Places.GEO_DATA_API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        mAutocompleteView.setOnItemClickListener(mAutocompleteClickListener);
        mAutocompleteView.setOnFocusChangeListener(mOnFocusChangeListener);
        mAutocompleteView.setOnEditorActionListener(mOnEditorActionListener);

        // Set up the adapter that will retrieve suggestions from the Places Geo Data API that cover
        // the entire world.
        mAdapter = new PlaceAutocompleteAdapter(this, mGoogleApiClient, null,
                null);
        mAutocompleteView.setAdapter(mAdapter);

        mHandler = new Handler();

        initMarkerIconDescriptors();

        prepareMap();

        groups = PlaceOfInterestHelper.loadPlaceGroups(getApplicationContext());

        preparePlaceTypeBitmapDescriptors();
    }

    private void preparePlaceTypeBitmapDescriptors() {
        int cacheSize = 4 * 1024 * 1024; // 4MiB
        placeTypeBitmapDescriptor = new LruCache<>(cacheSize);
        Resources res = getResources();
        for (int i = 0, size = groups.size(); i < size; i++) {
            int resId = res.getIdentifier(groups.get(i).getMapImageName(), "drawable", getPackageName());
            placeTypeBitmapDescriptor.put(groups.get(i).getPlaceGroupType(), getBitmapDescriptor(res, resId));
        }
    }

    private BitmapDescriptor getBitmapDescriptor(Resources resources, int resourceId) {
        VectorDrawableCompat placeIcon = VectorDrawableCompat.create(resources, resourceId, null);
        Bitmap bitmap = Bitmap.createBitmap(placeIcon.getIntrinsicWidth(), placeIcon.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        placeIcon.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        placeIcon.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }

    // init descriptors
    private void initMarkerIconDescriptors() {
        high = BitmapDescriptorFactory.fromResource(R.mipmap.ic_high_traffic_map);
        medium = BitmapDescriptorFactory.fromResource(R.mipmap.ic_medium_traffic_map);
        low = BitmapDescriptorFactory.fromResource(R.mipmap.ic_low_traffic_map);
    }

    private BitmapDescriptor markerIconDescriptorForTrafficCondition(String trafficCondition) {
        if (trafficCondition.equals("High")) {
            return high;
        } else if (trafficCondition.equals("Medium")) {
            return medium;
        } else {
            return low;
        }
    }

    private BitmapDescriptor markerIconDescriptorForPlaceType(Resources resources, String placeType) {
        String groupType = PlaceOfInterestHelper.groupTypeOfPlaceType(groups, placeType);
        BitmapDescriptor bitmapDescriptor = placeTypeBitmapDescriptor.get(groupType);
        if (bitmapDescriptor == null) {
            String groupMapImageName = PlaceOfInterestHelper.choosePinIconForMapAccordingToGroup(groups, placeType);
            int resId = resources.getIdentifier(groupMapImageName, "drawable", getPackageName());
            bitmapDescriptor = getBitmapDescriptor(resources, resId);
            placeTypeBitmapDescriptor.put(groupType, bitmapDescriptor);
        }
        return bitmapDescriptor;
    }

    private final Runnable mAfterSafelyGestureCompletedRunnable = new Runnable() {
        @Override
        public void run() {
            if (map != null) {
                lastCenteredLatLng = getCenterLocationOfMap();
                lastZoomLevel = map.getCameraPosition().zoom;
                double radiusToLook = getRadiusToLook(lastCenteredLatLng);
                fetchDateForAreaOfInterest(lastCenteredLatLng, radiusToLook);
                fetchPlacesOfInterest(lastCenteredLatLng, radiusToLook);
            }
        }
    };

    public LatLng getCenterLocationOfMap() {
        return map.getCameraPosition().target;
    }

    private double getRadiusToLook(LatLng centerLocation) {
        VisibleRegion visibleRegion = map.getProjection().getVisibleRegion();
        LatLng nearRight = visibleRegion.nearRight;
        LatLng nearLeft = visibleRegion.nearLeft;

        double maxRadius = Math.max(SphericalUtil.computeDistanceBetween(centerLocation, nearLeft),
                SphericalUtil.computeDistanceBetween(centerLocation, nearRight));

        return maxRadius;
    }

    private void fetchDateForAreaOfInterest(LatLng centerLocation, double radiusToLook) {
        restApi.getHeatData(centerLocation.latitude, centerLocation.longitude, radiusToLook, new Callback<List<Heat>>() {
            @Override
            public void success(List<Heat> heats, Response response) {
                for (Heat heat : heats) {
                    if (!shoutIdSet.contains(heat.getShoutId())) {
                        String trafficCondition = heat.getTrafficCondition();
                        MarkerOptions markerOptions = new MarkerOptions()
                                .icon(markerIconDescriptorForTrafficCondition(trafficCondition))
                                .position(new LatLng(heat.getLat(), heat.getLon()));
                        Marker marker = map.addMarker(markerOptions);
                        shoutIdSet.add(heat.getShoutId());
                        markerIdVsShoutIdMap.put(marker.getId(), heat.getShoutId());
                    }
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (error.getMessage() != null) {
                    Log.d(TAG, error.getMessage());
                }
            }
        });
    }

    private void fetchPlacesOfInterest(LatLng centerLocation, double radiusToLook) {
        restApi.getPlacesOfInterest(User.getInstance().getAccessToken(), centerLocation.latitude, centerLocation.longitude, radiusToLook, new Callback<List<PlaceOfInterest>>() {
            @Override
            public void success(List<PlaceOfInterest> placeOfInterests, Response response) {
                Resources resources = getResources();
                for (PlaceOfInterest place : placeOfInterests) {
                    String placeId = place.getPlaceId();
                    if (!placeIdSet.contains(placeId)) {
                        String placeType = place.getPlaceTypes().get(0);
                        double lat = place.getLocation().getLatitude();
                        double lon = place.getLocation().getLongitude();

                        MarkerOptions markerOptions = new MarkerOptions()
                                .icon(markerIconDescriptorForPlaceType(resources, placeType))
                                .position(new LatLng(lat, lon));
                        Marker marker = map.addMarker(markerOptions);

                        placeIdSet.add(placeId);
                        markerIdVsPlaceIdMap.put(marker.getId(), placeId);
                    }
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (error.getMessage() != null) {
                    Log.d(TAG, error.getMessage());
                }
            }
        });
    }

    private View.OnFocusChangeListener mOnFocusChangeListener = new View.OnFocusChangeListener() {
        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            if (!hasFocus) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
            }
        }
    };

    private TextView.OnEditorActionListener mOnEditorActionListener = new TextView.OnEditorActionListener() {
        @Override
        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                mAutocompleteView.clearFocus();
                return true;
            }
            return false;
        }
    };

    private AdapterView.OnItemClickListener mAutocompleteClickListener
            = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            /*
             Retrieve the place ID of the selected item from the Adapter.
             The adapter stores each Place suggestion in a AutocompletePrediction from which we
             read the place ID and title.
              */
            mAutocompleteView.clearFocus();

            final AutocompletePrediction item = mAdapter.getItem(position);
            final String placeId = item.getPlaceId();
            final CharSequence primaryText = item.getPrimaryText(null);

            Log.i(TAG, "Autocomplete item selected: " + primaryText);

            /*
             Issue a request to the Places Geo Data API to retrieve a Place object with additional
             details about the place.
              */
            PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi
                    .getPlaceById(mGoogleApiClient, placeId);
            placeResult.setResultCallback(mUpdatePlaceDetailsCallback);

            Log.i(TAG, "Called getPlaceById to get Place details for " + placeId);
        }
    };

    /**
     * Callback for results from a Places Geo Data API query that shows the first place result in
     * the details view on screen.
     */
    private ResultCallback<PlaceBuffer> mUpdatePlaceDetailsCallback
            = new ResultCallback<PlaceBuffer>() {
        @Override
        public void onResult(PlaceBuffer places) {
            if (!places.getStatus().isSuccess()) {
                // Request did not complete successfully
                Log.e(TAG, "Place query did not complete. Error: " + places.getStatus().toString());
                places.release();
                return;
            }

            Place place = places.get(0);
            animateCameraTo(place.getLatLng());
//            // Display the third party attributions if set.
//            final CharSequence thirdPartyAttribution = places.getAttributions();
//            if (thirdPartyAttribution == null) {
//                mPlaceDetailsAttribution.setVisibility(View.GONE);
//            } else {
//                mPlaceDetailsAttribution.setVisibility(View.VISIBLE);
//                mPlaceDetailsAttribution.setText(Html.fromHtml(thirdPartyAttribution.toString()));
//            }

            places.release();
        }
    };

    private static Spanned formatPlaceDetails(Resources res, CharSequence name, String id,
                                              CharSequence address, CharSequence phoneNumber, Uri websiteUri) {
        Log.e(TAG, res.getString(R.string.place_details, name, id, address, phoneNumber,
                websiteUri));
        return Html.fromHtml(res.getString(R.string.place_details, name, id, address, phoneNumber,
                websiteUri));

    }

    @Override
    protected void onStart() {
        super.onStart();
        if (!mResolvingError) {
            mGoogleApiClient.connect();
        }
    }

    @Override
    protected void onStop() {
        mHandler.removeCallbacks(mAfterSafelyGestureCompletedRunnable);
        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, mLocationListener);
        mGoogleApiClient.disconnect();
        ((DigbuzziApp)getApplication()).saveLastMapCondition(new MapCondition(lastCenteredLatLng, lastZoomLevel));
        super.onStop();
    }

    @Override
    protected void onNotification(Bundle data) {

    }

    @Override
    protected void onNotificationCounter(int counter) {

    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(STATE_RESOLVING_ERROR, mResolvingError);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void prepareMap() {
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        this.map.getUiSettings().setMyLocationButtonEnabled(false);

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    REQUEST_CODE_LOCATION);
        }

        // set click lister for marker tap event
        map.setOnMarkerClickListener(markerClickListener);
    }

    @SuppressWarnings("MissingPermission")
    private void animateCameraToMyLocation() {
        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(10000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setNumUpdates(1);
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, mLocationListener);
    }

    private void animateCameraTo(LatLng latlng) {
        animateCameraTo(latlng, AppConfig.DEFAULT_MAP_ZOOM_LEVEL);
    }

    private void animateCameraTo(LatLng latLng, float zoomLevel) {
        if (map != null) {
            CameraPosition cameraPosition =
                    new CameraPosition.Builder()
                            .target(new LatLng(latLng.latitude, latLng.longitude))
                            .zoom(zoomLevel)
                            .build();
            map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition), new GoogleMap.CancelableCallback() {
                @Override
                public void onFinish() {
                    lastCenteredLatLng = getCenterLocationOfMap();
                    lastZoomLevel = map.getCameraPosition().zoom;
                    double radiusToLook = getRadiusToLook(lastCenteredLatLng);
                    fetchDateForAreaOfInterest(lastCenteredLatLng, radiusToLook);
                    fetchPlacesOfInterest(lastCenteredLatLng, radiusToLook);
                }

                @Override
                public void onCancel() {

                }
            });
        }
    }

    private LocationListener mLocationListener = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            animateCameraTo(new LatLng(location.getLatitude(), location.getLongitude()));
        }
    };

    @Override
    public void onConnected(Bundle bundle) {
        isGoogleApiClientConnected = true;
        if (map != null) {
            MapCondition lastMapCondition = ((DigbuzziApp) getApplication()).getLastMapCondition();
            if (lastMapCondition != null && lastMapCondition.isUsableForMap()) {
                animateCameraTo(lastMapCondition.getLastLookedPosition(), lastMapCondition.getLastLookedZoomLevel());
            } else if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                animateCameraToMyLocation();
            }
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        isGoogleApiClientConnected = false;
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        isGoogleApiClientConnected = false;

        if (!mResolvingError) {
            if (result.hasResolution()) {
                try {
                    mResolvingError = true;
                    result.startResolutionForResult(this, REQUEST_RESOLVE_ERROR);
                } catch (IntentSender.SendIntentException e) {
                    // There was an error with the resolution intent. Try again.
                    mGoogleApiClient.connect();
                }
            } else {
                // Show dialog using GoogleApiAvailability.getErrorDialog()
                showErrorDialog(result.getErrorCode());
                mResolvingError = true;
            }
        }
    }

    /* Creates a dialog for an error message */
    private void showErrorDialog(int errorCode) {
        // Create a fragment for the error dialog
        ErrorDialogFragment dialogFragment = new ErrorDialogFragment();
        // Pass the error that should be displayed
        Bundle args = new Bundle();
        args.putInt(DIALOG_ERROR, errorCode);
        dialogFragment.setArguments(args);
        dialogFragment.show(getSupportFragmentManager(), "Unexpected error occurred");
    }

    /* Called from ErrorDialogFragment when the dialog is dismissed. */
    public void onDialogDismissed() {
        mResolvingError = false;
    }

    @Override
    public void onUpdateMapAfterUserInterection() {
        mHandler.removeCallbacks(mAfterSafelyGestureCompletedRunnable);
        mHandler.postDelayed(mAfterSafelyGestureCompletedRunnable, DELAY_AFTER_GESTURE);
    }

    /* A fragment to display an error dialog */
    public static class ErrorDialogFragment extends DialogFragment {
        public ErrorDialogFragment() { }

        @Override
        public @NonNull
        Dialog onCreateDialog(Bundle savedInstanceState) {
            // Get the error code and retrieve the appropriate dialog
            int errorCode = this.getArguments().getInt(DIALOG_ERROR);
            return GoogleApiAvailability.getInstance().getErrorDialog(
                    this.getActivity(), errorCode, REQUEST_RESOLVE_ERROR);
        }

        @Override
        public void onDismiss(DialogInterface dialog) {
            ((MapActivity) getActivity()).onDialogDismissed();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_RESOLVE_ERROR) {
            mResolvingError = false;
            if (resultCode == RESULT_OK) {
                // Make sure the app is not already connected or attempting to connect
                if (!mGoogleApiClient.isConnecting() &&
                        !mGoogleApiClient.isConnected()) {
                    mGoogleApiClient.connect();
                }
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == REQUEST_CODE_LOCATION) {
            if (grantResults.length == 1
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                animateCameraToMyLocation();
            }
        }
    }
}