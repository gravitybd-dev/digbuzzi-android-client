package com.digbuzzi.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.digbuzzi.R;
import com.digbuzzi.event.ForgetPasswordEvent;
import com.digbuzzi.event.LogInRequestEvent;
import com.digbuzzi.event.SignUpRequestEvent;
import com.digbuzzi.event.UserAuthorizedEvent;
import com.digbuzzi.ui.fragment.signuplogin.ForgetPasswordFragment;
import com.digbuzzi.ui.fragment.signuplogin.LogInFragment;
import com.digbuzzi.ui.fragment.signuplogin.SignUpFragment;
import com.digbuzzi.event.NewlyRegisteredEvent;

public class SignUpLogInActivity extends BaseNotificationEnabledActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up_log_in);

        Fragment loginFragment = new LogInFragment();
        getSupportFragmentManager().beginTransaction().replace(R.id.container, loginFragment).commit();
    }

    @Override
    protected boolean onTokenExpireShouldShowLoginScreen() {
        return false;
    }

    public void onEventMainThread(SignUpRequestEvent event) {
        getSupportFragmentManager().popBackStackImmediate();
        Fragment signUpFragment = new SignUpFragment();
        getSupportFragmentManager().beginTransaction().replace(R.id.container, signUpFragment).addToBackStack(null).commit();
    }

    public void onEventMainThread(LogInRequestEvent event) {
        getSupportFragmentManager().popBackStackImmediate();
        Fragment loginFragment = new LogInFragment();
        getSupportFragmentManager().beginTransaction().replace(R.id.container, loginFragment).commit();
    }

    public void onEventMainThread(ForgetPasswordEvent event) {
        getSupportFragmentManager().popBackStackImmediate();
        Fragment forgetPasswordFragment = new ForgetPasswordFragment();
        getSupportFragmentManager().beginTransaction().replace(R.id.container, forgetPasswordFragment).addToBackStack(null).commit();
    }

    public void onEventMainThread(UserAuthorizedEvent event) {
        startActivity(new Intent(this, NavDrawerActivity.class));
        finish();
    }

    public void onEventMainThread(NewlyRegisteredEvent event) {
        startActivity(new Intent(this, WelcomeActivity.class));
        finish();
    }

    @Override
    protected void onNotification(Bundle data) {

    }

    @Override
    protected void onNotificationCounter(int counter) {

    }
}
