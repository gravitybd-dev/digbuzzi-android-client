package com.digbuzzi.ui;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import com.digbuzzi.R;
import com.digbuzzi.adapter.CommentAdapter;
import com.digbuzzi.api.RestAdapterProvider;
import com.digbuzzi.model.User;
import com.digbuzzi.model.restmodel.Comment;
import com.digbuzzi.model.restmodel.RequestAddComment;
import com.digbuzzi.model.restmodel.ShoutFeed;
import com.digbuzzi.repo.BuzzRepository;
import java.util.ArrayList;
import java.util.List;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

@SuppressWarnings("WeakerAccess")
public class CommentActivity extends BaseNotificationEnabledActivity {

    private static final String TAG = CommentActivity.class.getSimpleName();

    public static final String EXTRA_COMMENT_TYPE = "comment_type_extra_key";
    public static final int COMMENT_TYPE_BUZZ = 1;
    public static final int COMMENT_TYPE_PLACE = 2;

    @Bind(R.id.toolbar)
    Toolbar toolbar;

    @Bind(R.id.rvComments)
    RecyclerView rvComments;

    @Bind(R.id.etComment)
    EditText etComment;

    @Bind(R.id.progressBar)
    ProgressBar progressBar;

    @Bind(R.id.statusContainer)
    View statusContainer;

    @Bind(R.id.tvStatus)
    TextView tvStatus;

    @Bind(R.id.btnRetry)
    Button btnRetry;

    @Bind(R.id.ivSend)
    View ivSend;

    @Bind(R.id.sendLoading)
    View sendLoading;

    List<Comment> comments;
    CommentAdapter adapter;

    boolean commentPosting;

    ShoutFeed feed;

    private int currentCommentType = -1; // -1 means no comment type associated yet

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comment);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);

        currentCommentType = getIntent().getIntExtra(EXTRA_COMMENT_TYPE, -1);

        comments = new ArrayList<>();

        LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rvComments.setLayoutManager(layoutManager);

        adapter = new CommentAdapter(getApplicationContext(), comments);
        rvComments.setAdapter(adapter);

        feed = BuzzRepository.getInstance().getBuzz(getIntent().getStringExtra("buzzId"));

        loadComments();
    }

    @Override
    protected void onNotification(Bundle data) {

    }

    @Override
    protected void onNotificationCounter(int counter) {

    }

    private void loadComments() {
        statusContainer.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);

        if (currentCommentType == COMMENT_TYPE_BUZZ) {
            loadCommentsForBuzz(commentsCallback);
        } else if (currentCommentType == COMMENT_TYPE_PLACE) {
            loadCommentsForPlace(commentsCallback);
        }
    }

    public void loadCommentsForBuzz(Callback<List<Comment>> commentsCallback) {
        String buzzId = feed != null ? feed.getShoutId() : getIntent().getStringExtra("buzzId");
        RestAdapterProvider.getProvider().getRestApiForRetrofit1().comments(User.getInstance().getAccessToken(), buzzId, 0, Integer.MAX_VALUE, commentsCallback);
    }

    public void loadCommentsForPlace(Callback<List<Comment>> commentsCallback) {
        String placeId = getIntent().getStringExtra("placeId");
        RestAdapterProvider.getProvider().getRestApiForRetrofit1().placeComments(User.getInstance().getAccessToken(), placeId, 0, Integer.MAX_VALUE, commentsCallback);
    }

    Callback<List<Comment>> commentsCallback = new Callback<List<Comment>>() {
        @Override
        public void success(List<Comment> comments, Response response) {
            progressBar.setVisibility(View.GONE);

            if (comments.size() == 0) {
                statusContainer.setVisibility(View.VISIBLE);
                tvStatus.setText(R.string.comment_none_msg);
                btnRetry.setVisibility(View.GONE);

                return;
            }

            CommentActivity.this.comments.addAll(comments);
            adapter.notifyDataSetChanged();
        }

        @Override
        public void failure(RetrofitError error) {
            progressBar.setVisibility(View.GONE);
            statusContainer.setVisibility(View.VISIBLE);
            tvStatus.setText(R.string.comment_fetch_failure_msg);
        }
    };

    @OnClick(R.id.btnRetry)
    public void retryToLoadComments() {
        loadComments();
    }

    @OnClick(R.id.ivSend)
    public void postComment() {
        if (commentPosting) return;

        String comment = etComment.getText().toString().trim();

        if (comment.isEmpty()) {
            Toast.makeText(getApplicationContext(), "Please, write a comment first", Toast.LENGTH_LONG).show();
            return;
        }

        commentPosting = true;
        resetLoadingUi();

        RequestAddComment requestAddComment = new RequestAddComment();
        requestAddComment.commentText = comment;
        requestAddComment.time = System.currentTimeMillis();

        if (currentCommentType == COMMENT_TYPE_BUZZ) {
            postCommentForBuzz(requestAddComment, commentCallback);
        } else {
            postCommentForPlace(requestAddComment, commentCallback);
        }
    }

    private void postCommentForBuzz(RequestAddComment requestAddComment, Callback<Comment> commentCallback) {
        String buzzId = feed != null ? feed.getShoutId() : getIntent().getStringExtra("buzzId");
        RestAdapterProvider.getProvider().getRestApiForRetrofit1().addComment(User.getInstance().getAccessToken(), buzzId, requestAddComment, commentCallback);
    }

    private void postCommentForPlace(RequestAddComment requestAddComment, Callback<Comment> commentCallback) {
        String placeId = getIntent().getStringExtra("placeId");
        RestAdapterProvider.getProvider().getRestApiForRetrofit1().addCommentForPlace(User.getInstance().getAccessToken(), placeId, requestAddComment, commentCallback);
    }

    private void resetLoadingUi() {
        ivSend.setVisibility(commentPosting ? View.INVISIBLE : View.VISIBLE);
        sendLoading.setVisibility(commentPosting ? View.VISIBLE : View.GONE);
    }

    private void showAddCommentErrorMessage() {
        Toast.makeText(this, "Couldn't post your comment! Please, try again", Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onPause() {
        if (feed != null) {
            if (comments.size() > 0) feed.setCommentCount(comments.size());
        }
        super.onPause();
    }

    Callback<Comment> commentCallback = new Callback<Comment>() {
        @Override
        public void success(Comment comment, Response response) {
            commentPosting = false;
            resetLoadingUi();

            if (comment == null) {
                showAddCommentErrorMessage();
                return;
            }

            etComment.getText().clear();

            progressBar.setVisibility(View.GONE);
            statusContainer.setVisibility(View.GONE);

            comments.add(comment);
            adapter.notifyDataSetChanged();
            rvComments.scrollToPosition(comments.size() - 1);
        }

        @Override
        public void failure(RetrofitError error) {
            commentPosting = false;
            resetLoadingUi();

            if (error.getResponse() != null) {
                Log.d(TAG, error.getResponse().toString());
            }
            showAddCommentErrorMessage();
        }
    };
}