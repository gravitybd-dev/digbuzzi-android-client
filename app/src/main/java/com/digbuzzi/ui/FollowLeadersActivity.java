package com.digbuzzi.ui;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.digbuzzi.R;
import com.digbuzzi.adapter.FollowLeadersAdapter;
import com.digbuzzi.api.RestAdapterProvider;
import com.digbuzzi.app.AppConfig;
import com.digbuzzi.model.User;
import com.digbuzzi.model.restmodel.Leader;
import java.util.List;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

@SuppressWarnings("WeakerAccess")
public class FollowLeadersActivity extends BaseNotificationEnabledActivity {
    private static final String TAG = FollowLeadersActivity.class.getSimpleName();

    @Bind(R.id.rvAllFeed)
    RecyclerView rvAllFeed;

    @Bind(R.id.srl)
    SwipeRefreshLayout srl;

    @Bind(R.id.statusContainer)
    View statusContainer;

    @Bind(R.id.tvStatus)
    TextView tvStatus;

    @Bind(R.id.btnRetry)
    Button btnRetry;

    FollowLeadersAdapter adapter;

    EndlessRecyclerOnScrollListener endlessRecyclerOnScrollListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_follow_leaders);
        ButterKnife.bind(this);

        init();
    }

    @Override
    protected void onNotification(Bundle data) {

    }

    @Override
    protected void onNotificationCounter(int counter) {

    }

    private void init() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);

        rvAllFeed.setLayoutManager(layoutManager);

        endlessRecyclerOnScrollListener = new EndlessRecyclerOnScrollListener(layoutManager) {
            @Override
            public void onLoadMore(int offset, int count) {
                loadMore(offset, count);
            }
        };

        rvAllFeed.addOnScrollListener(endlessRecyclerOnScrollListener);
        adapter = new FollowLeadersAdapter(this);
        rvAllFeed.setAdapter(adapter);

        if (adapter.getItemCount() == 0) {
            srl.post(new Runnable() {
                @Override
                public void run() {
                    srl.setRefreshing(true);
                }
            });
            refreshFeeds();
        }

        srl.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshFeeds();
            }
        });
    }

    @OnClick(R.id.btnRetry)
    public void retry() {
        if (adapter.getItemCount() == 0) {
            srl.post(new Runnable() {
                @Override
                public void run() {
                    srl.setRefreshing(true);
                }
            });
            refreshFeeds();
        }

        srl.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshFeeds();
            }
        });
    }

    private void refreshFeeds() {
        statusContainer.setVisibility(View.GONE);

        RestAdapterProvider.getProvider().getRestApiForRetrofit1()
                .leaders(User.getInstance().getAccessToken(), 0, AppConfig.LEADER_COUNT_PER_REQUEST,
                        new Callback<List<Leader>>() {
                            @Override
                            public void success(List<Leader> leaders, Response response) {
                                srl.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        srl.setRefreshing(false);
                                    }
                                });
                                if (leaders != null) {
                                    endlessRecyclerOnScrollListener.reset(leaders.size());
                                    adapter.setLeaders(leaders);
                                }

                                if (adapter.getItemCount() == 0) {
                                    statusContainer.setVisibility(View.VISIBLE);
                                    tvStatus.setText(R.string.no_leader_found_msg);
                                    btnRetry.setText(R.string.refresh_caps);
                                }
                            }

                            @Override
                            public void failure(RetrofitError error) {
                                srl.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        srl.setRefreshing(false);
                                    }
                                });
                                if (adapter.getItemCount() == 0) {
                                    statusContainer.setVisibility(View.VISIBLE);
                                    tvStatus.setText(R.string.leader_load_err_msg);
                                    btnRetry.setText(R.string.retry_caps);
                                }
                            }
                        });
    }

    private void loadMore(int offset, int count) {
        Log.d(TAG, "Offset: " + offset + ", Count: " + count);
        RestAdapterProvider.getProvider().getRestApiForRetrofit1().leaders(User.getInstance().getAccessToken(), offset, count,
                new Callback<List<Leader>>() {
                    @Override
                    public void success(List<Leader> leaders, Response response) {
                        Log.d(TAG, response.toString());
                        if (leaders != null) {
                            adapter.addLeaders(leaders);
                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        Log.d(TAG, error.getMessage());
                    }
                });
    }
}