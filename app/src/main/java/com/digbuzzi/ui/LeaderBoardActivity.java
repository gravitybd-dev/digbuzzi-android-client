
package com.digbuzzi.ui;

import android.os.Bundle;
import android.view.MenuItem;

import com.digbuzzi.R;

public class LeaderBoardActivity extends BaseNotificationEnabledActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_leader_board);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onNotification(Bundle data) {

    }

    @Override
    protected void onNotificationCounter(int counter) {

    }
}
