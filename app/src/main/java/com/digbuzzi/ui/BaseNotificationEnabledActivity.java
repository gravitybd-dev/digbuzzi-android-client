package com.digbuzzi.ui;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.text.Spanned;
import android.widget.Toast;

import com.digbuzzi.app.AppConfig;
import com.digbuzzi.app.DigbuzziApp;
import com.digbuzzi.event.BuzzPostedEvent;
import com.digbuzzi.event.FollowUserEvent;
import com.digbuzzi.event.NotificationEvent;
import com.digbuzzi.event.PlacePostedEvent;
import com.digbuzzi.event.RefreshNowFeedEvent;
import com.digbuzzi.model.User;
import com.digbuzzi.repo.BuzzRepository;

import cn.pedant.SweetAlert.SweetAlertDialog;
import de.greenrobot.event.EventBus;

public abstract class BaseNotificationEnabledActivity extends AppCompatActivity {
    private SharedPreferences preferences;
    private boolean isForeGround;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (User.getInstance().getExpireTime() < System.currentTimeMillis()) {
            preferences.edit().putBoolean(AppConfig.SENT_TOKEN_TO_SERVER, false).apply();
            if (onTokenExpireShouldShowLoginScreen()) {
                User.getInstance().clear();
                BuzzRepository.getInstance().clear();
                finish();
                Intent intent = new Intent(getApplicationContext(), SignUpLogInActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        }
    }

    protected boolean onTokenExpireShouldShowLoginScreen() {
        return true;
    }

    @Override
    protected void onStart() {
        isForeGround = true;
        preferences.edit().putBoolean(AppConfig.IS_FOREGROUND, isForeGround).apply();
        EventBus.getDefault().register(this);
        onNotificationCounter(preferences.getInt(AppConfig.NOTIFICATION_COUNTER, 0));
        super.onStart();
    }

    @Override
    protected void onStop() {
        isForeGround = false;
        preferences.edit().putBoolean(AppConfig.IS_FOREGROUND, isForeGround).apply();
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @SuppressWarnings("unused")
    public void onEventMainThread(NotificationEvent event) {
        int notificationCounter = preferences.getInt(AppConfig.NOTIFICATION_COUNTER, 0);
        onNotification(event.getData());
        onNotificationCounter(notificationCounter);
    }

    @SuppressWarnings("unused")
    public void onEventMainThread(BuzzPostedEvent event) {
        EventBus.getDefault().post(new RefreshNowFeedEvent());

        if(!((DigbuzziApp) getApplication()).isPointRewardConfirmationForPostingBuzzEnabled()) return;

        new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE)
                .setTitleText("Congratulations!")
                .setContentText("You've earned \"02\" points for posting a Buzz. Keep up the good work and in no time you'll be on top of the leaderboard!")
                .setConfirmText("Okay!")
                .showCancelButton(true)
                .setCancelText("Don't show this again")
                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        ((DigbuzziApp) getApplication()).setPointRewardConfirmationForPostingBuzzEnabled(false);
                        sDialog.cancel();
                    }
                })
                .show();
    }

    @SuppressWarnings("unused")
    public void onEventMainThread(FollowUserEvent event) {
        if (!((DigbuzziApp) getApplication()).isPointRewardConfirmationForFollowOtherUserEnabled()) return;

        new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE)
                .setTitleText("Congratulations!")
                .setContentText("You've earned \"02\" points for following " + event.getUsername() + ". Keep up the good work and in no time you'll be on top of the leaderboard!")
                .setConfirmText("Okay!")
                .showCancelButton(true)
                .setCancelText("Don't show this again")
                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        ((DigbuzziApp) getApplication()).setPointRewardConfirmationForFollowOtherUserEnabled(false);
                        sDialog.cancel();
                    }
                })
                .show();
    }

    @SuppressWarnings("unused")
    public void onEventMainThread(PlacePostedEvent event) {
        Toast.makeText(getApplicationContext(), "Place posted Successfully", Toast.LENGTH_LONG).show();

        if (!((DigbuzziApp) getApplication()).isPointRewardConfirmationForPlacePostedEnabled()) return;

        new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE)
                .setTitleText("Congratulations!")
                .setContentText("You've earned \"02\" points for posting a new place of interest. Keep up the good work and in no time you'll be on top of the leaderboard!")
                .setConfirmText("Okay!")
                .showCancelButton(true)
                .setCancelText("Don't show this again")
                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        ((DigbuzziApp) getApplication()).setPointRewardConfirmationForPlacePostedEnabled(false);
                        sDialog.cancel();
                    }
                })
                .show();
    }

    protected abstract void onNotification(Bundle data);
    protected abstract void onNotificationCounter(int counter);
}