package com.digbuzzi.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.digbuzzi.R;
import com.digbuzzi.adapter.ShoutFeedAdapter;
import com.digbuzzi.api.RestAdapterProvider;
import com.digbuzzi.app.AppConfig;
import com.digbuzzi.model.User;
import com.digbuzzi.model.restmodel.ShoutFeed;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

@SuppressWarnings("WeakerAccess")
public class NearbyBuzz extends BaseNotificationEnabledActivity {

    public static final String KEY_PLACE_NAME = "key_place_name";
    public static final String KEY_LATITUDE = "key_latitude";
    public static final String KEY_LONGITUDE = "key_longitude";

    @Bind(R.id.rvAllFeed)
    RecyclerView rvAllFeed;

    @Bind(R.id.srl)
    SwipeRefreshLayout srl;

    @Bind(R.id.statusContainer)
    View statusContainer;

    @Bind(R.id.tvStatus)
    TextView tvStatus;

    @Bind(R.id.btnRetry)
    Button btnRetry;

    List<ShoutFeed> feeds;
    ShoutFeedAdapter adapter;

    double latitude;
    double longitude;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nearby_buzz);

        ButterKnife.bind(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(NearbyBuzz.this, PostActivity.class));
            }
        });

        String placeName = getIntent().getStringExtra(KEY_PLACE_NAME);
        getSupportActionBar().setTitle("Buzz near " + placeName);

        latitude = getIntent().getDoubleExtra(KEY_LATITUDE, -1);
        longitude = getIntent().getDoubleExtra(KEY_LONGITUDE, -1);

        init();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onNotification(Bundle data) {

    }

    @Override
    protected void onNotificationCounter(int counter) {

    }

    private void init() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);

        rvAllFeed.setLayoutManager(layoutManager);
        feeds = new ArrayList<>();
        adapter = new ShoutFeedAdapter(this);
        rvAllFeed.setAdapter(adapter);

        if (feeds.size() == 0) {
            srl.post(new Runnable() {
                @Override
                public void run() {
                    srl.setRefreshing(true);
                }
            });
            refreshFeeds();
        }

        srl.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshFeeds();
            }
        });
    }

    @OnClick(R.id.btnRetry)
    public void retry() {
        if (feeds.size() == 0) {
            srl.post(new Runnable() {
                @Override
                public void run() {
                    srl.setRefreshing(true);
                }
            });
            refreshFeeds();
        }

        srl.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshFeeds();
            }
        });
    }

    private void refreshFeeds() {
        statusContainer.setVisibility(View.GONE);
        String accessToken = User.getInstance().getAccessToken();
        RestAdapterProvider.getProvider().getRestApiForRetrofit1().nearbyBuzz(accessToken, latitude, longitude, AppConfig.DEFAULT_SEARCH_RADIUS, new Callback<List<ShoutFeed>>() {
            @Override
            public void success(List<ShoutFeed> shoutFeeds, Response response) {
                srl.post(new Runnable() {
                    @Override
                    public void run() {
                        srl.setRefreshing(false);
                    }
                });
                if (shoutFeeds != null) {
                    feeds.addAll(shoutFeeds);
                    adapter.setFeeds(shoutFeeds);
                    adapter.notifyDataSetChanged();
                }

                if (feeds.size() == 0) {
                    statusContainer.setVisibility(View.VISIBLE);
                    tvStatus.setText("No Buzz available! Post one by hitting the bottom '+' button");
                    btnRetry.setText("REFRESH");
                }
            }

            @Override
            public void failure(RetrofitError error) {
                srl.post(new Runnable() {
                    @Override
                    public void run() {
                        srl.setRefreshing(false);
                    }
                });
                if (feeds.size() == 0) {
                    statusContainer.setVisibility(View.VISIBLE);
                    tvStatus.setText("Couldn't get any Buzz :(");
                    btnRetry.setText("RETRY");
                }
            }
        });
    }
}