package com.digbuzzi.ui;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import com.digbuzzi.R;
import com.digbuzzi.Util.Utils;
import com.digbuzzi.api.RestAdapterProvider;
import com.digbuzzi.app.AppConfig;
import com.digbuzzi.app.DigbuzziApp;
import com.digbuzzi.event.ScrollToTopEvent;
import com.digbuzzi.model.User;
import com.digbuzzi.model.restmodel.DeviceRegisterUnregisterRequest;
import com.digbuzzi.model.restmodel.GenericResponse;
import com.digbuzzi.model.restmodel.Profile;
import com.digbuzzi.receivers.AppUnusedForSpecificDurationReceiver;
import com.digbuzzi.repo.BuzzRepository;
import com.digbuzzi.service.CacheNowFeedService;
import com.digbuzzi.service.RegistrationIntentService;
import com.digbuzzi.ui.fragment.NotificationFragment;
import com.digbuzzi.ui.fragment.shoutfeed.FollowingFeedFragment;
import com.digbuzzi.ui.fragment.shoutfeed.NowFeedFragment;
import com.facebook.drawee.view.SimpleDraweeView;
import com.google.android.gms.appinvite.AppInviteInvitation;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit2.Call;

@SuppressWarnings("WeakerAccess")
public class NavDrawerActivity extends BaseNotificationEnabledActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    private static final String TAG = NavDrawerActivity.class.getSimpleName();

    private static final int PLACE_AUTOCOMPLETE_REQUEST_CODE = 1;
    private static final int APP_INVITE_REQUEST_CODE = 99;

    @Bind(R.id.tabs)
    TabLayout tabs;

    @Bind(R.id.pager)
    ViewPager pager;

    //how to user butter here

    //@Bind(R.id.ivUserPhoto)
    SimpleDraweeView ivUserPhoto;

    //@Bind(R.id.ivUserName)
    TextView ivUserName;

    //@Bind(R.id.ivUserEmail)
    TextView ivUserEmail;

    private Profile mProfile;

    @SuppressWarnings("unused")
    private String mUserId;

    SharedPreferences sharedPreferences;

    int lastNavigationId;
    int currentNavigationId;

    NavigationView navigationView;

    ShoutFeedPagerAdapter shoutFeedPagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nav_drawer);
        ButterKnife.bind(this);

        lastNavigationId = -1;
        currentNavigationId = -1;

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        //setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(NavDrawerActivity.this, PostActivity.class));
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        drawer.addDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {

            }

            @Override
            public void onDrawerOpened(View drawerView) {

            }

            @Override
            public void onDrawerClosed(View drawerView) {
                if (lastNavigationId != currentNavigationId) {
                    lastNavigationId = currentNavigationId;
                    navigateToCurrentSelectedItem();
                }
            }

            @Override
            public void onDrawerStateChanged(int newState) {

            }
        });
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        ivUserPhoto = (SimpleDraweeView)navigationView.getHeaderView(0).findViewById(R.id.ivUserPhoto);
        ivUserName = (TextView)navigationView.getHeaderView(0).findViewById(R.id.ivUserName);
        ivUserEmail = (TextView)navigationView.getHeaderView(0).findViewById(R.id.ivUserEmail);

        fetchProfileInfo();

        shoutFeedPagerAdapter = new ShoutFeedPagerAdapter(getSupportFragmentManager());
        pager.setAdapter(shoutFeedPagerAdapter);
        tabs.setupWithViewPager(pager);
        tabs.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                pager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                EventBus.getDefault().post(
                        new ScrollToTopEvent(
                                pager.getAdapter()
                                        .getPageTitle(tab.getPosition())
                                        .toString()));
            }
        });

        ivUserPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(NavDrawerActivity.this, ProfileActivity.class));
            }
        });


        if (!sharedPreferences.getBoolean(AppConfig.SENT_TOKEN_TO_SERVER, false)) {
            startService(new Intent(this, RegistrationIntentService.class));
        }
    }

    /**
     * Set a timer to throw a notification after a certain period the app is unused by user
     *
     * @param period - Interval in millis to throw a notification
     */
    private void setAppUnusedNotificationForCertainPeriod(long period) {
        AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        Intent intent = new Intent(getApplicationContext(), AppUnusedForSpecificDurationReceiver.class);
        PendingIntent pendingIntent =
                PendingIntent.getBroadcast(getApplicationContext(),
                                            0,
                                            intent,
                                            PendingIntent.FLAG_UPDATE_CURRENT);
        alarmManager.set(AlarmManager.RTC_WAKEUP,
                System.currentTimeMillis() + period,
                pendingIntent);
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    private void fetchProfileInfo() {
        RestAdapterProvider.getProvider().getRestApiForRetrofit1()
                .getUserData(User.getInstance().getAccessToken(), mUserId, new Callback<Profile>() {
                    @Override
                    public void success(Profile profile, Response response) {
                        mProfile = profile;

                        ivUserName.setText(mProfile.getUserName());
                        ivUserPhoto.setImageURI(Uri.parse(mProfile.getPhoto()));
                        ivUserEmail.setText(mProfile.getEmail());
                    }

                    @Override
                    public void failure(RetrofitError error) {

                    }
                });
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.search:
                showPlaceSearch();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @OnClick(R.id.searchPanel)
    public void showPlaceSearch() {
        try {
            Intent intent =
                    new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_OVERLAY)
                            .build(this);
            startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);
        } catch (GooglePlayServicesRepairableException | GooglePlayServicesNotAvailableException e) {
            // TODO: Handle the error.
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        lastNavigationId = -1;
        currentNavigationId = item.getItemId();

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);

        // return false so that item isn't selected in the navigation view (in this case we don't want it)
        return false;
    }

    /**
     * Handle navigation view item clicks here.
     */
    private void navigateToCurrentSelectedItem() {
        int id = currentNavigationId;
        if (id == R.id.map) {
            Intent intent = new Intent(this, MapActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_logout) {
            logout();
        } else if (id == R.id.nav_rateUs){
            Uri uri = Uri.parse("market://details?id=" + getPackageName());
            Intent myAppLinkToMarket = new Intent(Intent.ACTION_VIEW, uri);
            try {
                startActivity(myAppLinkToMarket);
            } catch (ActivityNotFoundException e) {
                Toast.makeText(this, " unable to find market app", Toast.LENGTH_LONG).show();
            }
        } else if (id == R.id.board) {
            Intent intent = new Intent(this, LeaderBoardActivity.class);
            startActivity(intent);
        } else if (id == R.id.appInvite) {
            Intent intent = new AppInviteInvitation.IntentBuilder(getString(R.string.app_invite_title))
                    .setMessage(getString(R.string.app_invite_message))
//                    .setDeepLink(Uri.parse(getString(R.string.invitation_deep_link)))
//                    .setCustomImage(Uri.parse(getString(R.string.invitation_custom_image)))
//                    .setCallToActionText(getString(R.string.invitation_cta))
                    .build();
            startActivityForResult(intent, APP_INVITE_REQUEST_CODE);
        }else if (id == R.id.profile){
            startActivity(new Intent(NavDrawerActivity.this, ProfileActivity.class));
        }else if(id == R.id.people){
            Intent intent = new Intent(NavDrawerActivity.this, FollowLeadersActivity.class);
            startActivity(intent);
        }else if(id == R.id.settings){
            startActivity( new Intent(this,EditProfileActivity.class));
        }
    }

    private void logout() {
        DeviceRegisterUnregisterRequest request = new DeviceRegisterUnregisterRequest();
        request.appId = Utils.appId(getApplicationContext());
        Call<GenericResponse> call = RestAdapterProvider.getProvider().getRestApiForRetrofit2().removeDevice(User.getInstance().getAccessToken(), request);
        call.enqueue(new retrofit2.Callback<GenericResponse>() {
            @Override
            public void onResponse(Call<GenericResponse> call, retrofit2.Response<GenericResponse> response) {
                sharedPreferences.edit().putBoolean(AppConfig.SENT_TOKEN_TO_SERVER, false).apply();
                User.getInstance().clear();
                BuzzRepository.getInstance().clear();
                clearCache();
                finish();
                ((DigbuzziApp) getApplication()).resetUserPreferences();
                Intent intent = new Intent(getApplicationContext(), SignUpLogInActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }

            @Override
            public void onFailure(Call<GenericResponse> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Sorry couldn't logout. Please, try again", Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(this, data);
                Log.i(TAG, "Place: " + place.getName());
                Intent intent = new Intent(this, NearbyBuzz.class);
                intent.putExtra(NearbyBuzz.KEY_PLACE_NAME, place.getName());
                intent.putExtra(NearbyBuzz.KEY_LATITUDE, place.getLatLng().latitude);
                intent.putExtra(NearbyBuzz.KEY_LONGITUDE, place.getLatLng().longitude);
                startActivity(intent);
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(this, data);
                // TODO: Handle the error.
                Log.i(TAG, status.getStatusMessage());

            } else if (resultCode == RESULT_CANCELED) {
                Log.i(TAG, "Cancelled");
            }
        }
    }

    @Override
    protected void onNotification(Bundle data) {
        // todo: handle notification
    }

    @Override
    protected void onNotificationCounter(int counter) {
        // todo: update notification counter
    }

    private static class ShoutFeedPagerAdapter extends FragmentPagerAdapter {

        ShoutFeedPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            Fragment fragment = null;
            switch (position) {
                case 0:
                    fragment = NowFeedFragment.newInstance();
                    break;
                case 1:
                    fragment = FollowingFeedFragment.newInstance();
                    break;
                case 2:
                    fragment = NotificationFragment.newInstance("", "");
                    break;
            }
            return fragment;
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            String title = "";
            switch (position) {
                case 0:
                    title = NowFeedFragment.TITLE;
                    break;
                case 1:
                    title = FollowingFeedFragment.TITLE;
                    break;
                case 2:
                    title = NotificationFragment.TITLE;
                    break;
            }
            return title;
        }

    }

    private void clearCache() {
        try {
            deleteFile(CacheNowFeedService.NOW_CACHE_FILE_NAME);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onStop() {
        setAppUnusedNotificationForCertainPeriod(AppConfig.APP_UNUSED_PERIOD_TO_NOTIFY_USER);
        super.onStop();
    }
}