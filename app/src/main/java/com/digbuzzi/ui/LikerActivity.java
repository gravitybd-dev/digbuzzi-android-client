package com.digbuzzi.ui;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.digbuzzi.R;
import com.digbuzzi.adapter.LikeAdapter;
import com.digbuzzi.api.RestAdapterProvider;
import com.digbuzzi.model.User;
import com.digbuzzi.model.restmodel.Like;
import java.util.ArrayList;
import java.util.List;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

@SuppressWarnings("WeakerAccess")
public class LikerActivity extends BaseNotificationEnabledActivity {
    private static final String TAG = LikerActivity.class.getSimpleName();

    public static final String EXTRA_LIKE_TYPE = "like_type_extra_key";
    public static final int LIKE_TYPE_BUZZ = 1;
    public static final int LIKE_TYPE_PLACE = 2;

    @Bind(R.id.toolbar) Toolbar toolbar;

    @Bind(R.id.rvLikes) RecyclerView rvLikes;

    @Bind(R.id.progressBar) ProgressBar progressBar;

    @Bind(R.id.statusContainer) View statusContainer;

    @Bind(R.id.tvStatus) TextView tvStatus;

    @Bind(R.id.btnRetry) Button btnRetry;

    private List<Like> likes;
    private LikeAdapter adapter;

    private int currentLikeType = -1; // -1 means currently no type specified

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_liker);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);

        likes = new ArrayList<>();

        LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rvLikes.setLayoutManager(layoutManager);

        adapter = new LikeAdapter(getApplicationContext(), likes);
        rvLikes.setAdapter(adapter);

        currentLikeType = getIntent().getIntExtra(EXTRA_LIKE_TYPE, -1);

        loadLikes();
    }

    @Override
    protected void onNotification(Bundle data) {

    }

    @Override
    protected void onNotificationCounter(int counter) {

    }

    private void loadLikes() {
        statusContainer.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);

        if (currentLikeType == LIKE_TYPE_BUZZ) {
            loadLikesForBuzz(likesCallback);
        } else if (currentLikeType == LIKE_TYPE_PLACE) {
            loadLikesForPlace(likesCallback);
        }
    }

    private void loadLikesForBuzz(Callback<List<Like>> likesCallback) {
        String buzzId = getIntent().getStringExtra("buzzId");
        RestAdapterProvider.getProvider().getRestApiForRetrofit1().likes(User.getInstance().getAccessToken(), buzzId, likesCallback);
    }

    private void loadLikesForPlace(Callback<List<Like>> likesCallback) {
        String placeId = getIntent().getStringExtra("placeId");
        RestAdapterProvider.getProvider().getRestApiForRetrofit1().placeLikes(User.getInstance().getAccessToken(), placeId, likesCallback);
    }

    @OnClick(R.id.btnRetry)
    public void retryToLoadLikes() {
        loadLikes();
    }

    Callback<List<Like>> likesCallback = new Callback<List<Like>>() {
        @Override
        public void success(List<Like> likes, Response response) {
            progressBar.setVisibility(View.GONE);

            if (likes.size() == 0) {
                statusContainer.setVisibility(View.VISIBLE);
                tvStatus.setText(R.string.no_liker_yet_msg);
                btnRetry.setVisibility(View.GONE);

                return;
            }

            LikerActivity.this.likes.addAll(likes);
            adapter.notifyDataSetChanged();
        }

        @Override
        public void failure(RetrofitError error) {
            progressBar.setVisibility(View.GONE);
            statusContainer.setVisibility(View.VISIBLE);
            tvStatus.setText(R.string.liker_fetch_failure_msg);
        }
    };
}