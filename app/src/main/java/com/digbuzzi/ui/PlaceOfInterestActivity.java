package com.digbuzzi.ui;

import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.os.Bundle;

import com.digbuzzi.R;
import com.digbuzzi.event.PlaceOfInterestGroupSelectedEvent;
import com.digbuzzi.event.PostingPlaceOfInterestInitiatedEvent;
import com.digbuzzi.model.PlaceOfInterestType;
import com.digbuzzi.model.PlaceOfInterestGroup;
import com.digbuzzi.ui.fragment.placeofinterest.PlaceGroupFragment;
import com.digbuzzi.ui.fragment.placeofinterest.PlacePostFragment;
import com.digbuzzi.ui.fragment.placeofinterest.common.IProvidePlaces;

import java.util.List;

public class PlaceOfInterestActivity extends BaseNotificationEnabledActivity implements IProvidePlaces {
    PlaceOfInterestGroup group;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_place_of_interest);

        loadPlaceGroupFragment();
    }

    @Override
    protected void onNotification(Bundle data) {

    }

    @Override
    protected void onNotificationCounter(int counter) {

    }

    private void loadPlaceGroupFragment() {
        load(PlaceGroupFragment.newInstance());
    }

    private void loadPlacePostFragment() {
        load(PlacePostFragment.newInstance());
    }

    private void load(Fragment fragment) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container, fragment)
                .commit();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.container);
        if (fragment != null) {
            fragment.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @SuppressWarnings("unused")
    public void onEventMainThread(PlaceOfInterestGroupSelectedEvent event) {
        group = event.getGroup();
        loadPlacePostFragment();
    }

    @SuppressWarnings("unused")
    public void onEventMainThread(PostingPlaceOfInterestInitiatedEvent event) {
        finish();
    }

    @Override
    public List<PlaceOfInterestType> getPlaces() {
        return group.getPlaceGroupMembers();
    }
}