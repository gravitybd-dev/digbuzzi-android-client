package com.digbuzzi.ui.fragment.shoutfeed;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import com.digbuzzi.R;
import com.digbuzzi.adapter.ShoutFeedAdapter;
import com.digbuzzi.api.RestAdapterProvider;
import com.digbuzzi.app.AppConfig;
import com.digbuzzi.event.BuzzUpdated;
import com.digbuzzi.event.NewContentAvailableEvent;
import com.digbuzzi.event.RefreshNowFeedEvent;
import com.digbuzzi.event.ScrollToTopEvent;
import com.digbuzzi.model.User;
import com.digbuzzi.model.restmodel.ShoutFeed;
import com.digbuzzi.repo.BuzzRepository;
import com.digbuzzi.service.CacheNowFeedService;
import com.digbuzzi.ui.EndlessRecyclerOnScrollListener;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link NowFeedFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
@SuppressWarnings("WeakerAccess")
public class NowFeedFragment extends Fragment {
    public static final String TITLE = "NOW";
    private static final String TAG = NowFeedFragment.class.getSimpleName();

    @Bind(R.id.rvFeed)
    RecyclerView rvAllFeed;

    @Bind(R.id.srl)
    SwipeRefreshLayout srl;

    @Bind(R.id.statusContainer)
    View statusContainer;

    @Bind(R.id.tvStatus)
    TextView tvStatus;

    @Bind(R.id.btnRetry)
    Button btnRetry;

    ShoutFeedAdapter adapter;

    EndlessRecyclerOnScrollListener endlessRecyclerOnScrollListener;

    private BuzzRepository repository = BuzzRepository.getInstance();

    //GcmNetworkManager mGcmNetworkManager;
    //PeriodicTask newContentAvailabilityPullTask;

    public NowFeedFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     */
    public static NowFeedFragment newInstance() {
        return new NowFeedFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        mGcmNetworkManager = GcmNetworkManager.getInstance(getContext());
//        newContentAvailabilityPullTask = new PeriodicTask.Builder()
//                .setPeriod(AppConfig.NEW_CONTENT_PULLING_PERIOD)
//                .setRequiredNetwork(Task.NETWORK_STATE_CONNECTED)
//                .setTag(TASK_NEW_CONTENT_AVAILABILITY_FOR_NOW)
//                .setService(NewContentCheckServiceForNow.class)
//                .build();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_all_shout_feed, container, false);
        ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        adapter.notifyDataSetChanged();
        EventBus.getDefault().register(this);
        //mGcmNetworkManager.schedule(newContentAvailabilityPullTask);
    }

    @Override
    public void onPause() {
        EventBus.getDefault().unregister(this);
        //mGcmNetworkManager.cancelTask(TASK_NEW_CONTENT_AVAILABILITY_FOR_NOW, NewContentCheckServiceForNow.class);
        super.onPause();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);

        rvAllFeed.setLayoutManager(layoutManager);

        endlessRecyclerOnScrollListener = new EndlessRecyclerOnScrollListener(layoutManager) {
            @Override
            public void onLoadMore(int offset, int count) {
                loadMore(offset, count);
            }
        };

        rvAllFeed.addOnScrollListener(endlessRecyclerOnScrollListener);
        adapter = new ShoutFeedAdapter(getContext());
        rvAllFeed.setAdapter(adapter);

        String cacheJson = getNowFeedCache();

        srl.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshFeeds(true);
            }
        });

        if (cacheJson == null) {
            srl.post(new Runnable() {
                @Override
                public void run() {
                    srl.setRefreshing(true);
                }
            });
            refreshFeeds(true);
        } else {
            List<ShoutFeed> shoutFeeds = new Gson().fromJson(cacheJson, new TypeToken<List<ShoutFeed>>(){}.getType());
            if (shoutFeeds != null) {
                statusContainer.setVisibility(View.GONE);
                endlessRecyclerOnScrollListener.reset(shoutFeeds.size());
                repository.saveBuzzes(TAG, shoutFeeds);
                adapter.setFeeds(shoutFeeds);
                adapter.notifyDataSetChanged();
                refreshFeeds(false);
            } else {
                srl.post(new Runnable() {
                    @Override
                    public void run() {
                        srl.setRefreshing(true);
                    }
                });
            }
        }
    }

    @OnClick(R.id.btnRetry)
    public void retry() {
        if (adapter.getItemCount() == 0) {
            srl.post(new Runnable() {
                @Override
                public void run() {
                    srl.setRefreshing(true);
                }
            });
            refreshFeeds(true);
        }

        srl.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshFeeds(true);
            }
        });
    }

    private void refreshFeeds(final boolean showLoadingRelatedViews) {
        statusContainer.setVisibility(View.GONE);
        String accessToken = User.getInstance().getAccessToken();
        RestAdapterProvider.getProvider().getRestApiForRetrofit1().allShouts(accessToken, 0, AppConfig.FEED_COUNT_PER_REQUEST, new Callback<List<ShoutFeed>>() {
            @Override
            public void success(List<ShoutFeed> shoutFeeds, Response response) {
                srl.post(new Runnable() {
                    @Override
                    public void run() {
                        srl.setRefreshing(false);
                    }
                });
                if (shoutFeeds != null) {
                    cacheResponse(shoutFeeds);
                    repository.removeBuzzes(TAG);
                    repository.saveBuzzes(TAG, shoutFeeds);
                    List<ShoutFeed> feeds = repository.getBuzzes(TAG);
                    endlessRecyclerOnScrollListener.reset(feeds.size());
                    adapter.setFeeds(feeds);
                    adapter.notifyDataSetChanged();

                }

                if (adapter.getItemCount() == 0 && showLoadingRelatedViews) {
                    statusContainer.setVisibility(View.VISIBLE);
                    tvStatus.setText(R.string.no_feed_msg);
                    btnRetry.setText(R.string.refresh);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                srl.post(new Runnable() {
                    @Override
                    public void run() {
                        srl.setRefreshing(false);
                    }
                });
                if (adapter.getItemCount() == 0 && showLoadingRelatedViews) {
                    statusContainer.setVisibility(View.VISIBLE);
                    tvStatus.setText(R.string.feed_fetch_error);
                    btnRetry.setText(R.string.retry);
                }
            }
        });
    }

    private void loadMore(int offset, int count) {
        RestAdapterProvider.getProvider().getRestApiForRetrofit1().allShouts(User.getInstance().getAccessToken(), offset, count, new Callback<List<ShoutFeed>>() {
            @Override
            public void success(List<ShoutFeed> shoutFeeds, Response response) {
                Log.d(NowFeedFragment.class.getSimpleName(), response.toString());
                if (shoutFeeds != null) {
                    repository.saveBuzzes(TAG, shoutFeeds);
                    adapter.setFeeds(repository.getBuzzes(TAG));
                    adapter.notifyDataSetChanged();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (error.getResponse() != null) {
                    Log.d(NowFeedFragment.class.getSimpleName(), error.getResponse().toString());
                }
            }
        });
    }

    @SuppressWarnings("unused")
    public void onEventMainThread(NewContentAvailableEvent event) {
        Toast.makeText(getContext(), "Content available", Toast.LENGTH_LONG).show();
    }

    @SuppressWarnings("unused")
    public void onEventMainThread(BuzzUpdated event) {
        adapter.notifyDataSetChanged();
    }

    @SuppressWarnings("unused")
    public void onEventMainThread(ScrollToTopEvent event) {
        if (rvAllFeed != null && event.getTitle().equals(TITLE)) {
            rvAllFeed.smoothScrollToPosition(0);
        }
    }

    @SuppressWarnings("unused")
    public void onEventMainThread(RefreshNowFeedEvent event) {
        int position = rvAllFeed.getChildAdapterPosition(rvAllFeed.getChildAt(0));

        // if feed is at the top refresh feed to show currently posted buzz
        if (position == 0) {
            refreshFeeds(false);
        }
    }

    private void cacheResponse(List<ShoutFeed> shoutFeeds) {
        try {
            String json = new Gson().toJson(shoutFeeds);
            Intent intent = new Intent(getContext(), CacheNowFeedService.class);
            intent.putExtra(CacheNowFeedService.NOW_JSON, json);
            getContext().startService(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String getNowFeedCache() {
        FileInputStream inputStream = null;
        String cache = null;
        try {
            inputStream = getContext().openFileInput(CacheNowFeedService.NOW_CACHE_FILE_NAME);
            StringBuilder builder = new StringBuilder();
            int ch;
            while((ch = inputStream.read()) != -1){
                builder.append((char)ch);
            }
            inputStream.close();
            cache = builder.toString();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return cache;
    }
}