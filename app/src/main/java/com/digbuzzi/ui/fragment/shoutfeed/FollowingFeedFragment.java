package com.digbuzzi.ui.fragment.shoutfeed;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.digbuzzi.R;
import com.digbuzzi.adapter.ShoutFeedAdapter;
import com.digbuzzi.api.RestAdapterProvider;
import com.digbuzzi.app.AppConfig;
import com.digbuzzi.event.BuzzUpdated;
import com.digbuzzi.event.NewContentAvailableEvent;
import com.digbuzzi.event.ScrollToTopEvent;
import com.digbuzzi.model.User;
import com.digbuzzi.model.restmodel.ShoutFeed;
import com.digbuzzi.repo.BuzzRepository;
import com.digbuzzi.ui.EndlessRecyclerOnScrollListener;
import de.greenrobot.event.EventBus;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link FollowingFeedFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
@SuppressWarnings("WeakerAccess")
public class FollowingFeedFragment extends Fragment {
    public static final String TITLE = "FOLLOWING";
    private static final String TAG = FollowingFeedFragment.class.getSimpleName();

    @Bind(R.id.rvFeed)
    RecyclerView rvFollowingFeed;

    @Bind(R.id.srl)
    SwipeRefreshLayout srl;

    @Bind(R.id.statusContainer)
    View statusContainer;

    @Bind(R.id.tvStatus)
    TextView tvStatus;

    @Bind(R.id.btnRetry)
    Button btnRetry;

    ShoutFeedAdapter adapter;

    EndlessRecyclerOnScrollListener endlessRecyclerOnScrollListener;
    BuzzRepository repository = BuzzRepository.getInstance();

    //GcmNetworkManager mGcmNetworkManager;
    //PeriodicTask newContentAvailabilityPullTask;

    public FollowingFeedFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment.
     * @return A new instance of fragment FollowingFeedFragment.
     */
    public static FollowingFeedFragment newInstance() {
        return new FollowingFeedFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        mGcmNetworkManager = GcmNetworkManager.getInstance(getContext());
//        newContentAvailabilityPullTask = new PeriodicTask.Builder()
//                .setPeriod(AppConfig.NEW_CONTENT_PULLING_PERIOD)
//                .setRequiredNetwork(Task.NETWORK_STATE_CONNECTED)
//                .setTag(TASK_NEW_CONTENT_AVAILABILITY_FOR_FOLLOWING)
//                .setService(NewContentCheckServiceForFollowing.class)
//                .build();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_all_shout_feed, container, false);
        ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        adapter.notifyDataSetChanged();
        EventBus.getDefault().register(this);
        //mGcmNetworkManager.schedule(newContentAvailabilityPullTask);
    }

    @Override
    public void onPause() {
        EventBus.getDefault().unregister(this);
        //mGcmNetworkManager.cancelTask(TASK_NEW_CONTENT_AVAILABILITY_FOR_FOLLOWING, NewContentCheckServiceForFollowing.class);
        super.onPause();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);

        rvFollowingFeed.setLayoutManager(layoutManager);

        endlessRecyclerOnScrollListener = new EndlessRecyclerOnScrollListener(layoutManager) {
            @Override
            public void onLoadMore(int offset, int count) {
                loadMore(offset, count);
            }
        };

        rvFollowingFeed.addOnScrollListener(endlessRecyclerOnScrollListener);
        adapter = new ShoutFeedAdapter(getContext());
        rvFollowingFeed.setAdapter(adapter);

        if (adapter.getItemCount() == 0) {
            srl.post(new Runnable() {
                @Override
                public void run() {
                    srl.setRefreshing(true);
                }
            });
            refreshFeeds();
        }

        srl.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshFeeds();
            }
        });
    }

    @OnClick(R.id.btnRetry)
    public void retry() {
        if (adapter.getItemCount() == 0) {
            srl.post(new Runnable() {
                @Override
                public void run() {
                    srl.setRefreshing(true);
                }
            });
            refreshFeeds();
        }

        srl.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshFeeds();
            }
        });
    }

    private void refreshFeeds() {
        statusContainer.setVisibility(View.GONE);

        RestAdapterProvider.getProvider().getRestApiForRetrofit1().followerShouts(User.getInstance().getAccessToken(), 0, AppConfig.FEED_COUNT_PER_REQUEST, new Callback<List<ShoutFeed>>() {
            @Override
            public void success(List<ShoutFeed> shoutFeeds, Response response) {
                srl.post(new Runnable() {
                    @Override
                    public void run() {
                        srl.setRefreshing(false);
                    }
                });
                if (shoutFeeds != null) {
                    repository.removeBuzzes(TAG);
                    repository.saveBuzzes(TAG, shoutFeeds);
                    List<ShoutFeed> feeds = repository.getBuzzes(TAG);
                    endlessRecyclerOnScrollListener.reset(feeds.size());
                    adapter.setFeeds(feeds);
                    adapter.notifyDataSetChanged();
                }

                if (adapter.getItemCount() == 0) {
                    statusContainer.setVisibility(View.VISIBLE);
                    tvStatus.setText("You are not following anyone !");
                    btnRetry.setText("REFRESH");
                }
            }

            @Override
            public void failure(RetrofitError error) {
                srl.post(new Runnable() {
                    @Override
                    public void run() {
                        srl.setRefreshing(false);
                    }
                });
                if (adapter.getItemCount() == 0) {
                    statusContainer.setVisibility(View.VISIBLE);
                    tvStatus.setText("Couldn't get the feeds");
                    btnRetry.setText("RETRY");
                }
            }
        });
    }

    private void loadMore(int offset, int count) {
        RestAdapterProvider.getProvider().getRestApiForRetrofit1().followerShouts(User.getInstance().getAccessToken(), offset, count, new Callback<List<ShoutFeed>>() {
            @Override
            public void success(List<ShoutFeed> shoutFeeds, Response response) {
                Log.d(NowFeedFragment.class.getSimpleName(), response.toString());
                if (shoutFeeds != null) {
                    repository.saveBuzzes(TAG, shoutFeeds);
                    adapter.setFeeds(repository.getBuzzes(TAG));
                    adapter.notifyDataSetChanged();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (error.getResponse() != null) {
                    Log.d(NowFeedFragment.class.getSimpleName(), error.getResponse().toString());
                }
            }
        });
    }

    @SuppressWarnings("unused")
    public void onEventMainThread(NewContentAvailableEvent event) {
        
    }

    @SuppressWarnings("unused")
    public void onEventMainThread(BuzzUpdated event) {
        adapter.notifyDataSetChanged();
    }

    @SuppressWarnings("unused")
    public void onEventMainThread(ScrollToTopEvent event) {
        if (rvFollowingFeed != null && event.getTitle().equals(TITLE)) {
            rvFollowingFeed.smoothScrollToPosition(0);
        }
    }
}