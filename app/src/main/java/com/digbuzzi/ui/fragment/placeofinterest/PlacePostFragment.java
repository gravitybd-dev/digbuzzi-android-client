package com.digbuzzi.ui.fragment.placeofinterest;

import android.Manifest;
import android.app.Activity;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.NotificationCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.digbuzzi.R;
import com.digbuzzi.Util.PermissionUtils;
import com.digbuzzi.adapter.PlaceOfInterestTypeAdapter;
import com.digbuzzi.api.RestAdapterProvider;
import com.digbuzzi.app.AppConfig;
import com.digbuzzi.event.PlaceOfInterestSelectionEvent;
import com.digbuzzi.event.PlacePostedEvent;
import com.digbuzzi.event.PostingPlaceOfInterestInitiatedEvent;
import com.digbuzzi.model.PlaceOfInterestType;
import com.digbuzzi.model.User;
import com.digbuzzi.model.restmodel.Location;
import com.digbuzzi.model.restmodel.PlaceOfInterest;
import com.digbuzzi.ui.fragment.placeofinterest.common.IProvidePlaces;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;
import id.zelory.compressor.Compressor;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedFile;

/**
 * Created by Joker on 7/22/16.
 */
public class PlacePostFragment extends Fragment implements
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private static final String TAG = PlacePostFragment.class.getSimpleName();

    private static final int REQUEST_IMAGE_CAPTURE = 1;
    private static final int PLACE_PICKER_REQUEST = 2;
    private static final int IMAGE_PICKER_REQUEST = 3;
    private static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE_FOR_GALLERY = 0;
    private static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE_FOR_CAMERA = 1;
    private static final int PERMISSION_REQUEST_FINE_LOCATION = 2;
    private static final int REQUEST_CHECK_SETTINGS = 11;

    File image;
    TypedFile typedImage;
    String mCurrentPhotoPath;
    Location location = new Location();
    String locationName;

    @Bind(R.id.place_list)
    RecyclerView rvPlaceList;

    @Bind(R.id.ivPhotoPreview)
    ImageView ivPhotoPreview;

    @Bind(R.id.shoutPicture)
    View shoutPicture;

    @Bind(R.id.etShout)
    EditText shoutText;

    @Bind(R.id.ivLocationName)
    TextView ivLocationName;

    @Bind(R.id.ivUploadStatus)
    ProgressBar ivUploadStatus;

    @Bind(R.id.ivSend)
    TextView ivSend;

    NotificationCompat.Builder mBuilder;
    NotificationManager mNotifyManager;

    int width, height;
    int loading_flag = 0;

    List<PlaceOfInterestType> places;

    //----------- Properties for google api client -----
    private static final int REQUEST_RESOLVE_ERROR = 1001;

    GoogleApiClient mGoogleApiClient;
    boolean googleApiClientConnected;
    boolean resolvingError;
    LocationRequest mLocationRequest;
    //----------- End ----------------------------------

    IProvidePlaces iProvidePlaces;
    private String placeTypes;

    public static PlacePostFragment newInstance() {
        return new PlacePostFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            iProvidePlaces = (IProvidePlaces) getActivity();
        } catch (ClassCastException e) {
            throw new RuntimeException("Host activity must implement com.digbuzzi.ui.fragment.placeofinterest.common.IProviePlaces interface");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_post, container, false);
        ButterKnife.bind(this, view);

        init();
        return view;
    }

    private void init() {
        shoutPicture.setVisibility(View.INVISIBLE);
        ivUploadStatus.setVisibility(View.INVISIBLE);

        ivPhotoPreview.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                ivPhotoPreview.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                width = ivPhotoPreview.getWidth();
                height = ivPhotoPreview.getHeight();
            }
        });

        // ATTENTION: This "addApi(AppIndex.API)"was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        mGoogleApiClient = new GoogleApiClient.Builder(getContext())
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        if (!resolvingError) {
            mGoogleApiClient.connect();
        }

        prepareCurrentLocationRequest();

        preparePlaceList();
    }

    private void preparePlaceList() {
        places = iProvidePlaces.getPlaces();

        PlaceOfInterestTypeAdapter adapter =
                new PlaceOfInterestTypeAdapter(getContext(), places);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);

        rvPlaceList.setLayoutManager(layoutManager);
        rvPlaceList.setAdapter(adapter);
    }

    private void prepareCurrentLocationRequest() {
        Log.d(TAG,"Current location picker called");
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(5000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setNumUpdates(1);
    }

    @Override
    public void onStart() {
        EventBus.getDefault().register(this);
        super.onStart();
    }

    @Override
    public void onStop() {
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, mLocationListener);
            mGoogleApiClient.disconnect();
        }
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @SuppressWarnings("unused")
    public void onEventMainThread(PlaceOfInterestSelectionEvent event) {
        for (PlaceOfInterestType place : places) {
            if (place.isSelected()) {
                Toast.makeText(getContext(), place.getPlaceName(), Toast.LENGTH_LONG).show();
                shoutText.setText(place.getPlaceName());
                changeBuzzColor();
                break;
            }
        }
    }

    private void changeBuzzColor() {
        if (isAnyPlaceSelected() && location.getLatitude() != 0 && !shoutText.getText().toString().trim().isEmpty()) {
            ivSend.setTextColor(ContextCompat.getColor(getContext(), R.color.accent));
        }
    }

    @OnClick(R.id.ivLocation)
    public void openPlacePicker() {
        ivUploadStatus.setVisibility(View.INVISIBLE);
        PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
        try {
            startActivityForResult(builder.build(getActivity()), PLACE_PICKER_REQUEST);
        } catch (GooglePlayServicesRepairableException e) {
            e.printStackTrace();
        } catch (GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }
    }

    @OnClick(R.id.ivCamera)
    public void takePicture() {
        if (PermissionUtils.hasPermission(getContext(), Manifest.permission.READ_EXTERNAL_STORAGE)) {
            captureImage();
        } else {
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                    Manifest.permission.READ_EXTERNAL_STORAGE)) {
                Toast.makeText(getActivity(),
                        "We need this permission to capture photo",
                        Toast.LENGTH_SHORT)
                        .show();
            } else {
                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                        MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE_FOR_CAMERA);
            }
        }
    }

    @OnClick(R.id.ivPhotoPicker)
    public void pickPhoto() {
        if (PermissionUtils.hasPermission(getContext(), Manifest.permission.READ_EXTERNAL_STORAGE)) {
            pickImage();
        } else {
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                    Manifest.permission.READ_EXTERNAL_STORAGE)) {
                Toast.makeText(getActivity(),
                        "We need this permission to pick image from gallery",
                        Toast.LENGTH_SHORT)
                        .show();
            } else {
                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                        MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE_FOR_GALLERY);
            }
        }
    }

    @OnClick(R.id.ivCancelPreview)
    public void cancelPreview() {
        shoutPicture.setVisibility(View.INVISIBLE);
        ivPhotoPreview.setImageBitmap(null);
        image = null;
    }


    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";

        File storageDir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES);

        image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir/* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == PLACE_PICKER_REQUEST) {
                Place place = PlacePicker.getPlace(data, getContext());

                locationName = (String) place.getName();
                ivLocationName.setText(locationName);
                location.setLatitude(place.getLatLng().latitude);
                location.setLongitude(place.getLatLng().longitude);
                location.setPlace(locationName);
                location.setCity(place.getAddress().toString());
                changeBuzzColor();

            } else if (requestCode == IMAGE_PICKER_REQUEST) {
                shoutPicture.setVisibility(View.VISIBLE);
                try {
                    pickPic(data);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else if (requestCode == REQUEST_CHECK_SETTINGS) {
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        initRequest();
                        break;
                    case Activity.RESULT_CANCELED:
                        break;
                    default:
                        break;
                }
            } else {
                shoutPicture.setVisibility(View.VISIBLE);
                try {
                    setPic();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void pickPic(Intent intent) throws IOException {
        String[] filePathColumn = {MediaStore.Images.Media.DATA};

        // Get the cursor
        Cursor cursor = getActivity().getContentResolver().query(intent.getData(),
                filePathColumn, null, null, null);
        // Move to first row
        cursor.moveToFirst();

        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
        String imgDecodableString = cursor.getString(columnIndex);
        cursor.close();
        showAndCompressImage(imgDecodableString);
    }


    private void setPic() throws IOException {
        showAndCompressImage(mCurrentPhotoPath);
    }

    private void showAndCompressImage(String filePath) {
        image = new File(filePath);
        Bitmap compressedBitmap = new Compressor.Builder(getContext())
                .setMaxWidth(AppConfig.DIMENSION_WIDTH)
                .setMaxHeight(AppConfig.DIMENSION_HEIGHT)
                .setCompressFormat(Bitmap.CompressFormat.JPEG)
                .setQuality(100)
                .build()
                .compressToBitmap(image);

        FileOutputStream out = null;
        try {
            out = new FileOutputStream(image);
            compressedBitmap.compress(Bitmap.CompressFormat.WEBP, AppConfig.IMAGE_QUALITY, out);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        ivPhotoPreview.setImageBitmap(compressedBitmap);
        Log.d(TAG, "After compression: " + compressedBitmap.getByteCount());
    }


    @OnClick(R.id.ivSend)
    public void makeshout() {
        if (!isAnyPlaceSelected()) {
            Toast.makeText(getContext(), "Please, select a place type", Toast.LENGTH_SHORT).show();
        } else if (shoutText.getText().toString().trim().isEmpty()) {
            Toast.makeText(getContext(), "Place title cannot be empty", Toast.LENGTH_SHORT).show();
        } else if (location.getLatitude() == 0) {
            Toast.makeText(getContext(), "Location cannot be empty", Toast.LENGTH_SHORT).show();
        } else {
            addShout();
            notification();
            EventBus.getDefault().post(new PostingPlaceOfInterestInitiatedEvent());
        }

    }


    public void addShout() {
        ivUploadStatus.setVisibility(View.VISIBLE);
        Calendar calendar = Calendar.getInstance();
        long seconds = calendar.getTimeInMillis();

        String ApiToken = User.getInstance().getAccessToken();

        location.setPlace(ivLocationName.getText().toString());

        Log.d("Log", ApiToken.toString());

        if (image == null) {
            typedImage = null;
        } else {
            typedImage = new TypedFile("multipart/form-data", image);
        }


        String placeTitle = shoutText.getText().toString();

        String apiToken = User.getInstance().getAccessToken();
        RestAdapterProvider.getProvider().getRestApiForRetrofit1()
                .addPlace(apiToken, placeTitle, "", getPlaceTypes(),
                        typedImage, location, new Callback<PlaceOfInterest>() {
            @Override
            public void success(PlaceOfInterest placeOfInterest, Response response) {
                if (response.getStatus() == 200) {
                    String placeId = placeOfInterest.getPlaceId();
                    Log.d(TAG, placeId);

                    ivUploadStatus.setVisibility(View.INVISIBLE);
                    EventBus.getDefault().post(new PlacePostedEvent());
                    loading_flag = 1;
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (error.getMessage() != null) {
                    Log.d(TAG, error.getMessage());
                }
            }
        });
    }

    public void notification() {
        final int id = 1;

        mNotifyManager =
                (NotificationManager) getContext().getSystemService(Context.NOTIFICATION_SERVICE);
        mBuilder = new NotificationCompat.Builder(getContext());
        mBuilder.setContentTitle("Posting place")
                .setContentText("Posting place in progress")
                .setSmallIcon(R.mipmap.ic_launcher);
// Start a lengthy operation in a background thread
        new Thread(
                new Runnable() {
                    @Override
                    public void run() {
                        int incr = 20;
                        do {
                            // Sets the progress indicator to a max value, the
                            // current completion percentage, and "determinate"
                            // state
                            mBuilder.setProgress(100, incr, false);
                            // Displays the progress bar for the first time.
                            mNotifyManager.notify(id, mBuilder.build());
                            // Sleeps the thread, simulating an operation
                            // that takes time
                            try {
                                // Sleep for 5 seconds
                                Thread.sleep(1 * 1000);
                            } catch (InterruptedException e) {
                                Log.d("Notification", "sleep failure");
                            }
                            incr += 5;
                        } while (loading_flag == 0);
                        // When the loop is finished, updates the notification
                        mBuilder.setContentText("Buzz completed")
                                // Removes the progress bar
                                .setProgress(0, 0, false);
                        mNotifyManager.notify(id, mBuilder.build());
                    }
                }
// Starts the thread by calling the run() method in its Runnable
        ).start();
    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE_FOR_GALLERY: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    pickImage();
                } else {
                    Toast.makeText(getContext(), "Sorry, we cannot pick image without your permission", Toast.LENGTH_SHORT).show();
                }
                return;
            }
            case MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE_FOR_CAMERA: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    captureImage();
                } else {
                    Toast.makeText(getContext(), "Sorry, we cannot capture photo without your permission", Toast.LENGTH_SHORT).show();
                }
                return;
            }
            case PERMISSION_REQUEST_FINE_LOCATION: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    initRequest();
                }
                return;
            }
        }
    }

    private void pickImage() {
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, IMAGE_PICKER_REQUEST);
    }

    private void captureImage() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getContext().getPackageManager()) != null) {
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException e) {
                e.printStackTrace();
            }

            if (photoFile != null) {
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
            }
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        googleApiClientConnected = true;
        requestCurrentLocation();
    }

    @Override
    public void onConnectionSuspended(int i) {
        googleApiClientConnected = false;
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult result) {
        googleApiClientConnected = false;

        if (!resolvingError) {
            if (result.hasResolution()) {
                try {
                    resolvingError = true;
                    result.startResolutionForResult(getActivity(), REQUEST_RESOLVE_ERROR);
                } catch (IntentSender.SendIntentException e) {
                    // There was an error with the resolution intent. Try again.
                    mGoogleApiClient.connect();
                }
            } else {
                // Show dialog using GoogleApiAvailability.getErrorDialog()
                Log.d(TAG, "Auto location detection failed");
                resolvingError = true;
            }
        }
    }

    private void requestCurrentLocation() {
        if (PermissionUtils.hasPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION)) {
            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                    .addLocationRequest(mLocationRequest);
            PendingResult<LocationSettingsResult> result =
                    LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
            result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
                @Override
                public void onResult(LocationSettingsResult result) {
                    final Status status = result.getStatus();
                    switch (status.getStatusCode()) {
                        case LocationSettingsStatusCodes.SUCCESS:
                            initRequest();
                            break;
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            // Location settings are not satisfied. But could be fixed by showing the user
                            // a dialog.
                            try {
                                // Show the dialog by calling startResolutionForResult(),
                                // and check the result in onActivityResult().
                                status.startResolutionForResult(
                                        getActivity(),
                                        REQUEST_CHECK_SETTINGS);
                            } catch (IntentSender.SendIntentException ignored) {
                            }
                            break;
                    }
                }
            });
        } else {
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                    Manifest.permission.ACCESS_FINE_LOCATION)) {
                Toast.makeText(getContext(),
                        "Auto location detection won't work without this permission",
                        Toast.LENGTH_SHORT)
                        .show();
            } else {
                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        PERMISSION_REQUEST_FINE_LOCATION);
            }
        }
    }

    @SuppressWarnings("MissingPermission")
    private void initRequest() {
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, mLocationListener);
    }

    private LocationListener mLocationListener = new LocationListener() {
        @Override
        public void onLocationChanged(android.location.Location loc) {
            location.setLatitude(loc.getLatitude());
            location.setLongitude(loc.getLongitude());

            String place = "(" + loc.getLatitude() + ", " + loc.getLongitude() + ")";

            location.setPlace(place);
            ivLocationName.setText(place);
        }
    };

    public boolean isAnyPlaceSelected() {
        for (PlaceOfInterestType place : places) {
            if (place.isSelected()) {
                return true;
            }
        }
        return false;
    }

    public String getPlaceTypes() {
        for (PlaceOfInterestType place : places) {
            if (place.isSelected()) {
                return place.getPlaceType();
            }
        }
        throw new RuntimeException("No place is selected");
    }
}