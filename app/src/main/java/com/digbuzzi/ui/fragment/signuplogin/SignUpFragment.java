package com.digbuzzi.ui.fragment.signuplogin;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.util.Patterns;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.digbuzzi.R;
import com.digbuzzi.Util.UserUtil;
import com.digbuzzi.api.RestAdapterProvider;
import com.digbuzzi.app.AppConfig;
import com.digbuzzi.event.LogInRequestEvent;
import com.digbuzzi.model.User;
import com.digbuzzi.model.restmodel.AuthResponse;
import com.digbuzzi.model.restmodel.RegisterRequest;
import com.digbuzzi.event.NewlyRegisteredEvent;
import com.github.ybq.android.spinkit.SpinKitView;

import de.greenrobot.event.EventBus;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link SignUpFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SignUpFragment extends Fragment {
    private static final String TAG = SignUpFragment.class.getSimpleName();

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    @Bind(R.id.etUserName)
    EditText etUserName;

    @Bind(R.id.etEmail)
    EditText etEmail;

    @Bind(R.id.etPassword)
    EditText etPassword;

    @Bind(R.id.etConfirmPassword)
    EditText etConfirmPassword;

    @Bind(R.id.ivSignInSatus)
    ProgressBar ivSignInSatus;

    @Bind(R.id.spin_kit)
    SpinKitView spinKitView;

    @Bind(R.id.tvSignUp)
    TextView tvSignUp;

    private boolean isRegistering;

    public SignUpFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment SignUpFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static SignUpFragment newInstance(String param1, String param2) {
        SignUpFragment fragment = new SignUpFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.signup_fragment, container, false);
        ButterKnife.bind(this, view);
        ivSignInSatus.setVisibility(View.INVISIBLE);

        etConfirmPassword.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                int result = actionId & EditorInfo.IME_MASK_ACTION;
                if (result == EditorInfo.IME_ACTION_DONE) {
                    onSignUpClick();
                    return true;
                }
                return false;
            }
        });
        return view;
    }

    @OnClick(R.id.ll_log_in_request)
    public void openLogInScreen() {
        EventBus.getDefault().post(new LogInRequestEvent());
    }

    @OnClick(R.id.tvSignUp)
    public void onSignUpClick() {
        if (isRegistering) return;

        isRegistering = true;

        tvSignUp.setVisibility(View.GONE);
        spinKitView.setVisibility(View.VISIBLE);

        ivSignInSatus.setVisibility(View.INVISIBLE);
        final String userName = etUserName.getText().toString();
        final String userEmail = etEmail.getText().toString();
        final String userPassword = etPassword.getText().toString();
        final String userConfirmPassword = etConfirmPassword.getText().toString();

        boolean hasError = false;

        if (!UserUtil.validUsername(userName)) {
            String msg = "Username can only contain letter, digit and dot(.)";
            if (userName.trim().isEmpty()) {
                msg = "Username cannot be empty";
            } else if (userName.trim().length() < 6) {
                msg = "Username must be at least " + AppConfig.USERNAME_MIN_LENGTH + " characters";
            } else if (Character.isDigit(userName.trim().toCharArray()[0])) {
                msg = "First letter in username cannot be a number";
            } else if (userName.trim().endsWith(".")) {
                msg = "Username cannot end with dot(.)";
            }
            Toast.makeText(getContext(), msg, Toast.LENGTH_LONG).show();
            hasError = true;
        }

        if (!hasError && (userEmail.trim().isEmpty() || !Patterns.EMAIL_ADDRESS.matcher(userEmail).matches())) {
            Toast.makeText(getContext(), "Please, provide a valid email address", Toast.LENGTH_LONG).show();
            hasError = true;
        }

        if(!hasError && (userPassword.length() < 6 || !userPassword.equals(userConfirmPassword))){
            String msg;
            if (userPassword.length() < 6) {
                msg = "Password is less than 6 characters";
            } else {
                msg = "Password doesn't match";
            }
            Toast.makeText(getContext(), msg,Toast.LENGTH_LONG).show();
            hasError = true;
        }

        if (hasError) {
            isRegistering = false;
            tvSignUp.setVisibility(View.VISIBLE);
            spinKitView.setVisibility(View.GONE);
            return;
        }

        RegisterRequest registerRequest = new RegisterRequest();
        registerRequest.userName = userName;
        registerRequest.email = userEmail;
        registerRequest.password = userPassword;

        RestAdapterProvider.getProvider().getRestApiForRetrofit1().register(registerRequest, new Callback<AuthResponse>() {
            @Override
            public void success(AuthResponse authResponse, Response response) {
                isRegistering = false;

                String token = authResponse.getToken();
                if (token == null) return;

                User.getInstance().initiateUser(userEmail, authResponse);

                Log.d(TAG, User.getInstance().getAccessToken());
                ivSignInSatus.setVisibility(View.INVISIBLE);
                EventBus.getDefault().post(new NewlyRegisteredEvent());
            }

            @Override
            public void failure(RetrofitError error) {
                tvSignUp.setVisibility(View.VISIBLE);
                spinKitView.setVisibility(View.GONE);

                isRegistering = false;

                if (error.getResponse() != null) {
                    Log.d(SignUpFragment.class.getSimpleName(), error.getResponse().toString());
                }

                int statusCode = error.getResponse().getStatus();

                if (statusCode == 409) {
                    Toast.makeText(getActivity(), "username or email address already registered", Toast.LENGTH_LONG).show();
                }

                Toast.makeText(getContext(), "Error occurred! Please, try again", Toast.LENGTH_LONG).show();
            }
        });
    }
}