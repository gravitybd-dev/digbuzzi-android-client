package com.digbuzzi.ui.fragment;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.digbuzzi.R;
import com.digbuzzi.adapter.ShoutFeedAdapter;
import com.digbuzzi.api.RestAdapterProvider;
import com.digbuzzi.app.AppConfig;
import com.digbuzzi.model.User;
import com.digbuzzi.model.restmodel.ShoutFeed;
import com.digbuzzi.repo.BuzzRepository;
import com.digbuzzi.ui.EndlessRecyclerOnScrollListener;
import com.digbuzzi.ui.fragment.shoutfeed.NowFeedFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by U on 6/23/2016.
 */

public class BottomFeedDialogFragment extends BottomSheetDialogFragment {

    public static final String TAG = BottomFeedDialogFragment.class.getSimpleName();

    @Bind(R.id.rvAllFeed)
    RecyclerView rvAllFeed;

    @Bind(R.id.statusContainer)
    View statusContainer;

    @Bind(R.id.tvStatus)
    TextView tvStatus;

    @Bind(R.id.btnRetry)
    Button btnRetry;

    @Bind(R.id.loading)
    View loading;

    ShoutFeedAdapter adapter;

    private BuzzRepository repository = BuzzRepository.getInstance();

    public static BottomFeedDialogFragment newInstance(ArrayList<String> buzzIds) {
        BottomFeedDialogFragment fragment = new BottomFeedDialogFragment();

        Bundle bundle = new Bundle();
        bundle.putStringArrayList("feedIds", buzzIds);
        fragment.setArguments(bundle);

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_bottom_feed, container, false);
        ButterKnife.bind(this, view);

        adapter = new ShoutFeedAdapter(getContext());
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);

        rvAllFeed.setLayoutManager(layoutManager);
        rvAllFeed.setAdapter(adapter);

        loadData();
        return view;
    }

    @OnClick(R.id.btnRetry)
    public void loadData() {
        loading.setVisibility(View.VISIBLE);
        if (adapter != null) {
            adapter.setFeeds(new ArrayList<ShoutFeed>());
            adapter.notifyDataSetChanged();
        }
        refreshFeeds(getArguments().getStringArrayList("feedIds"));
    }

    @Override
    public void onResume() {
        adapter.notifyDataSetChanged();
        super.onResume();
    }

    private void refreshFeeds(List<String> feedIds) {
        loading.setVisibility(View.VISIBLE);
        statusContainer.setVisibility(View.GONE);
        String accessToken = User.getInstance().getAccessToken();
        RestAdapterProvider.getProvider().getRestApiForRetrofit1().singleBuzz(accessToken, feedIds.get(0), new Callback<ShoutFeed>() {
            @Override
            public void success(ShoutFeed shoutFeed, Response response) {
                loading.setVisibility(View.GONE);
                if (shoutFeed != null) {
                    repository.removeBuzzes(TAG);
                    repository.saveBuzz(TAG, shoutFeed);
                    List<ShoutFeed> feeds = repository.getBuzzes(TAG);
                    adapter.setFeeds(feeds);
                    adapter.notifyDataSetChanged();
                }

                if (adapter.getItemCount() == 0) {
                    statusContainer.setVisibility(View.VISIBLE);
                    tvStatus.setText(R.string.no_feed_msg);
                    btnRetry.setText(R.string.refresh);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                loading.setVisibility(View.GONE);
                if (adapter.getItemCount() == 0) {
                    statusContainer.setVisibility(View.VISIBLE);
                    tvStatus.setText(R.string.feed_fetch_error);
                    btnRetry.setText(R.string.retry);
                }
            }
        });
    }
}