package com.digbuzzi.ui.fragment.placeofinterest.common;

import com.digbuzzi.model.PlaceOfInterestType;

import java.util.List;

/**
 * Created by Joker on 7/22/16.
 */
public interface IProvidePlaces {
    List<PlaceOfInterestType> getPlaces();
}