package com.digbuzzi.ui.fragment.placeofinterest;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.digbuzzi.PlaceOfInterestHelper;
import com.digbuzzi.R;
import com.digbuzzi.adapter.PlaceGroupAdapter;
import com.digbuzzi.app.AppConfig;
import com.digbuzzi.model.PlaceOfInterestGroup;
import com.google.gson.Gson;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import butterknife.Bind;
import butterknife.ButterKnife;

public class PlaceGroupFragment extends Fragment {

    @Bind(R.id.rvPlaceGroup)
    RecyclerView rvPlaceGroup;

    public PlaceGroupFragment() {
    }

    public static PlaceGroupFragment newInstance() {
        return new PlaceGroupFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_place_group, container, false);
        ButterKnife.bind(this, view);
        populateUi();
        return view;
    }

    private void populateUi() {
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(), 2);
        List<PlaceOfInterestGroup> groups = PlaceOfInterestHelper.loadPlaceGroups(getContext());
        PlaceGroupAdapter adapter = new PlaceGroupAdapter(getContext(), groups);
        rvPlaceGroup.setLayoutManager(gridLayoutManager);
        rvPlaceGroup.setAdapter(adapter);
    }
}