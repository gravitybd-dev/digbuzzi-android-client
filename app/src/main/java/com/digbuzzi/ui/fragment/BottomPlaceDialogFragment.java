package com.digbuzzi.ui.fragment;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v4.app.ShareCompat;
import android.support.v7.app.AppCompatDelegate;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.digbuzzi.PlaceOfInterestHelper;
import com.digbuzzi.R;
import com.digbuzzi.Util.PlaceOfInterestUtil;
import com.digbuzzi.api.RestAdapterProvider;
import com.digbuzzi.databinding.PlaceOfInterestDetailItemViewBinding;
import com.digbuzzi.event.BuzzUpdated;
import com.digbuzzi.model.PlaceOfInterestGroup;
import com.digbuzzi.model.User;
import com.digbuzzi.model.restmodel.Like;
import com.digbuzzi.model.restmodel.LikeDislikeRequest;
import com.digbuzzi.model.restmodel.PlaceOfInterest;
import com.digbuzzi.repo.BuzzRepository;
import com.digbuzzi.ui.CommentActivity;
import com.digbuzzi.ui.LikerActivity;

import java.util.List;

import butterknife.OnClick;
import de.greenrobot.event.EventBus;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by U on 6/23/2016.
 */

public class BottomPlaceDialogFragment extends BottomSheetDialogFragment {

    public static final String TAG = BottomPlaceDialogFragment.class.getSimpleName();

    PlaceOfInterestDetailItemViewBinding binding;

    private boolean likeInProgress;

    private List<PlaceOfInterestGroup> groups;

    // Enable vector drawable compat
    // Set it to true where you want to use vector drawable compat
    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    private BuzzRepository repository = BuzzRepository.getInstance();

    public static BottomPlaceDialogFragment newInstance(String placeId) {
        BottomPlaceDialogFragment fragment = new BottomPlaceDialogFragment();

        Bundle bundle = new Bundle();
        bundle.putString("placeId", placeId);
        fragment.setArguments(bundle);

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.place_of_interest_detail_item_view, container, false);
        binding.setHandler(new EventHandler());
        binding.setLikeInProgress(likeInProgress);
        binding.setIcon(0);

        groups = PlaceOfInterestHelper.loadPlaceGroups(getContext());

        loadData();

        return binding.getRoot();
    }

    public void loadData() {
        binding.setLoading(true);
        binding.setRetry(false);
        refreshPlaceInfo(User.getInstance().getAccessToken(), getArguments().getString("placeId"));
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void refreshPlaceInfo(String accessToken, String placeId) {
        binding.loading.setVisibility(View.VISIBLE);
        binding.statusContainer.setVisibility(View.GONE);
        RestAdapterProvider.getProvider().getRestApiForRetrofit1().getPlaceOfInterestById(accessToken, placeId, new Callback<PlaceOfInterest>() {
            @Override
            public void success(PlaceOfInterest placeOfInterest, Response response) {
                binding.setLoading(false);
                binding.setRetry(false);
                int resourceIdentifier = getImageDrawableResourceIdByImageName(getContext(),
                        PlaceOfInterestHelper.getImageNameForPlaceType(groups, placeOfInterest.getPlaceTypes().get(0)));
                binding.setPlaceOfInterest(placeOfInterest);
                binding.setIcon(resourceIdentifier);
            }

            @Override
            public void failure(RetrofitError error) {
                binding.setLoading(false);
                binding.setRetry(true);
            }
        });
    }

    private int getImageDrawableResourceIdByImageName(Context context, String imageName) {
        return context.getResources().getIdentifier(imageName, "drawable", context.getPackageName());
    }

    public void processLikeDislike(final PlaceOfInterest placeOfInterest) {
        if (likeInProgress) return;

        LikeDislikeRequest likeDislikeRequest = new LikeDislikeRequest();
        likeDislikeRequest.id = placeOfInterest.getPlaceId();
        likeDislikeRequest.time = System.currentTimeMillis();

        likeInProgress = true;
        binding.setLikeInProgress(likeInProgress);

        RestAdapterProvider.getProvider().getRestApiForRetrofit1().likeDislikePlace(User.getInstance().getAccessToken(), placeOfInterest.getPlaceId(), likeDislikeRequest, new Callback<Like>() {
            @Override
            public void success(Like like, Response response) {
                likeInProgress = false;
                binding.setLikeInProgress(likeInProgress);

                if (placeOfInterest.isLikedByUser()) {
                    placeOfInterest.setLikeCount(placeOfInterest.getLikeCount() - 1);
                } else {
                    placeOfInterest.setLikeCount(placeOfInterest.getLikeCount() + 1);
                }

                placeOfInterest.setLikedByUser(!placeOfInterest.isLikedByUser());

                binding.setPlaceOfInterest(placeOfInterest);
            }

            @Override
            public void failure(RetrofitError error) {
                likeInProgress = false;
                binding.setLikeInProgress(likeInProgress);
                String msg;
                if (placeOfInterest.isLikedByUser()) {
                    msg = "couldn't dislike the place";
                } else {
                    msg = "couldn't like the place";
                }
                Toast.makeText(getContext(), msg, Toast.LENGTH_LONG).show();
                Log.d(TAG, error.getMessage());
            }
        });
    }

    public class EventHandler {
        public void retry() {
            loadData();
        }

        public void like(PlaceOfInterest placeOfInterest) {
            processLikeDislike(placeOfInterest);
        }

        public void showLikes() {
            Intent intent = new Intent(getContext(), LikerActivity.class);
            intent.putExtra(LikerActivity.EXTRA_LIKE_TYPE, LikerActivity.LIKE_TYPE_PLACE);
            intent.putExtra("placeId", getArguments().getString("placeId"));
            startActivity(intent);
        }

        public void showComments() {
            Intent intent = new Intent(getContext(), CommentActivity.class);
            intent.putExtra(CommentActivity.EXTRA_COMMENT_TYPE, CommentActivity.COMMENT_TYPE_PLACE);
            intent.putExtra("placeId", getArguments().getString("placeId"));
            startActivity(intent);
        }

        public void share(PlaceOfInterest placeOfInterest) {
            Intent intent = ShareCompat.IntentBuilder
                    .from(getActivity())
                    .setType("text/plain")
                    .setText(placeOfInterest.getSharableLink())
                    .setChooserTitle("Share place using").getIntent();
            startActivity(Intent.createChooser(intent, "Share place using"));
        }

        public void navigate(PlaceOfInterest placeOfInterest) {
            if (!PlaceOfInterestUtil.isGoogleMapInstalled(getActivity().getPackageManager())) {
                Toast.makeText(getContext(), "Please, install Google Map first", Toast.LENGTH_SHORT).show();
                return;
            }

            double latitude = placeOfInterest.getLocation().getLatitude();
            double longitude = placeOfInterest.getLocation().getLongitude();
            Uri gmmIntentUri = Uri.parse("google.navigation:q=" + latitude + "," + longitude);
            Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
            mapIntent.setPackage("com.google.android.apps.maps");
            startActivity(mapIntent);
        }
    }
}