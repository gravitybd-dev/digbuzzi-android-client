package com.digbuzzi.ui.fragment.signuplogin;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.digbuzzi.R;
import com.digbuzzi.Util.UiUtil;
import com.digbuzzi.api.RestAdapterProvider;
import com.digbuzzi.event.LogInRequestEvent;
import com.digbuzzi.model.restmodel.ForgetPasswordResponse;
import com.github.ybq.android.spinkit.SpinKitView;

import de.greenrobot.event.EventBus;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class ForgetPasswordFragment extends Fragment {
    private static final String TAG = ForgetPasswordFragment.class.getSimpleName();

    @Bind(R.id.fieldHolder)
    View fieldHolder;

    @Bind(R.id.requestPass)
    View requestPass;

    @Bind(R.id.tvRequestPass)
    TextView tvRequestPass;

    @Bind(R.id.tvPrompt)
    TextView tvPrompt;

    @Bind(R.id.etEmail)
    EditText etEmail;

    @Bind(R.id.spin_kit)
    SpinKitView spinKitView;

    private boolean isBusy;

    public ForgetPasswordFragment() {
        // Required empty public constructor
    }

    public static ForgetPasswordFragment newInstance() {
        return new ForgetPasswordFragment();
    }

    @OnClick(R.id.ll_log_in_request)
    public void openLogInScreen() {
        EventBus.getDefault().post(new LogInRequestEvent());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_forget_password, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @OnClick(R.id.requestPass)
    public void requestPass(){
        if (isBusy) return;

        String userEmail = etEmail.getText().toString().trim();
        if (userEmail.isEmpty()){
            Toast.makeText(getContext(),"Email address cannot be empty",Toast.LENGTH_SHORT).show();
            return;
        }
        if (!Patterns.EMAIL_ADDRESS.matcher(userEmail).matches()) {
            Toast.makeText(getContext(),"Please, type a valid email address",Toast.LENGTH_SHORT).show();
            return;
        }

        UiUtil.hideKeyboard(getContext(), etEmail);

        isBusy = true;

        tvRequestPass.setVisibility(View.GONE);
        spinKitView.setVisibility(View.VISIBLE);

        RestAdapterProvider.getProvider().getRestApiForRetrofit1().forgetPass(userEmail, new Callback<ForgetPasswordResponse>() {

            @Override
            public void success(ForgetPasswordResponse forgetPasswordResponse, Response response) {
                if (response.getStatus() == 200) {
                    fieldHolder.setVisibility(View.INVISIBLE);
                    tvPrompt.setVisibility(View.VISIBLE);
                } else if (response.getStatus() == 400) {
                    tvRequestPass.setVisibility(View.VISIBLE);
                    spinKitView.setVisibility(View.GONE);
                    Toast.makeText(getContext(), "Couldn't find you email", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                tvRequestPass.setVisibility(View.VISIBLE);
                spinKitView.setVisibility(View.GONE);
                Toast.makeText(getContext(), "Sorry, something happened. Please, try again", Toast.LENGTH_LONG).show();
            }
        });
    }
}