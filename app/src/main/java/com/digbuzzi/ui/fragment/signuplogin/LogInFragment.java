package com.digbuzzi.ui.fragment.signuplogin;


import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.digbuzzi.R;
import com.digbuzzi.api.RestAdapterProvider;
import com.digbuzzi.app.AppConfig;
import com.digbuzzi.event.ForgetPasswordEvent;
import com.digbuzzi.event.SignUpRequestEvent;
import com.digbuzzi.event.UserAuthorizedEvent;
import com.digbuzzi.model.User;
import com.digbuzzi.model.restmodel.AuthResponse;
import com.digbuzzi.model.restmodel.LogInRequest;
import com.digbuzzi.ui.MapActivity;
import com.github.ybq.android.spinkit.SpinKitView;

import org.w3c.dom.Text;

import de.greenrobot.event.EventBus;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link LogInFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class LogInFragment extends Fragment {
    private static final String TAG = LogInFragment.class.getSimpleName();

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    @Bind(R.id.etIdentity)
    EditText etIdentity;

    @Bind(R.id.etPassword)
    EditText etPassword;


    @Bind(R.id.tvForgetPassword)
    TextView tvForgetPassword;

    @Bind(R.id.spin_kit)
    SpinKitView spinKitView;

    @Bind(R.id.tvSignIn)
    TextView tvSignIn;

    boolean isSigningIn;

    public LogInFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment LogInFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static LogInFragment newInstance(String param1, String param2) {
        LogInFragment fragment = new LogInFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

        RestAdapterProvider.getProvider().getRestApiForRetrofit1();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.login_fragment, container, false);
        ButterKnife.bind(this, view);

        etPassword.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                int result = actionId & EditorInfo.IME_MASK_ACTION;
                if (result == EditorInfo.IME_ACTION_DONE) {
                    onSignInClick();
                    return true;
                }
                return false;
            }
        });
        return view;
    }

    @OnClick(R.id.ll_sign_up_request)
    public void openSignUpScreen() {
        EventBus.getDefault().post(new SignUpRequestEvent());
    }

    @OnClick(R.id.tvExplore)
    public void explore() {
        Intent intent = new Intent(getContext(), MapActivity.class);
        startActivity(intent);
    }
    @OnClick(R.id.tvForgetPassword)
    public void onForgetPassword(){
        EventBus.getDefault().post(new ForgetPasswordEvent());
    }

    @OnClick(R.id.tvSignIn)
    public void onSignInClick() {
        if (isSigningIn) return;

        isSigningIn = true;

        tvSignIn.setVisibility(View.GONE);
        spinKitView.setVisibility(View.VISIBLE);

        final String identity = etIdentity.getText().toString();
        final String password = etPassword.getText().toString();

        boolean hasError = false;
        if (identity.trim().isEmpty() || password.trim().isEmpty()) {
            Toast.makeText(getContext(), "Username or password cannot be empty", Toast.LENGTH_SHORT).show();
            hasError = true;
        }

        if (hasError) {
            isSigningIn = false;
            tvSignIn.setVisibility(View.VISIBLE);
            spinKitView.setVisibility(View.GONE);
            return;
        }

        LogInRequest request = new LogInRequest();
        request.identity = identity;
        request.password = password;

        RestAdapterProvider.getProvider().getRestApiForRetrofit1().login(request, new Callback<AuthResponse>() {
            @Override
            public void success(AuthResponse authResponse, Response response) {
                isSigningIn = false;

                String token = authResponse.getToken();
                if (token == null) return;

                User.getInstance().initiateUser(identity, authResponse);

                Log.d(TAG, User.getInstance().getAccessToken());
                EventBus.getDefault().post(new UserAuthorizedEvent());
            }

            @Override
            public void failure(RetrofitError error) {
                tvSignIn.setVisibility(View.VISIBLE);
                spinKitView.setVisibility(View.GONE);

                isSigningIn = false;
                Toast.makeText(getContext(), "User name or password incorrect", Toast.LENGTH_LONG).show();
                Log.d(LogInFragment.class.getSimpleName(), error.getMessage());
            }
        });
    }
}