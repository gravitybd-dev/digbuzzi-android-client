package com.digbuzzi.ui.fragment;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.digbuzzi.R;
import com.digbuzzi.adapter.NotificationAdapter;
import com.digbuzzi.api.RestAdapterProvider;
import com.digbuzzi.app.AppConfig;
import com.digbuzzi.event.NewContentAvailableEvent;
import com.digbuzzi.event.ScrollToTopEvent;
import com.digbuzzi.model.User;
import com.digbuzzi.model.restmodel.Notification;
import com.digbuzzi.ui.EndlessRecyclerOnScrollListener;
import de.greenrobot.event.EventBus;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link NotificationFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class NotificationFragment extends Fragment {
    public static final String TITLE = "NOTIFICATION";
    private static final String TASK_NEW_CONTENT_AVAILABILITY_FOR_NOW = "task_new_content_availability_for_now";
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    @Bind(R.id.rvFeed)
    RecyclerView rvNotificationFeed;

    @Bind(R.id.srl)
    SwipeRefreshLayout srl;

    @Bind(R.id.statusContainer)
    View statusContainer;

    @Bind(R.id.tvStatus)
    TextView tvStatus;

    @Bind(R.id.btnRetry)
    Button btnRetry;

    List<Notification> mNotifications;
    NotificationAdapter adapter;

    EndlessRecyclerOnScrollListener endlessRecyclerOnScrollListener;

    //GcmNetworkManager mGcmNetworkManager;
    //PeriodicTask newContentAvailabilityPullTask;

    public NotificationFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment NowFeedFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static NotificationFragment newInstance(String param1, String param2) {
        NotificationFragment fragment = new NotificationFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

//        mGcmNetworkManager = GcmNetworkManager.getInstance(getContext());
//        newContentAvailabilityPullTask = new PeriodicTask.Builder()
//                .setPeriod(AppConfig.NEW_CONTENT_PULLING_PERIOD)
//                .setRequiredNetwork(Task.NETWORK_STATE_CONNECTED)
//                .setTag(TASK_NEW_CONTENT_AVAILABILITY_FOR_NOW)
//                .setService(NewContentCheckServiceForNow.class)
//                .build();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_notification, container, false);
        ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
        //mGcmNetworkManager.schedule(newContentAvailabilityPullTask);
    }

    @Override
    public void onPause() {
        EventBus.getDefault().unregister(this);
        //mGcmNetworkManager.cancelTask(TASK_NEW_CONTENT_AVAILABILITY_FOR_NOW, NewContentCheckServiceForNow.class);
        super.onPause();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);

        rvNotificationFeed.setLayoutManager(layoutManager);

        endlessRecyclerOnScrollListener = new EndlessRecyclerOnScrollListener(layoutManager) {
            @Override
            public void onLoadMore(int offset, int count) {
                loadMore(offset, count);
            }
        };

        rvNotificationFeed.addOnScrollListener(endlessRecyclerOnScrollListener);
        mNotifications = new ArrayList<>();
        adapter = new NotificationAdapter(getContext(), mNotifications);
        rvNotificationFeed.setAdapter(adapter);

        if (mNotifications.size() == 0) {
            srl.post(new Runnable() {
                @Override
                public void run() {
                    srl.setRefreshing(true);
                }
            });
            refreshFeeds();
        }

        srl.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshFeeds();
            }
        });
    }

    @OnClick(R.id.btnRetry)
    public void retry() {
        if (mNotifications.size() == 0) {
            srl.post(new Runnable() {
                @Override
                public void run() {
                    srl.setRefreshing(true);
                }
            });
            refreshFeeds();
        }

        srl.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshFeeds();
            }
        });
    }

    private void refreshFeeds() {
        statusContainer.setVisibility(View.GONE);

        RestAdapterProvider.getProvider().getRestApiForRetrofit1().userNotification(User.getInstance().getAccessToken(), 0, AppConfig.NOTIFICATION_COUNT_PER_REQUEST, new Callback<List<Notification>>() {
            @Override
            public void success(List<Notification> notifications, Response response) {
                srl.post(new Runnable() {
                    @Override
                    public void run() {
                        srl.setRefreshing(false);
                    }
                });
                if (notifications != null) {
                    endlessRecyclerOnScrollListener.reset(notifications.size());
                    mNotifications.clear();
                    mNotifications.addAll(notifications);
                    adapter.notifyDataSetChanged();
                }

                if (NotificationFragment.this.mNotifications.size() == 0) {
                    statusContainer.setVisibility(View.VISIBLE);
                    tvStatus.setText(R.string.no_notifications);
                    btnRetry.setText("REFRESH");
                }
            }

            @Override
            public void failure(RetrofitError error) {
                srl.post(new Runnable() {
                    @Override
                    public void run() {
                        srl.setRefreshing(false);
                    }
                });
                if (mNotifications.size() == 0) {
                    statusContainer.setVisibility(View.VISIBLE);
                    tvStatus.setText("Couldn't get the notifications");
                    btnRetry.setText("RETRY");
                }
            }
        });
    }

    private void loadMore(int offset, int count) {
        String token = User.getInstance().getAccessToken();
        RestAdapterProvider.getProvider().getRestApiForRetrofit1().userNotification(token, offset, count, new Callback<List<Notification>>() {
            @Override
            public void success(List<Notification> notifications, Response response) {
                Log.d(NotificationFragment.class.getSimpleName(), response.toString());
                if (notifications != null) {
                    mNotifications.addAll(notifications);
                    adapter.notifyDataSetChanged();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Log.d(NotificationFragment.class.getSimpleName(), error.getMessage());
            }
        });
    }

    @SuppressWarnings("unused")
    public void onEventMainThread(NewContentAvailableEvent event) {
        Toast.makeText(getContext(), "Content available", Toast.LENGTH_LONG).show();
    }

    @SuppressWarnings("unused")
    public void onEventMainThread(ScrollToTopEvent event) {
        if (rvNotificationFeed != null && event.getTitle().equals(TITLE)) {
            rvNotificationFeed.smoothScrollToPosition(0);
        }
    }
}