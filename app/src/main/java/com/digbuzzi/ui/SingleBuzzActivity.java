package com.digbuzzi.ui;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v4.app.ShareCompat;
import android.support.v4.app.TaskStackBuilder;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.digbuzzi.PrimaryNavigatorActivity;
import com.digbuzzi.R;
import com.digbuzzi.Util.NavigationUtil;
import com.digbuzzi.Util.TimeUtil;
import com.digbuzzi.api.RestAdapterProvider;
import com.digbuzzi.api.RestApi;
import com.digbuzzi.model.User;
import com.digbuzzi.model.restmodel.Like;
import com.digbuzzi.model.restmodel.LikeDislikeRequest;
import com.digbuzzi.model.restmodel.ShoutFeed;
import com.digbuzzi.repo.BuzzRepository;
import com.facebook.drawee.view.SimpleDraweeView;
import java.util.List;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

@SuppressWarnings("WeakerAccess")
public class SingleBuzzActivity extends AppCompatActivity {
    private static final String TAG = SingleBuzzActivity.class.getSimpleName();
    public static final String BUZZ_ID = "com.digbuzzi.buzz_id";

    // Loading related views
    @Bind(R.id.loadingBar) ProgressBar loadingBar;
    @Bind(R.id.statusContainer) View statusContainer;

    // Buzz related views
    @Bind(R.id.shout) View shout;
    @Bind(R.id.topIndicator) View topIndicator;
    @Bind(R.id.ivPropic) SimpleDraweeView ivPropic;
    @Bind(R.id.tvShoutUserName) TextView tvShoutUserName;
    @Bind(R.id.tvShoutUserLocation) TextView tvShoutUserLocation;
    @Bind(R.id.tvShoutTime) TextView tvShoutTime;
    @Bind(R.id.tvShout) TextView tvShout;
    @Bind(R.id.ivPhoto) SimpleDraweeView ivPhoto;
    @Bind(R.id.ivLike) ImageView ivLike;
    @Bind(R.id.likeLoading) View likeLoading;
    @Bind(R.id.tvLikeCount) TextView tvLikeCount;
    @Bind(R.id.tvCommentCount) TextView tvCommentCount;
    @Bind(R.id.tvShareCount) TextView tvShareCount;

    RestApi restApi;

    String buzzId;
    ShoutFeed buzz;

    boolean isShared = false;
    boolean isLikeProcessing;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_buzz);
        ButterKnife.bind(this);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        gotoLoginScreenIfNotLoggedIn();

        Uri uri = getIntent().getData();
        if (uri != null) {
            buzzId = uri.getEncodedQuery();
            isShared = true;
        } else {
            buzzId = getIntent().getStringExtra(BUZZ_ID);
        }

        init();
    }

    private void gotoLoginScreenIfNotLoggedIn() {
        if (TextUtils.isEmpty(User.getInstance().getAccessToken())) {
            finish();
            startActivity(new Intent(getApplicationContext(), PrimaryNavigatorActivity.class));
        }
    }

    private void init() {
        restApi = RestAdapterProvider.getProvider().getRestApiForRetrofit1();
    }

    @Override
    protected void onResume() {
        if (buzzId != null) {
            if (buzz != null) {
                populateUiWithBuzz(buzz);
            } else {
                fetchBuzz();
            }
        } else {
            fetchBuzz();
        }
        super.onResume();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent upIntent = NavUtils.getParentActivityIntent(this);
                if (NavUtils.shouldUpRecreateTask(this, upIntent)) {
                    // This activity is NOT part of this app's task, so create a new task
                    // when navigating up, with a synthesized back stack.
                    TaskStackBuilder.create(this)
                            // Add all of this activity's parents to the back stack
                            .addNextIntentWithParentStack(upIntent)
                            // Navigate up to the closest parent
                            .startActivities();
                } else {
                    // This activity is part of this app's task, so simply
                    // navigate up to the logical parent activity.
                    NavUtils.navigateUpTo(this, upIntent);
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void fetchBuzz() {
        if (buzzId == null) return;
        statusContainer.setVisibility(View.GONE);
        loadingBar.setVisibility(View.VISIBLE);
        String accessToken = User.getInstance().getAccessToken();
        Log.d(TAG, "Called Api");
        if (isShared) {
            restApi.singleSharedBuzz(accessToken, buzzId, shoutFeedCallback);
        } else {
            restApi.singleBuzz(accessToken, buzzId, shoutFeedCallback);
        }
    }

    private Callback<ShoutFeed> shoutFeedCallback = new Callback<ShoutFeed>() {
        @Override
        public void success(ShoutFeed shoutFeed, Response response) {
            loadingBar.setVisibility(View.GONE);
            if (response.getStatus() == 200) {
                BuzzRepository.getInstance().saveBuzz(TAG, shoutFeed);
                buzz = BuzzRepository.getInstance().getBuzz(shoutFeed.getShoutId());
                populateUiWithBuzz(shoutFeed);
                loadingBar.setVisibility(View.GONE);
            } else {
                loadingBar.setVisibility(View.GONE);
                statusContainer.setVisibility(View.VISIBLE);
            }
        }

        @Override
        public void failure(RetrofitError error) {
            loadingBar.setVisibility(View.GONE);
            statusContainer.setVisibility(View.VISIBLE);
        }
    };

    @OnClick(R.id.btnRetry)
    public void retry() {
        fetchBuzz();
    }

    @SuppressLint("DefaultLocale")
    private void populateUiWithBuzz(final ShoutFeed feed) {
        shout.setVisibility(View.VISIBLE);
        topIndicator.setBackgroundResource(getTrafficConditionColorResource(feed.getTrafficCondition()));
        ivPropic.setImageURI(Uri.parse(feed.getPhoto()));
        ivPropic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavigationUtil.openProfileActivity(SingleBuzzActivity.this, feed.getUserId());
            }
        });
        tvShoutUserName.setText(feed.getUserName());
        tvShoutUserName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavigationUtil.openProfileActivity(getApplicationContext(), feed.getUserId());
            }
        });
        tvShoutUserLocation.setText(feed.getLocation().getPlace());
        tvShout.setText(feed.getShoutText());
        tvShoutTime.setText(TimeUtil.getElapsedTime(feed.getTime()));

        List<String> attachments = feed.getAttachments();
        if (attachments != null && attachments.size() > 0) {
            ivPhoto.setVisibility(View.VISIBLE);
            ivPhoto.setImageURI(Uri.parse(attachments.get(0)));
        } else {
            ivPhoto.setVisibility(View.GONE);
        }

        ivLike.setAlpha(feed.isLikedByUser() ? 1.0f : 0.5f); // increase/decrease opacity to represent like/dislike state
        ivLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isLikeProcessing) return;

                LikeDislikeRequest likeDislikeRequest = new LikeDislikeRequest();
                likeDislikeRequest.id = feed.getShoutId();
                likeDislikeRequest.time = System.currentTimeMillis();

                isLikeProcessing = true;
                resetVisibility();

                restApi.likeDislike(User.getInstance().getAccessToken(), feed.getShoutId(), likeDislikeRequest, new Callback<Like>() {
                    @Override
                    public void success(Like like, Response response) {
                        isLikeProcessing = false;
                        resetVisibility();

                        if (feed.isLikedByUser()) {
                            feed.setLikeCount(feed.getLikeCount() - 1);
                        } else {
                            feed.setLikeCount(feed.getLikeCount() + 1);
                        }

                        feed.setLikedByUser(!feed.isLikedByUser());

                        populateUiWithBuzz(feed);
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        isLikeProcessing = false;
                        resetVisibility();

                        if (error.getResponse() != null) {
                            Log.d(TAG, error.getResponse().toString());
                        }
                    }
                });
            }
        });
        tvLikeCount.setText(String.format("+%d", feed.getLikeCount()));
        tvLikeCount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SingleBuzzActivity.this, LikerActivity.class);
                intent.putExtra(LikerActivity.EXTRA_LIKE_TYPE, LikerActivity.LIKE_TYPE_BUZZ);
                intent.putExtra("buzzId", feed.getShoutId());
                startActivity(intent);
            }
        });

        tvCommentCount.setText(String.valueOf(feed.getCommentCount()));
        tvCommentCount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SingleBuzzActivity.this, CommentActivity.class);
                intent.putExtra(CommentActivity.EXTRA_COMMENT_TYPE, CommentActivity.COMMENT_TYPE_BUZZ);
                intent.putExtra("buzzId", feed.getShoutId());
                startActivity(intent);
            }
        });
        tvShareCount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = ShareCompat.IntentBuilder
                        .from(SingleBuzzActivity.this)
                        .setType("text/plain")
                        .setText(feed.getSharableLink())
                        .setChooserTitle("Share link using").getIntent();
                startActivity(Intent.createChooser(intent, "Share link using"));
            }
        });
    }

    private int getTrafficConditionColorResource(String trafficCondition) {
        if (trafficCondition.equalsIgnoreCase("HIGH")) {
            return R.mipmap.ic_high_traffic;
        }

        if (trafficCondition.equalsIgnoreCase("MEDIUM")) {
            return R.mipmap.ic_medium_traffic;
        }

        if (trafficCondition.equalsIgnoreCase("LOW")) {
            return R.mipmap.ic_low_traffic;
        }

        return 0;
    }

    private void resetVisibility() {
        ivLike.setVisibility(isLikeProcessing ? View.INVISIBLE : View.VISIBLE);
        likeLoading.setVisibility(isLikeProcessing ? View.VISIBLE : View.GONE);
    }
}