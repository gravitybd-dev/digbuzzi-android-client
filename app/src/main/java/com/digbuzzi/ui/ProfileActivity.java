package com.digbuzzi.ui;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.digbuzzi.event.FollowUserEvent;
import com.digbuzzi.repo.BuzzRepository;
import com.facebook.drawee.view.SimpleDraweeView;
import java.util.List;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.digbuzzi.R;
import com.digbuzzi.adapter.ShoutFeedAdapter;
import com.digbuzzi.api.RestAdapterProvider;
import com.digbuzzi.app.AppConfig;
import com.digbuzzi.model.User;
import com.digbuzzi.model.restmodel.FollowUnfollowRequest;
import com.digbuzzi.model.restmodel.FollowUnfollowResponse;
import com.digbuzzi.model.restmodel.Profile;
import com.digbuzzi.model.restmodel.ShoutFeed;

import de.greenrobot.event.EventBus;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

@SuppressWarnings("WeakerAccess")
public class ProfileActivity extends BaseNotificationEnabledActivity {
    private static final String TAG = ProfileActivity.class.getSimpleName();
    public static final String KEY_USER_ID = "userId";

    @Bind(R.id.toolbar)
    Toolbar toolbar;

    @Bind(R.id.rvAllFeed)
    RecyclerView rvAllFeed;

    @Bind(R.id.srl)
    SwipeRefreshLayout srl;

    @Bind(R.id.statusContainer)
    View statusContainer;

    @Bind(R.id.tvStatus)
    TextView tvStatus;

    @Bind(R.id.btnRetry)
    Button btnRetry;

    @Bind(R.id.ivPropic)
    SimpleDraweeView ivPropic;

    @Bind(R.id.tvName)
    TextView tvName;

    @Bind(R.id.tvFullName)
    TextView tvFullName;

    @Bind(R.id.tvAbout)
    TextView tvAbout;

    @Bind(R.id.tvFollowersCount)
    TextView tvFollowersCount;

    @Bind(R.id.tvFollowingCount)
    TextView tvFollowingCount;

    @Bind(R.id.tvScore)
    TextView tvScore;

    @Bind(R.id.followUnfollowContainer)
    View followUnfollowContainer;

    @Bind(R.id.tvFollowUnfollow)
    TextView tvFollowUnfollow;

    @Bind(R.id.followUnfollowLoading)
    View followUnfollowLoading;

    @Bind(R.id.tvEditUser)
    TextView tvEditUser;

    boolean followUnfollowRequestProcessing;

    ShoutFeedAdapter adapter;

    EndlessRecyclerOnScrollListener endlessRecyclerOnScrollListener;

    private String mUserId;
    private Profile mProfile;

    private BuzzRepository repository = BuzzRepository.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        init();

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ProfileActivity.this, PostActivity.class));
            }
        });
    }

    @Override
    protected void onNotification(Bundle data) {

    }

    @Override
    protected void onNotificationCounter(int counter) {

    }

    private void init() {
        mUserId = getIntent().getStringExtra(KEY_USER_ID);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);

        rvAllFeed.setLayoutManager(layoutManager);

        endlessRecyclerOnScrollListener = new EndlessRecyclerOnScrollListener(layoutManager) {
            @Override
            public void onLoadMore(int offset, int count) {
                loadMore(offset, count);
            }
        };

        rvAllFeed.addOnScrollListener(endlessRecyclerOnScrollListener);
        adapter = new ShoutFeedAdapter(this);
        rvAllFeed.setAdapter(adapter);

        if (adapter.getItemCount() == 0) {
            srl.post(new Runnable() {
                @Override
                public void run() {
                    srl.setRefreshing(true);
                }
            });
            refreshFeeds();
        }

        srl.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshFeeds();
            }
        });
    }

    @Override
    protected void onResume() {
        adapter.notifyDataSetChanged();
        fetchProfileInfo();
        super.onResume();
    }

    private void fetchProfileInfo() {
        RestAdapterProvider.getProvider().getRestApiForRetrofit1()
                .getUserData(User.getInstance().getAccessToken(), mUserId, new Callback<Profile>() {
                    @Override
                    public void success(Profile profile, Response response) {
                        mProfile = profile;
                        populateProfileUIWithData();
                    }

                    @Override
                    public void failure(RetrofitError error) {

                    }
                });
    }

    private void populateProfileUIWithData() {
        ivPropic.setImageURI(Uri.parse(mProfile.getPhoto()));

        tvName.setText(mProfile.getUserName());
        tvFullName.setText(mProfile.getName());
        tvAbout.setText(mProfile.getBio());
        tvFollowersCount.setText(String.valueOf(mProfile.getFollowerCount()));
        tvFollowingCount.setText(String.valueOf(mProfile.getFollowingCount()));
        tvScore.setText(String.valueOf(mProfile.getPoint().getTotalPoint()));

        boolean isOwnProfile = mProfile.isOwnProfile();
        if (!isOwnProfile) {
            followUnfollowContainer.setVisibility(View.VISIBLE);
            tvFollowUnfollow.setText(mProfile.isFollowing() ? "unfollow" : "follow");
            followUnfollowContainer.setOnClickListener(followUnfollowClickListener);
        } else {
            followUnfollowContainer.setVisibility(View.INVISIBLE);
            tvEditUser.setVisibility(View.VISIBLE);
            tvEditUser.setOnClickListener(editUserListner);
        }
    }

    @OnClick(R.id.llFollowers)
    public void openFollowerList() {
        Intent intent = new Intent(ProfileActivity.this, FollowerFollowingActivity.class);
        intent.putExtra(FollowerFollowingActivity.KEY_TITLE, "Followers");
        intent.putExtra(FollowerFollowingActivity.KEY_USERID, mUserId);
        intent.putExtra(FollowerFollowingActivity.KEY_FOLLOWING_LIST, false);
        startActivity(intent);
    }

    @OnClick(R.id.llFollowing)
    public void showFollowingCount() {
        Intent intent = new Intent(ProfileActivity.this, FollowerFollowingActivity.class);
        intent.putExtra(FollowerFollowingActivity.KEY_TITLE, "Following");
        intent.putExtra(FollowerFollowingActivity.KEY_USERID, mUserId);
        intent.putExtra(FollowerFollowingActivity.KEY_FOLLOWING_LIST, true);
        startActivity(intent);
    }

    @OnClick(R.id.llPoints)
    public void showLeaderBoard() {
        Intent intent = new Intent(this, LeaderBoardActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.btnRetry)
    public void retry() {
        if (adapter.getItemCount() == 0) {
            srl.post(new Runnable() {
                @Override
                public void run() {
                    srl.setRefreshing(true);
                }
            });
            refreshFeeds();
        }

        srl.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshFeeds();
            }
        });
    }

    private void refreshFeeds() {
        statusContainer.setVisibility(View.GONE);

        String token = User.getInstance().getAccessToken();
        String userId = mUserId;
        RestAdapterProvider.getProvider().getRestApiForRetrofit1().getUserShouts(token, userId, 0, AppConfig.FEED_COUNT_PER_REQUEST, new Callback<List<ShoutFeed>>() {
            @Override
            public void success(List<ShoutFeed> shoutFeeds, Response response) {
                srl.post(new Runnable() {
                    @Override
                    public void run() {
                        srl.setRefreshing(false);
                    }
                });
                if (shoutFeeds != null) {
                    repository.removeBuzzes(TAG);
                    repository.saveBuzzes(TAG, shoutFeeds);
                    List<ShoutFeed> feeds = repository.getBuzzes(TAG);
                    endlessRecyclerOnScrollListener.reset(feeds.size());
                    adapter.setFeeds(feeds);
                    adapter.notifyDataSetChanged();
                }

                if (adapter.getItemCount() == 0) {
                    statusContainer.setVisibility(View.VISIBLE);
                    tvStatus.setText("No feed available!");
                    btnRetry.setText("REFRESH");
                }
            }

            @Override
            public void failure(RetrofitError error) {
                srl.post(new Runnable() {
                    @Override
                    public void run() {
                        srl.setRefreshing(false);
                    }
                });
                if (adapter.getItemCount() == 0) {
                    statusContainer.setVisibility(View.VISIBLE);
                    tvStatus.setText("Couldn't get the feeds");
                    btnRetry.setText("RETRY");
                }
            }
        });
    }

    private void loadMore(int offset, int count) {
        RestAdapterProvider.getProvider().getRestApiForRetrofit1().getUserShouts(User.getInstance().getAccessToken(), mUserId, offset, count, new Callback<List<ShoutFeed>>() {
            @Override
            public void success(List<ShoutFeed> shoutFeeds, Response response) {
                Log.d(ProfileActivity.class.getSimpleName(), response.toString());
                if (shoutFeeds != null) {
                    repository.saveBuzzes(TAG, shoutFeeds);
                    adapter.setFeeds(repository.getBuzzes(TAG));
                    adapter.notifyDataSetChanged();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Log.d(ProfileActivity.class.getSimpleName(), error.getMessage());
            }
        });
    }


    private View.OnClickListener editUserListner = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            startActivity( new Intent(ProfileActivity.this,EditProfileActivity.class));
        }
    };

    private View.OnClickListener followUnfollowClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (mUserId != null) {
                FollowUnfollowRequest request = new FollowUnfollowRequest();
                request.setName(mProfile.getName());
                request.setUserName(mProfile.getUserName());
                request.setPhoto(mProfile.getPhoto());
                request.setUserId(mProfile.getId());
                request.setTime(System.currentTimeMillis());
                if (mProfile.isFollowing()) {
                    unfollowUser(request);
                } else {
                    followUser(request);
                }
            }
        }
    };

    private void followUser(FollowUnfollowRequest request) {
        if (followUnfollowRequestProcessing) return;
        setFollowUnfollowProcessing(true);

        RestAdapterProvider.getProvider().getRestApiForRetrofit1()
                .follow(User.getInstance().getAccessToken(), request, new Callback<FollowUnfollowResponse>() {
                    @Override
                    public void success(FollowUnfollowResponse followUnfollowResponse, Response response) {
                        setFollowUnfollowProcessing(false);

                        mProfile.setIsFollowing(true);
                        mProfile.setFollowerCount(mProfile.getFollowerCount() + 1);
                        populateProfileUIWithData();

                        EventBus.getDefault().post(new FollowUserEvent(mProfile.getUserName()));
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        setFollowUnfollowProcessing(false);

                        Toast.makeText(getApplicationContext(), "Couldn't follow the user. Please, try again", Toast.LENGTH_LONG).show();
                    }
                });
    }

    private void unfollowUser(FollowUnfollowRequest request) {
        if (followUnfollowRequestProcessing) return;
        setFollowUnfollowProcessing(true);

        RestAdapterProvider.getProvider().getRestApiForRetrofit1()
                .unfollow(User.getInstance().getAccessToken(), request, new Callback<FollowUnfollowResponse>() {
                    @Override
                    public void success(FollowUnfollowResponse followUnfollowResponse, Response response) {
                        setFollowUnfollowProcessing(false);

                        mProfile.setIsFollowing(false);
                        mProfile.setFollowerCount(mProfile.getFollowerCount() - 1);
                        populateProfileUIWithData();

                        Toast.makeText(getApplicationContext(), "You're now not following " + mProfile.getUserName(), Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        setFollowUnfollowProcessing(false);

                        Toast.makeText(getApplicationContext(), "Couldn't unfollow the user. Please, try again", Toast.LENGTH_LONG).show();
                    }
                });
    }

    private void setFollowUnfollowProcessing(boolean processing) {
        followUnfollowRequestProcessing = processing;
        tvFollowUnfollow.setVisibility(processing ? View.INVISIBLE : View.VISIBLE);
        followUnfollowLoading.setVisibility(processing ? View.VISIBLE : View.GONE);
    }
}