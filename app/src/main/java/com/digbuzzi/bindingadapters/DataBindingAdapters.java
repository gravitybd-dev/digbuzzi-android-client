package com.digbuzzi.bindingadapters;

import android.databinding.BindingAdapter;
import android.widget.ImageView;

/**
 * Created by anwarshahriar on 9/15/16.
 */
public class DataBindingAdapters {

    @BindingAdapter("imageResource")
    public static void setImageResource(ImageView imageView, int resource){
        if (resource != 0)
            imageView.setImageResource(resource);
    }
}
