package com.digbuzzi.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by U on 7/20/2016.
 */

public class PlaceOfInterestGroup {
    @SerializedName("placeGroupType")
    private String placeGroupType;

    @SerializedName("placeGroupName")
    private String placeGroupName;

    @SerializedName("imageName")
    private String imageName;

    @SerializedName("mapImageName")
    private String mapImageName;

    @SerializedName("placeGroupMembers")
    private List<PlaceOfInterestType> placeGroupMembers;

    public String getPlaceGroupType() {
        return placeGroupType;
    }

    public void setPlaceGroupType(String placeGroupType) {
        this.placeGroupType = placeGroupType;
    }

    public String getPlaceGroupName() {
        return placeGroupName;
    }

    public void setPlaceGroupName(String placeGroupName) {
        this.placeGroupName = placeGroupName;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public String getMapImageName() {
        return mapImageName;
    }

    public void setMapImageName(String mapImageName) {
        this.mapImageName = mapImageName;
    }

    public List<PlaceOfInterestType> getPlaceGroupMembers() {
        return placeGroupMembers;
    }

    public void setPlaceGroupMembers(List<PlaceOfInterestType> placeGroupMembers) {
        this.placeGroupMembers = placeGroupMembers;
    }

    public boolean hasPlaceType(String placeType) {
        for (int i = 0, size = placeGroupMembers.size(); i < size; i++) {
            if (placeGroupMembers.get(i).getPlaceType().equals(placeType))
                return true;
        }
        return false;
    }
}