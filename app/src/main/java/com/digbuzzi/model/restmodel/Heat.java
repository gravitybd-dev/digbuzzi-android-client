package com.digbuzzi.model.restmodel;

/**
 * Created by Joker on 1/7/16.
 */
public class Heat {
    private String userName;
    private String userId;
    private String photo;
    private long time;
    private String shoutId;
    private double lat;
    private double lon;
    private String trafficCondition;

    public double getLat() {
        return lat;
    }

    public double getLon() {
        return lon;
    }

    public String getTrafficCondition() {
        return trafficCondition;
    }

    public String getUserName() {
        return userName;
    }

    public String getUserId() {
        return userId;
    }

    public long getTime() {
        return time;
    }

    public String getPhoto() {
        return photo;
    }

    public String getShoutId() {
        return shoutId;
    }
}