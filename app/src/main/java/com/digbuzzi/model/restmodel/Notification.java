package com.digbuzzi.model.restmodel;

import com.digbuzzi.Util.StringUtils;

/**
 * Created by Joker on 3/23/16.
 */
public class Notification {
    public String type;
    public String text;
    public long time;
    public String participantUserName;
    public String participantUserId;
    public String receipentUserName;
    public String receipentUserId;
    public String participentAvatar;
    public String receipentAvatar;
    public String shoutId;
}
