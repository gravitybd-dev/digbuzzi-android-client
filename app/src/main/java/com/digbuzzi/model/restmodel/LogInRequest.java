package com.digbuzzi.model.restmodel;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Joker on 1/2/16.
 */
public class LogInRequest {
    @SerializedName("identity")
    public String identity;

    @SerializedName("password")
    public String password;
}
