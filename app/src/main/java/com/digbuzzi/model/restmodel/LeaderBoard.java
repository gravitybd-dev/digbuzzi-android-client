package com.digbuzzi.model.restmodel;

import java.util.List;

/**
 * Created by Joker on 3/8/16.
 */
public class LeaderBoard {
    private Leader user;
    private List<Leader> leaders;

    public Leader getUser() {
        return user;
    }

    public List<Leader> getLeaders() {
        return leaders;
    }
}