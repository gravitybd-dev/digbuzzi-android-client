package com.digbuzzi.model.restmodel;

/**
 * Created by U on 1/22/2016.
 */
public class FollowPersonModel {
    private long time;
    private String userName;
    private String userId;
    private String photo;
    private String name;
    private String email;

    public long getTime() {
        return time;
    }

    public String getPhoto() {
        return photo;
    }

    public String getUserName() {
        return userName;
    }

    public String getUserId() {
        return userId;
    }

    public String getEmail() {
        return email;
    }

    public String getName() {
        return name;
    }
}
