package com.digbuzzi.model.restmodel;

/**
 * Created by U on 1/4/2016.
 */
public class Commentor {
    private String id;
    private String name;
    private String userName;
    private String photo;

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getUserName() {
        return userName;
    }

    public String getPhoto() {
        return photo;
    }
}
