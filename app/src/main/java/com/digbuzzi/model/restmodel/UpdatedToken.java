package com.digbuzzi.model.restmodel;

/**
 * Created by Joker on 1/10/16.
 */
public class UpdatedToken {
    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    private String token;
}
