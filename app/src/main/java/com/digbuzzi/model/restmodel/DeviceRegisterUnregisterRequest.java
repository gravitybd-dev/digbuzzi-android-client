package com.digbuzzi.model.restmodel;

/**
 * Created by Joker on 3/20/16.
 */
public class DeviceRegisterUnregisterRequest {
    public String appId;
    public String deviceId;
    public String platform = "android";
}
