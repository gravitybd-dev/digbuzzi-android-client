package com.digbuzzi.model.restmodel;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Joker on 1/2/16.
 */
public class RegisterRequest {
    @SerializedName("userName")
    public String userName;

    @SerializedName("email")
    public String email;

    @SerializedName("password")
    public String password;
}
