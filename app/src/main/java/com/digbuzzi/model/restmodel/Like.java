package com.digbuzzi.model.restmodel;

/**
 * Created by U on 1/6/2016.
 */
public class Like {
    private String userId;
    private String userName;
    private String photo;
    private long time;

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserName() {
        return userName;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getPhoto() {
        return photo;
    }

    public void setTime(long likeTime) {
        this.time = time;
    }

    public long getTime() {
        return time;
    }
}
