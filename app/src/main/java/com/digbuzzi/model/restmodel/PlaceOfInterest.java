package com.digbuzzi.model.restmodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.ArrayList;
import java.util.List;

public class PlaceOfInterest {
    @SerializedName("placeId")
    @Expose
    private String placeId;

    @SerializedName("placeTitle")
    @Expose
    private String placeTitle;

    @SerializedName("placeDescription")
    @Expose
    private String placeDescription;

    @SerializedName("likeCount")
    @Expose
    private int likeCount;

    @SerializedName("commentCount")
    @Expose
    private int commentCount;

    @SerializedName("shareCount")
    @Expose
    private int shareCount;

    @SerializedName("comments")
    @Expose
    private List<Comment> comments = new ArrayList<>();

    @SerializedName("sharableLink")
    @Expose
    private String sharableLink;

    @SerializedName("attachments")
    @Expose
    private List<String> attachments = new ArrayList<>();

    @SerializedName("placeTypes")
    @Expose
    private List<String> placeTypes = new ArrayList<>();

    @SerializedName("location")
    @Expose
    private Location location;

    @SerializedName("isLikedByUser")
    @Expose
    private boolean isLikedByUser;

    @SerializedName("userName")
    @Expose
    private String userName;

    @SerializedName("userId")
    @Expose
    private String userId;

    @SerializedName("photo")
    @Expose
    private String photo;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("email")
    @Expose
    private String email;

    @SerializedName("time")
    @Expose
    private long time;

    public String getPlaceId() {
        return placeId;
    }

    public void setPlaceId(String placeId) {
        this.placeId = placeId;
    }

    public String getPlaceTitle() {
        return placeTitle;
    }

    public void setPlaceTitle(String placeTitle) {
        this.placeTitle = placeTitle;
    }

    public String getPlaceDescription() {
        return placeDescription;
    }

    public void setPlaceDescription(String placeDescription) {
        this.placeDescription = placeDescription;
    }

    public int getLikeCount() {
        return likeCount;
    }

    public void setLikeCount(int likeCount) {
        this.likeCount = likeCount;
    }

    public int getCommentCount() {
        return commentCount;
    }

    public void setCommentCount(int commentCount) {
        this.commentCount = commentCount;
    }

    public int getShareCount() {
        return shareCount;
    }

    public void setShareCount(int shareCount) {
        this.shareCount = shareCount;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    public String getSharableLink() {
        return sharableLink;
    }

    public void setSharableLink(String sharableLink) {
        this.sharableLink = sharableLink;
    }

    public List<String> getAttachments() {
        return attachments;
    }

    public void setAttachments(List<String> attachments) {
        this.attachments = attachments;
    }

    public List<String> getPlaceTypes() {
        return placeTypes;
    }

    public void setPlaceTypes(List<String> placeTypes) {
        this.placeTypes = placeTypes;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public boolean isLikedByUser() {
        return isLikedByUser;
    }

    public void setLikedByUser(boolean likedByUser) {
        isLikedByUser = likedByUser;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Object getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }
}