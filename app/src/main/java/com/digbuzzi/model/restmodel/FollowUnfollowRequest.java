package com.digbuzzi.model.restmodel;

/**
 * Created by Joker on 1/8/16.
 */
public class FollowUnfollowRequest {
    private String userName;
    private String userId;
    private String photo;
    private String name;
    private long time;

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setTime(long time) {
        this.time = time;
    }
}
