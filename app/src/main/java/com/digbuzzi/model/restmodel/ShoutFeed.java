package com.digbuzzi.model.restmodel;

import java.util.List;

/**
 * Created by anwarshahriar on 1/4/2016.
 * Used as a model for GSON
 */
@SuppressWarnings("unused")
public class ShoutFeed {
    private String shoutId;
    private String name;
    private String userId;
    private String userName;
    private String photo;
    private String shoutText;
    private Location location;
    private String trafficCondition;
    private long time;
    private int commentCount;
    private int likeCount;
    private List<String> attachments;
    private List<Comment> comments;
    private int shareCount;
    private boolean isLikedByUser;
    private String sharableLink;

    public String getShoutId() {
        return shoutId;
    }

    public String getName() {
        return name;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserName() {
        return userName;
    }

    public String getUserId() {
        return userId;
    }

    public String getPhoto() {
        return photo;
    }

    public String getShoutText() {
        return shoutText;
    }

    public Location getLocation() {
        return location;
    }

    public String getTrafficCondition() {
        return trafficCondition;
    }

    public long getTime() {
        return time;
    }

    public int getCommentCount() {
        return commentCount;
    }

    public void setCommentCount(int commentCount) {
        this.commentCount = commentCount;
    }

    public int getLikeCount() {
        return likeCount;
    }

    public void setLikeCount(int likeCount) {
        this.likeCount = likeCount;
    }

    public int getShareCount() {
        return shareCount;
    }

    public void setShareCount(int shareCount) {
        this.shareCount = shareCount;
    }

    public List<String> getAttachments() {
        return attachments;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setLikedByUser(boolean likedByUser) {
        isLikedByUser = likedByUser;
    }

    public boolean isLikedByUser() {
        return isLikedByUser;
    }

    public void setSharableLink(String sharableLink) {
        this.sharableLink = sharableLink;
    }

    public String getSharableLink() {
        return sharableLink;
    }
}