package com.digbuzzi.model.restmodel;

import org.parceler.Parcel;

/**
 * Created by Joker on 1/8/16.
 */
@Parcel
public class Badge {
    private String badgeId;
    private String badgeName;
    private String badgeImg;
    private String badgeTitle;
    private String badgeDescription;
    private String badgeStatus;

    public String getBadgeId() {
        return badgeId;
    }

    public String getBadgeName() {
        return badgeName;
    }

    public String getBadgeImg() {
        return badgeImg;
    }

    public String getBadgeTitle() {
        return badgeTitle;
    }

    public String getBadgeDescription() {
        return badgeDescription;
    }

    public String getBadgeStatus() {
        return badgeStatus;
    }
}
