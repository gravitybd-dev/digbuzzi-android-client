package com.digbuzzi.model.restmodel;

import org.parceler.Parcel;

/**
 * Created by Joker on 3/9/16.
 */
@Parcel
public class Point {
    private int totalPoint;

    public void setTotalPoint(int totalPoint) {
        this.totalPoint = totalPoint;
    }

    public int getTotalPoint() {
        return totalPoint;
    }
}
