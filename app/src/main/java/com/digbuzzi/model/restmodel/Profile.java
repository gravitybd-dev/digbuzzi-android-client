package com.digbuzzi.model.restmodel;

import org.parceler.Parcel;

import java.util.List;

/**
 * Created by Joker on 1/8/16.
 */
@Parcel
public class Profile {
    private String userId;
    private String name;
    private String userName;
    private boolean isOwnProfile;
    private boolean isFollowing;
    private String photo;
    private String email;
    private Point point;
    private String address;
    private String mood;
    private String bio;
    private boolean emailVerified;
    private List<Badge> badges;
    private int followerCount;
    private int followingCount;

    public String getId() {
        return userId;
    }

    public String getName() {
        return name;
    }

    public String getUserName() {
        return userName;
    }

    public boolean isOwnProfile() {
        return isOwnProfile;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getPhoto() {
        return photo;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public Point getPoint() {
        return point;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAddress() {
        return address;
    }

    public void setMood(String mood) {
        this.mood = mood;
    }

    public String getMood() {
        return mood;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public String getBio() {
        return bio;
    }

    public boolean isEmailVerified() {
        return emailVerified;
    }

    public boolean isFollowing() {
        return isFollowing;
    }

    public void setIsFollowing(boolean isFollowing) {
        this.isFollowing = isFollowing;
    }

    public List<Badge> getBadges() {
        return badges;
    }

    public void setFollowerCount(int followerCount) {
        this.followerCount = followerCount;
    }

    public int getFollowerCount() {
        return followerCount;
    }

    public void setFollowingCount(int followingCount) {
        this.followingCount = followingCount;
    }

    public int getFollowingCount() {
        return followingCount;
    }
}
