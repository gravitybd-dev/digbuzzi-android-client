package com.digbuzzi.model.restmodel;

/**
 * Created by U on 3/7/2016.
 */
public class Leader {
    private long time;
    private String userName;
    private String userId;
    private String photo;
    private String name;
    private String email;
    private int point;
    private int rank;
    private boolean isFollowing;
    private int followerCount;

    public long getTime() {
        return time;
    }

    public String getUserId() {
        return userId;
    }

    public String getEmail() {
        return email;
    }

    public String getName() {
        return name;
    }

    public String getPhoto() {
        return photo;
    }

    public int getPoint() {
        return point;
    }

    public String getUserName() {
        return userName;
    }

    public int getRank() {
        return rank;
    }

    public void setFollowing(boolean isFollowing) {
        this.isFollowing = isFollowing;
    }

    public boolean isFollowing() {
        return isFollowing;
    }

    public int getFollowerCount() {
        return followerCount;
    }
}