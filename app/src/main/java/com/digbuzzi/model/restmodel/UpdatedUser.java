package com.digbuzzi.model.restmodel;

/**
 * Created by Joker on 1/10/16.
 */
public class UpdatedUser {

    public Profile getProfile() {
        return profile;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }


    private Profile profile;
    private String updatedToken;

    public String getUpdatedToken() {
        return updatedToken;
    }

    public void setUpdatedToken(String updatedToken) {
        this.updatedToken = updatedToken;
    }
}
