package com.digbuzzi.model.restmodel;

/**
 * Created by U on 1/4/2016.
 */
public class Comment {
    private String commentId;
    private Commentor commentor;
    private long time;
    private String commentText;

    public Commentor getCommentor() {
        return commentor;
    }

    public long getTime() {
        return time;
    }

    public String getCommentId() {
        return commentId;
    }

    public String getCommentText() {
        return commentText;
    }
}
