package com.digbuzzi.model.restmodel;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Joker on 1/2/16.
 */
public class AuthResponse {
    @SerializedName("token")
    private String token;

    @SerializedName("userId")
    private String userId;

    @SerializedName("validity")
    private long validity;

    public String getToken() {
        return token;
    }

    public String getUserId() {
        return userId;
    }

    public long getValidity() {
        return validity;
    }
}
