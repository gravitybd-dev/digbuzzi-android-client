package com.digbuzzi.model.restmodel;

/**
 * Created by U on 1/4/2016.
 */
public class Location {
    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    private double latitude;
    private double longitude;
    private String place;
    private String city;
    private String country;

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public String getCity() {
        return city;
    }

    public String getCountry() {
        return country;
    }

    public String getPlace() {
        return place;
    }
}
