package com.digbuzzi.model.restmodel;

import com.google.gson.annotations.SerializedName;

/**
 * Created by U on 1/5/2016.
 */
public class RequestAddComment {
    @SerializedName("time")
    public long time;

    @SerializedName("commentText")
    public String commentText;
}
