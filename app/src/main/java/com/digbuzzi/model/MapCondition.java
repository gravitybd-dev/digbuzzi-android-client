package com.digbuzzi.model;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by anwarshahriar on 6/22/2016.
 */

public class MapCondition {
    private LatLng lastLookedPosition;
    private float lastLookedZoomLevel;

    public MapCondition(LatLng lastLookedPosition, float lastLookedZoomLevel) {
        this.lastLookedPosition = lastLookedPosition;
        this.lastLookedZoomLevel = lastLookedZoomLevel;
    }

    public LatLng getLastLookedPosition() {
        return lastLookedPosition;
    }

    public float getLastLookedZoomLevel() {
        return lastLookedZoomLevel;
    }

    /**
     * Tell if we can use the map condition to manipulate the map camera
     * @return true if it can be used to manipulate camera in map using the map condition otherwise false
     */
    public boolean isUsableForMap() {
        return lastLookedPosition != null && lastLookedPosition.latitude != 0 && lastLookedPosition.longitude != 0 && lastLookedZoomLevel != 0;
    }
}