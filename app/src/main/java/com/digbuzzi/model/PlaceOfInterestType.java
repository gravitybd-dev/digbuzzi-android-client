package com.digbuzzi.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by U on 7/20/2016.
 */

public class PlaceOfInterestType {
    @SerializedName("placeType")
    private String placeType;

    @SerializedName("placeName")
    private String placeName;

    @SerializedName("imageName")
    private String imageName;

    private boolean selected;

    public String getPlaceType() {
        return placeType;
    }

    public void setPlaceType(String placeType) {
        this.placeType = placeType;
    }

    public String getPlaceName() {
        return placeName;
    }

    public void setPlaceName(String placeName) {
        this.placeName = placeName;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public boolean isSelected() {
        return selected;
    }
}