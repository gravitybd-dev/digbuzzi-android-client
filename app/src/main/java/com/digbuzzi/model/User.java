package com.digbuzzi.model;

import android.content.SharedPreferences;

import com.digbuzzi.app.AppConfig;
import com.digbuzzi.model.restmodel.AuthResponse;

/**
 * Created by U on 12/29/2015.
 */
public class User {
    private static final String PREF_USER_ACCESSTOKEN = "city.trafficnow.trafficnow.user_accesstoken";
    private static final String PREF_USER_EMAIL = "city.trafficnow.trafficnow.user_email";
    private static final String PREF_LAST_UPDATED_TIME = "city.trafficnow.trafficnow.last_updated_time";
    private static final String PREF_USER_ID = "city.trafficnow.trafficnow.user_id";
    private static final long MAX_DAY_EXPIRATION_LIMIT = 13 * 24 * 60 * 60 * 1000;

    private static User instance;

    private SharedPreferences mPreferences;
    private String mAccessToken;
    private String mEmail;
    private String mUserId;
    private long mLastUpdateMillis;

    private boolean prepared;
    private long mExpireTime = 0;

    private User() {}

    public static synchronized User getInstance() {
        if (instance == null) {
            instance = new User();
        }

        return instance;
    }

    public void prepare(SharedPreferences preferences) {
        mPreferences = preferences;
        prepared = true;
    }

    public void setAccessToken(String accessToken) {
        checkIsPrepared();
        mLastUpdateMillis = System.currentTimeMillis();
        mPreferences.edit().putLong(PREF_LAST_UPDATED_TIME, mLastUpdateMillis).commit();
        mAccessToken = "Bearer " + accessToken;
        mPreferences.edit().putString(PREF_USER_ACCESSTOKEN, mAccessToken).commit();
    }

    public String getAccessToken() {
        checkIsPrepared();
        if (System.currentTimeMillis() - getLastUpdatedMillis() >= MAX_DAY_EXPIRATION_LIMIT) {
            mAccessToken = null;
            mPreferences.edit().remove(PREF_USER_ACCESSTOKEN).remove(PREF_USER_EMAIL).remove(PREF_LAST_UPDATED_TIME).commit();
        }
        if (mAccessToken == null) {
            mAccessToken = mPreferences.getString(PREF_USER_ACCESSTOKEN, "");
        }

        return mAccessToken;
    }

    public void setEmail(String email) {
        checkIsPrepared();
        mEmail = email;
        mPreferences.edit().putString(PREF_USER_EMAIL, email).commit();
    }

    public String getEmail() {
        checkIsPrepared();
        if (mEmail == null) {
            mEmail = mPreferences.getString(PREF_USER_EMAIL, "");
        }

        return mEmail;
    }

    public void setUserId(String userId) {
        checkIsPrepared();
        mUserId = userId;
        mPreferences.edit().putString(PREF_USER_ID, userId).commit();
    }

    public String getUserId() {
        checkIsPrepared();
        if (mUserId == null) {
            mUserId = mPreferences.getString(PREF_USER_ID, "");
        }

        return mUserId;
    }

    public void setExpireTime(long expireTime) {
        checkIsPrepared();
        mExpireTime = expireTime;
        mPreferences.edit().putLong(AppConfig.PREF_EXPIRATION_TIME, expireTime).commit();
    }

    public long getExpireTime() {
        checkIsPrepared();
        if (mExpireTime == 0) {
            mExpireTime = mPreferences.getLong(AppConfig.PREF_EXPIRATION_TIME, 0);
        }
        return mExpireTime;
    }

    private long getLastUpdatedMillis() {
        if (mLastUpdateMillis == 0) {
            mLastUpdateMillis = mPreferences.getLong(PREF_LAST_UPDATED_TIME, 0);
        }

        return mLastUpdateMillis;
    }

    private void checkIsPrepared() {
        if (!prepared) {
            throw new RuntimeException("User.prepare() must be called at least once");
        }
    }

    public void initiateUser(String identity, AuthResponse authResponse) {
        setAccessToken(authResponse.getToken());
        setEmail(identity);
        setUserId(authResponse.getUserId());
        setExpireTime(authResponse.getValidity() - AppConfig.EXPIRATION_THRESHOLD);
    }

    public void clear() {
        mAccessToken = null;
        mPreferences.edit().remove(PREF_USER_ACCESSTOKEN)
                .remove(AppConfig.PREF_EXPIRATION_TIME)
                .remove(PREF_USER_EMAIL)
                .remove(PREF_LAST_UPDATED_TIME)
                .remove(PREF_USER_ID).commit();
        mEmail = null;
        mLastUpdateMillis = 0;
        mUserId = null;
        mExpireTime = 0;
    }
}