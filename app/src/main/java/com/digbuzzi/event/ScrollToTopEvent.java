package com.digbuzzi.event;

/**
 * Created by Joker on 7/2/16.
 */
public class ScrollToTopEvent {
    private String title;

    public ScrollToTopEvent(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }
}
