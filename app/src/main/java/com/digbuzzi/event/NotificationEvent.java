package com.digbuzzi.event;

import android.os.Bundle;

/**
 * Created by Joker on 3/20/16.
 */
public class NotificationEvent {
    private Bundle mData;

    public NotificationEvent(Bundle data) {
        mData = data;
    }

    public Bundle getData() {
        return mData;
    }
}
