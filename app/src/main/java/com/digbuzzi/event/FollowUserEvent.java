package com.digbuzzi.event;

/**
 * Created by U on 6/23/2016.
 */

public class FollowUserEvent {
    private String username;

    public FollowUserEvent(String username) {
        this.username = username;
    }

    public String getUsername() {
        return username;
    }
}