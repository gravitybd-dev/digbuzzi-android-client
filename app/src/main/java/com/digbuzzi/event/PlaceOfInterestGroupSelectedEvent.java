package com.digbuzzi.event;

import com.digbuzzi.model.PlaceOfInterestGroup;

/**
 * Created by U on 7/20/2016.
 */

public class PlaceOfInterestGroupSelectedEvent {
    private PlaceOfInterestGroup mGroup;

    public PlaceOfInterestGroupSelectedEvent(PlaceOfInterestGroup group) {
        mGroup = group;
    }

    public PlaceOfInterestGroup getGroup() {
        return mGroup;
    }
}