package com.digbuzzi.adapter;

import com.digbuzzi.model.restmodel.ShoutFeed;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by anwarshahriar on 5/13/16.
 */
class ShoutFeedHelper {
    public static ShoutFeedAdapterWrapper getShoutFeedWrapper(ShoutFeed shoutFeed) {
        return new ShoutFeedAdapterWrapper(shoutFeed);
    }

    public static List<ShoutFeedAdapterWrapper> getShoutFeedWrappers(List<ShoutFeed> shoutFeeds) {
        if (shoutFeeds == null) return null;

        List<ShoutFeedAdapterWrapper> wrappers = new ArrayList<>(shoutFeeds.size());
        for (int i = 0, size = shoutFeeds.size(); i < size; i++) {
            wrappers.add(new ShoutFeedAdapterWrapper(shoutFeeds.get(i)));
        }

        return wrappers;
    }
}