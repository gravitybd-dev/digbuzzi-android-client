package com.digbuzzi.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.digbuzzi.R;
import com.digbuzzi.Util.NavigationUtil;
import com.digbuzzi.Util.StringUtils;
import com.digbuzzi.api.RestAdapterProvider;
import com.digbuzzi.api.RestApi;
import com.digbuzzi.event.FollowUserEvent;
import com.digbuzzi.model.User;
import com.digbuzzi.model.restmodel.FollowUnfollowRequest;
import com.digbuzzi.model.restmodel.FollowUnfollowResponse;
import com.digbuzzi.model.restmodel.Leader;
import com.facebook.drawee.view.SimpleDraweeView;
import java.util.ArrayList;
import java.util.List;
import butterknife.Bind;
import butterknife.ButterKnife;
import de.greenrobot.event.EventBus;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

@SuppressWarnings("WeakerAccess")
public class FollowLeadersAdapter extends RecyclerView.Adapter {
    List<FollowLeaderWrapper> mLeaders;
    LayoutInflater mInflater;
    Context mContext;

    RestApi restApi = RestAdapterProvider.getProvider().getRestApiForRetrofit1();

    public FollowLeadersAdapter(Context context) {
        mContext = context;
        mLeaders = new ArrayList<>();
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void setLeaders(List<Leader> leaders) {
        mLeaders.clear();
        if (leaders != null)
            mLeaders.addAll(FollowLeaderAdapterHelper.getFollowLeaderWrappers(leaders));

        notifyDataSetChanged();
    }

    public void addLeaders(List<Leader> leaders) {
        if (leaders != null)
            mLeaders.addAll(FollowLeaderAdapterHelper.getFollowLeaderWrappers(leaders));

        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new FollowLeaderHolder(mInflater.inflate(R.layout.follow_person_item, parent, false));
    }

    @SuppressLint("DefaultLocale")
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final FollowLeaderWrapper followLeaderWrapper = mLeaders.get(position);
        final Leader leader = mLeaders.get(position).getLeader();
        final FollowLeaderHolder leaderHolder = (FollowLeaderHolder) holder;
        leaderHolder.ivPropic.setImageURI(Uri.parse(leader.getPhoto()));
        leaderHolder.ivPropic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NavigationUtil.openProfileActivity(mContext, leader.getUserId());
            }
        });
        leaderHolder.tvLeaderName.setText(TextUtils.isEmpty(leader.getName()) ? leader.getUserName() : leader.getName());
        leaderHolder.tvPoint.setText(String.valueOf(leader.getPoint()));

        View.OnClickListener followUnfollowListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (leader.isFollowing()) {
                    unfollow(followLeaderWrapper);
                } else {
                    follow(followLeaderWrapper);
                }

                resetVisibility(followLeaderWrapper, leaderHolder);
            }
        };

        resetVisibility(followLeaderWrapper, leaderHolder);

        leaderHolder.flFollowUnfollow.setOnClickListener(followUnfollowListener);
        leaderHolder.tvFollowersCount.setText(String.format("%d people following", leader.getFollowerCount()));
    }

    private void resetVisibility(FollowLeaderWrapper wrapper, FollowLeaderHolder leaderHolder) {
        if (wrapper.isFollowUnfollowProcessing()) {
            leaderHolder.tvFollowUnfollow.setVisibility(View.GONE);
            leaderHolder.loading.setVisibility(View.VISIBLE);
        } else {
            leaderHolder.loading.setVisibility(View.GONE);
            leaderHolder.tvFollowUnfollow.setVisibility(View.VISIBLE);
            leaderHolder.tvFollowUnfollow.setText(wrapper.getLeader().isFollowing() ? "unfollow" : "follow");
        }
    }

    @Override
    public int getItemCount() {
        return mLeaders.size();
    }

    private void follow(final FollowLeaderWrapper wrapper) {
        if (wrapper.isFollowUnfollowProcessing()) return;

        wrapper.setFollowUnfollowProcessing(true);

        final Leader leader = wrapper.getLeader();
        restApi.follow(User.getInstance().getAccessToken(), getRequest(leader), new Callback<FollowUnfollowResponse>() {
            @Override
            public void success(FollowUnfollowResponse followUnfollowResponse, Response response) {
                wrapper.setFollowUnfollowProcessing(false);

                if (response.getStatus() == 200) {
                    leader.setFollowing(true);
                    updateLeader(leader);

                    EventBus.getDefault().post(new FollowUserEvent(leader.getUserName()));
                } else {
                    showError();
                }
            }

            private void showError() {
                String name = TextUtils.isEmpty(leader.getName()) ? leader.getUserName() : leader.getName();
                Toast.makeText(mContext, "Couldn't follow " + name, Toast.LENGTH_LONG).show();
            }

            @Override
            public void failure(RetrofitError error) {
                wrapper.setFollowUnfollowProcessing(false);
                showError();
            }
        });
    }

    private void unfollow(final FollowLeaderWrapper wrapper) {
        if (wrapper.isFollowUnfollowProcessing()) return;

        wrapper.setFollowUnfollowProcessing(true);

        final Leader leader = wrapper.getLeader();
        restApi.unfollow(User.getInstance().getAccessToken(), getRequest(wrapper.getLeader()), new Callback<FollowUnfollowResponse>() {
            @Override
            public void success(FollowUnfollowResponse followUnfollowResponse, Response response) {
                wrapper.setFollowUnfollowProcessing(false);

                if (response.getStatus() == 200) {
                    leader.setFollowing(false);
                    updateLeader(leader);
                } else {
                    showError();
                }
            }

            private void showError() {
                String name = TextUtils.isEmpty(leader.getName()) ? leader.getUserName() : leader.getName();
                Toast.makeText(mContext, "Couldn't unfollow " + name, Toast.LENGTH_LONG).show();
            }

            @Override
            public void failure(RetrofitError error) {
                wrapper.setFollowUnfollowProcessing(false);
                showError();
            }
        });
    }

    private void updateLeader(Leader leader) {
        for(FollowLeaderWrapper item: mLeaders) {
            if (item.getLeader().getUserId().equals(leader.getUserId())) {
                item.getLeader().setFollowing(leader.isFollowing());
                break;
            }
        }
        notifyDataSetChanged();
    }

    private FollowUnfollowRequest getRequest(Leader leader) {
        FollowUnfollowRequest request = new FollowUnfollowRequest();
        request.setUserName(leader.getUserName());
        request.setName(leader.getName());
        request.setTime(System.currentTimeMillis());
        request.setUserId(leader.getUserId());
        request.setPhoto(leader.getPhoto());
        return request;
    }

    static class FollowLeaderHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.ivPropic)
        SimpleDraweeView ivPropic;

        @Bind(R.id.tvLeaderName)
        TextView tvLeaderName;

        @Bind(R.id.tvFollowUnfollow)
        TextView tvFollowUnfollow;

        @Bind(R.id.tvPoint)
        TextView tvPoint;

        @Bind(R.id.tvFollowersCount)
        TextView tvFollowersCount;

        @Bind(R.id.flFollowUnfollow)
        FrameLayout flFollowUnfollow;

        @Bind(R.id.loading)
        View loading;

        FollowLeaderHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}