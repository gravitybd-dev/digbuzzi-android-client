package com.digbuzzi.adapter;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import com.digbuzzi.R;
import com.digbuzzi.Util.TimeUtil;
import com.digbuzzi.model.restmodel.Comment;

/**
 * Created by U on 1/5/2016.
 */
public class CommentAdapter extends RecyclerView.Adapter {
    List<Comment> mComments;
    LayoutInflater mInflater;

    public CommentAdapter(Context context, List<Comment> comments) {
        mComments = comments;
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new CommentHolder(mInflater.inflate(R.layout.comment_item, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Comment comment = mComments.get(position);
        CommentHolder commentHolder = (CommentHolder) holder;
        commentHolder.ivPropic.setImageURI(Uri.parse(comment.getCommentor().getPhoto()));
        commentHolder.tvShoutUserName.setText(comment.getCommentor().getUserName());
        commentHolder.tvComment.setText(comment.getCommentText());
        commentHolder.tvCommentTime.setText(TimeUtil.getElapsedTime(comment.getTime()));
    }

    @Override
    public int getItemCount() {
        return mComments.size();
    }

    public static class CommentHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.ivPropic)
        SimpleDraweeView ivPropic;

        @Bind(R.id.tvShoutUserName)
        TextView tvShoutUserName;

        @Bind(R.id.tvComment)
        TextView tvComment;

        @Bind(R.id.tvCommentTime)
        TextView tvCommentTime;

        public CommentHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}