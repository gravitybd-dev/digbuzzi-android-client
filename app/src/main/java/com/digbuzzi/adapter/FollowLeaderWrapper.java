package com.digbuzzi.adapter;

import com.digbuzzi.model.restmodel.Leader;

/**
 * Created by U on 6/20/2016.
 */

public class FollowLeaderWrapper {
    private Leader leader;
    private boolean isFollowUnfollowProcessing;

    public FollowLeaderWrapper(Leader leader) {
        this.leader = leader;
    }

    public Leader getLeader() {
        return leader;
    }

    public void setFollowUnfollowProcessing(boolean followUnfollowProcessing) {
        isFollowUnfollowProcessing = followUnfollowProcessing;
    }

    public boolean isFollowUnfollowProcessing() {
        return isFollowUnfollowProcessing;
    }
}