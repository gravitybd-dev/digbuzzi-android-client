package com.digbuzzi.adapter;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import com.digbuzzi.R;
import com.digbuzzi.Util.TimeUtil;
import com.digbuzzi.model.restmodel.Like;

/**
 * Created by U on 1/5/2016.
 */
public class LikeAdapter extends RecyclerView.Adapter {
    List<Like> mLikes;
    LayoutInflater mInflater;

    public LikeAdapter(Context context, List<Like> likes) {
        mLikes = likes;
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new LikeHolder(mInflater.inflate(R.layout.like_item, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Like like = mLikes.get(position);
        LikeHolder likeHolder = (LikeHolder) holder;
        if (like.getPhoto() != null) likeHolder.ivPropic.setImageURI(Uri.parse(like.getPhoto()));
        likeHolder.tvLikerName.setText(like.getUserName());
        likeHolder.tvLikeTime.setText(TimeUtil.getElapsedTime(like.getTime()));
    }

    @Override
    public int getItemCount() {
        return mLikes.size();
    }

    public static class LikeHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.ivPropic)
        SimpleDraweeView ivPropic;

        @Bind(R.id.tvLikerName)
        TextView tvLikerName;

        @Bind(R.id.tvLikeTime)
        TextView tvLikeTime;

        public LikeHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}