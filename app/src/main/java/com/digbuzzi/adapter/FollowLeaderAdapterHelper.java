package com.digbuzzi.adapter;

import com.digbuzzi.model.restmodel.Leader;
import com.digbuzzi.model.restmodel.ShoutFeed;
import java.util.ArrayList;
import java.util.List;

class FollowLeaderAdapterHelper {
    public static FollowLeaderWrapper getFollowLeaderWrapper(Leader leader) {
        return new FollowLeaderWrapper(leader);
    }

    public static List<FollowLeaderWrapper> getFollowLeaderWrappers(List<Leader> leaders) {
        if (leaders == null) return null;

        List<FollowLeaderWrapper> wrappers = new ArrayList<>(leaders.size());
        for (int i = 0, size = leaders.size(); i < size; i++) {
            wrappers.add(new FollowLeaderWrapper(leaders.get(i)));
        }

        return wrappers;
    }
}