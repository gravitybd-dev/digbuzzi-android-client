package com.digbuzzi.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.digbuzzi.R;
import com.digbuzzi.event.PlaceOfInterestSelectionEvent;
import com.digbuzzi.model.PlaceOfInterestType;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import de.greenrobot.event.EventBus;

/**
 * Created by U on 7/20/2016.
 */

public class PlaceOfInterestTypeAdapter extends RecyclerView.Adapter<PlaceOfInterestTypeAdapter.PlaceGroupViewHolder> {
    private LayoutInflater mInflater;
    private List<PlaceOfInterestType> mPlaces;
    private Context mContext;

    public PlaceOfInterestTypeAdapter(Context context, List<PlaceOfInterestType> places) {
        mContext = context;
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mPlaces = places;
    }

    @Override
    public PlaceGroupViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new PlaceGroupViewHolder(mInflater.inflate(R.layout.place_item, parent, false));
    }

    @Override
    public void onBindViewHolder(PlaceGroupViewHolder holder, int position) {
        final PlaceOfInterestType place = mPlaces.get(position);

        String groupImageName = place.getImageName();
        String groupName = place.getPlaceName();

        holder.ivImage.setImageResource(
                getGroupImageDrawableResourceIdByImageName(groupImageName));
        holder.tvTitle.setText(groupName);
        holder.cbPlaceSelection.setChecked(place.isSelected());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (PlaceOfInterestType place : mPlaces) {
                    place.setSelected(false);
                }
                place.setSelected(true);
                notifyDataSetChanged();
                EventBus.getDefault().post(new PlaceOfInterestSelectionEvent());
            }
        });
    }

    private int getGroupImageDrawableResourceIdByImageName(String imageName) {
        return mContext.getResources().getIdentifier(imageName, "drawable", mContext.getPackageName());
    }

    @Override
    public int getItemCount() {
        return mPlaces.size();
    }

    class PlaceGroupViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.ivImage)
        ImageView ivImage;

        @Bind(R.id.tvTitle)
        TextView tvTitle;

        @Bind(R.id.cbPlaceSelection)
        RadioButton cbPlaceSelection;

        public PlaceGroupViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
