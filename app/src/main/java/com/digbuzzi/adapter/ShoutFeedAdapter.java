package com.digbuzzi.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.ShareCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;
import com.digbuzzi.R;
import com.digbuzzi.Util.NavigationUtil;
import com.digbuzzi.Util.TimeUtil;
import com.digbuzzi.api.RestAdapterProvider;
import com.digbuzzi.api.RestApi;
import com.digbuzzi.event.BuzzUpdated;
import com.digbuzzi.model.User;
import com.digbuzzi.model.restmodel.Like;
import com.digbuzzi.model.restmodel.LikeDislikeRequest;
import com.digbuzzi.model.restmodel.ShoutFeed;
import com.digbuzzi.ui.CommentActivity;
import com.digbuzzi.ui.LikerActivity;
import com.facebook.drawee.view.SimpleDraweeView;
import java.util.ArrayList;
import java.util.List;
import butterknife.Bind;
import butterknife.ButterKnife;
import de.greenrobot.event.EventBus;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by anwarshahriar on 1/4/2016.
 * Adapter for buzz feeds
 */
public class ShoutFeedAdapter extends RecyclerView.Adapter {
    private Context mContext;
    private List<ShoutFeedAdapterWrapper> mFeeds;
    private LayoutInflater inflater;
    private RestApi restApi;

    public ShoutFeedAdapter(Context context) {
        mContext = context;
        mFeeds = new ArrayList<>();
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        restApi = RestAdapterProvider.getProvider().getRestApiForRetrofit1();
    }

    public void setFeeds(List<ShoutFeed> feeds) {
        mFeeds.clear();
        if (feeds != null)
            mFeeds.addAll(ShoutFeedHelper.getShoutFeedWrappers(feeds));
    }

    @SuppressWarnings("unused")
    public void addFeeds(List<ShoutFeed> feeds) {
        if (feeds != null)
            mFeeds.addAll(ShoutFeedHelper.getShoutFeedWrappers(feeds));
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new Holder(inflater.inflate(R.layout.shout_feed_item, parent, false));
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        final Holder viewHolder = (Holder) holder;
        final ShoutFeedAdapterWrapper shoutFeedAdapterWrapper = mFeeds.get(position);
        final ShoutFeed feed = shoutFeedAdapterWrapper.getShoutFeed();
        viewHolder.topIndicator.setBackgroundResource(getTrafficConditionColorResource(feed.getTrafficCondition()));
        viewHolder.ivPropic.setImageURI(Uri.parse(feed.getPhoto()));
        viewHolder.ivPropic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavigationUtil.openProfileActivity(mContext, feed.getUserId());
            }
        });
        viewHolder.tvShoutUserName.setText(feed.getUserName());
        viewHolder.tvShoutUserName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavigationUtil.openProfileActivity(mContext, feed.getUserId());
            }
        });
        viewHolder.tvShoutUserLocation.setText(feed.getLocation().getPlace());
        viewHolder.tvShout.setText(feed.getShoutText());
        viewHolder.tvShoutTime.setText(TimeUtil.getElapsedTime(feed.getTime()));

        List<String> attachments = feed.getAttachments();
        if (attachments != null && attachments.size() > 0) {
            viewHolder.ivPhoto.setVisibility(View.VISIBLE);
            viewHolder.ivPhoto.setImageURI(Uri.parse(attachments.get(0)));
        } else {
            viewHolder.ivPhoto.setVisibility(View.GONE);
        }

        viewHolder.ivLike.setAlpha(feed.isLikedByUser() ? 1.0f : 0.5f);

        resetVisibility(shoutFeedAdapterWrapper, viewHolder);

        viewHolder.ivLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                if (shoutFeedAdapterWrapper.isLikeProcessing()) return;

                LikeDislikeRequest likeDislikeRequest = new LikeDislikeRequest();
                likeDislikeRequest.id = feed.getShoutId();
                likeDislikeRequest.time = System.currentTimeMillis();

                shoutFeedAdapterWrapper.setLikeProcessing(true);
                resetVisibility(shoutFeedAdapterWrapper, viewHolder);

                viewHolder.ivLike.setVisibility(shoutFeedAdapterWrapper.isLikeProcessing() ? View.GONE : View.VISIBLE);
                viewHolder.likeLoading.setVisibility(shoutFeedAdapterWrapper.isLikeProcessing() ? View.VISIBLE : View.GONE);

                restApi.likeDislike(User.getInstance().getAccessToken(), feed.getShoutId(), likeDislikeRequest, new Callback<Like>() {
                    @Override
                    public void success(Like like, Response response) {
                        shoutFeedAdapterWrapper.setLikeProcessing(false);
                        resetVisibility(shoutFeedAdapterWrapper, viewHolder);

                        if (feed.isLikedByUser()) {
                            feed.setLikeCount(feed.getLikeCount() - 1);
                        } else {
                            feed.setLikeCount(feed.getLikeCount() + 1);
                        }

                        feed.setLikedByUser(!feed.isLikedByUser());

                        EventBus.getDefault().post(new BuzzUpdated(feed.getShoutId()));

                        notifyDataSetChanged();
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        shoutFeedAdapterWrapper.setLikeProcessing(false);
                        resetVisibility(shoutFeedAdapterWrapper, viewHolder);
                        String msg;
                        if (feed.isLikedByUser()) {
                            msg = "couldn't dislike the buzz";
                        } else {
                            msg = "couldn't like the buzz";
                        }
                        Toast.makeText(mContext, msg, Toast.LENGTH_LONG).show();
                        Log.d(ShoutFeedAdapter.class.getSimpleName(), error.getMessage());
                    }
                });
            }
        });
        viewHolder.tvLikeCount.setText("+" + feed.getLikeCount());
        viewHolder.tvLikeCount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, LikerActivity.class);
                intent.putExtra(LikerActivity.EXTRA_LIKE_TYPE, LikerActivity.LIKE_TYPE_BUZZ);
                intent.putExtra("buzzId", feed.getShoutId());
                mContext.startActivity(intent);
            }
        });

        viewHolder.tvCommentCount.setText(String.valueOf(feed.getCommentCount()));
        viewHolder.tvCommentCount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, CommentActivity.class);
                intent.putExtra(CommentActivity.EXTRA_COMMENT_TYPE, CommentActivity.COMMENT_TYPE_BUZZ);
                intent.putExtra("buzzId", feed.getShoutId());
                mContext.startActivity(intent);
            }
        });
        viewHolder.tvShareCount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = ShareCompat.IntentBuilder
                        .from((Activity)mContext)
                        .setType("text/plain")
                        .setText(feed.getSharableLink())
                        .setChooserTitle("Share link using").getIntent();
                mContext.startActivity(Intent.createChooser(intent, "Share link using"));
            }
        });
    }

    private void resetVisibility(ShoutFeedAdapterWrapper shoutFeedAdapterWrapper, Holder viewHolder) {
        viewHolder.ivLike.setVisibility(shoutFeedAdapterWrapper.isLikeProcessing() ? View.INVISIBLE : View.VISIBLE);
        viewHolder.likeLoading.setVisibility(shoutFeedAdapterWrapper.isLikeProcessing() ? View.VISIBLE : View.GONE);
    }

    private int getTrafficConditionColorResource(String trafficCondition) {
        if (trafficCondition.equalsIgnoreCase("HIGH")) {
            return R.mipmap.ic_high_traffic;
        }

        if (trafficCondition.equalsIgnoreCase("MEDIUM")) {
            return R.mipmap.ic_medium_traffic;
        }

        if (trafficCondition.equalsIgnoreCase("LOW")) {
            return R.mipmap.ic_low_traffic;
        }

        return 0;
    }

    @Override
    public int getItemCount() {
        return mFeeds.size();
    }

    @SuppressWarnings("WeakerAccess")
    public class Holder extends RecyclerView.ViewHolder {

        @Bind(R.id.topIndicator)
        View topIndicator;

        @Bind(R.id.ivPropic)
        SimpleDraweeView ivPropic;

        @Bind(R.id.tvShoutUserName)
        TextView tvShoutUserName;

        @Bind(R.id.tvShoutUserLocation)
        TextView tvShoutUserLocation;

        @Bind(R.id.tvShoutTime)
        TextView tvShoutTime;

        @Bind(R.id.tvShout)
        TextView tvShout;

        @Bind(R.id.ivPhoto)
        SimpleDraweeView ivPhoto;

        @Bind(R.id.ivLike)
        View ivLike;

        @Bind(R.id.likeLoading)
        View likeLoading;

        @Bind(R.id.tvLikeCount)
        TextView tvLikeCount;

        @Bind(R.id.tvCommentCount)
        TextView tvCommentCount;

        @Bind(R.id.tvShareCount)
        TextView tvShareCount;

        Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}