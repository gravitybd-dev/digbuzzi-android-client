package com.digbuzzi.adapter;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import com.digbuzzi.R;
import com.digbuzzi.Util.NavigationUtil;
import com.digbuzzi.Util.TimeUtil;
import com.digbuzzi.model.restmodel.FollowPersonModel;

/**
 * Created by U on 1/22/2016.
 */
public class FollowerFollowingAdapter extends RecyclerView.Adapter {
    List<FollowPersonModel> mModels;
    LayoutInflater mInflater;
    Context mContext;

    public FollowerFollowingAdapter(Context context, List<FollowPersonModel> models) {
        mContext = context;
        mModels = models;
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new LikeHolder(mInflater.inflate(R.layout.like_item, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final FollowPersonModel model = mModels.get(position);
        LikeHolder likeHolder = (LikeHolder) holder;
        if (model.getPhoto() != null) likeHolder.ivPropic.setImageURI(Uri.parse(model.getPhoto()));

        View.OnClickListener profileListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NavigationUtil.openProfileActivity(mContext, model.getUserId());
            }
        };

        likeHolder.tvLikerName.setText(model.getUserName());
        likeHolder.tvLikeTime.setText(TimeUtil.getElapsedTime(model.getTime()));

        likeHolder.tvLikerName.setOnClickListener(profileListener);
        likeHolder.ivPropic.setOnClickListener(profileListener);
    }

    @Override
    public int getItemCount() {
        return mModels.size();
    }

    public static class LikeHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.ivPropic)
        SimpleDraweeView ivPropic;

        @Bind(R.id.tvLikerName)
        TextView tvLikerName;

        @Bind(R.id.tvLikeTime)
        TextView tvLikeTime;

        public LikeHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
