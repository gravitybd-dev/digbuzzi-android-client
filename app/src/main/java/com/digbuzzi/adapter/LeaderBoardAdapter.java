package com.digbuzzi.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import com.digbuzzi.R;
import com.digbuzzi.Util.StringUtils;
import com.digbuzzi.model.restmodel.Leader;
import com.digbuzzi.ui.ProfileActivity;

/**
 * Created by U on 1/5/2016.
 */
public class LeaderBoardAdapter extends RecyclerView.Adapter {
    List<Leader> mLeaders;
    LayoutInflater mInflater;

    Context mContext;

    public LeaderBoardAdapter(Context context, List<Leader> leaders) {
        mContext = context;
        mLeaders = leaders;
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new LeaderHolder(mInflater.inflate(R.layout.leaderboard_leader_item, parent, false));
    }

    @SuppressLint("DefaultLocale")
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final Leader leader = mLeaders.get(position);
        LeaderHolder leaderHolder = (LeaderHolder) holder;
        leaderHolder.ivPropic.setImageURI(Uri.parse(leader.getPhoto()));
        leaderHolder.tvRank.setText(StringUtils.ordinal(leader.getRank()));
        leaderHolder.tvLeaderName.setText(TextUtils.isEmpty(leader.getName()) ? leader.getUserName() : leader.getName());
        leaderHolder.tvLeaderPoint.setText(String.format("%d", leader.getPoint()));

        View.OnClickListener profileListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, ProfileActivity.class);
                intent.putExtra(ProfileActivity.KEY_USER_ID, leader.getUserId());
                mContext.startActivity(intent);
            }
        };

        leaderHolder.ivPropic.setOnClickListener(profileListener);
        leaderHolder.tvLeaderName.setOnClickListener(profileListener);
    }

    @Override
    public int getItemCount() {
        return mLeaders.size();
    }

    public static class LeaderHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.ivPropic)
        SimpleDraweeView ivPropic;

        @Bind(R.id.tvRank)
        TextView tvRank;

        @Bind(R.id.tvLeaderName)
        TextView tvLeaderName;

        @Bind(R.id.tvLeaderPoint)
        TextView tvLeaderPoint;

        public LeaderHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}