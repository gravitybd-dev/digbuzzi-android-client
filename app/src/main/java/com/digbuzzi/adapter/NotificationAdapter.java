package com.digbuzzi.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.digbuzzi.ui.ProfileActivity;
import com.digbuzzi.ui.SingleBuzzActivity;
import com.facebook.drawee.view.SimpleDraweeView;
import java.util.List;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.digbuzzi.R;
import com.digbuzzi.Util.TimeUtil;
import com.digbuzzi.model.restmodel.Notification;

public class NotificationAdapter extends RecyclerView.Adapter {
    List<Notification> mNotifications;
    LayoutInflater mInflater;
    Context mContext;

    public NotificationAdapter(Context context, List<Notification> notifications) {
        mContext = context;
        mNotifications = notifications;
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new NotificationHolder(mInflater.inflate(R.layout.notification_item, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final Notification notification = mNotifications.get(position);
        NotificationHolder notificationHolder = (NotificationHolder) holder;
        notificationHolder.ivPropic.setImageURI(Uri.parse(notification.participentAvatar));
        notificationHolder.tvText.setText(notification.text);
        notificationHolder.tvTime.setText(TimeUtil.getElapsedTime(notification.time));
        notificationHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent;
                if (notification.type.equals("NEWFOLLOWING")) {
                    intent = new Intent(mContext, ProfileActivity.class);
                    intent.putExtra(ProfileActivity.KEY_USER_ID, notification.participantUserId);
                } else {
                    intent = new Intent(mContext, SingleBuzzActivity.class);
                    intent.putExtra(SingleBuzzActivity.BUZZ_ID, notification.shoutId);
                }
                mContext.startActivity(intent);
            }
        });
        notificationHolder.ivPropic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, ProfileActivity.class);
                intent.putExtra(ProfileActivity.KEY_USER_ID, notification.participantUserId);
                mContext.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mNotifications.size();
    }

    public static class NotificationHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.ivPropic)
        SimpleDraweeView ivPropic;

        @Bind(R.id.tvText)
        TextView tvText;

        @Bind(R.id.tvTime)
        TextView tvTime;

        public NotificationHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}