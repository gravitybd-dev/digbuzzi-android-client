package com.digbuzzi.adapter;

import com.digbuzzi.model.restmodel.ShoutFeed;

/**
 * Created by anwarshahriar on 5/13/16.
 */
class ShoutFeedAdapterWrapper {
    private ShoutFeed shoutFeed;
    private boolean likeProcessing;

    public ShoutFeedAdapterWrapper(ShoutFeed shoutFeed) {
        this.shoutFeed = shoutFeed;
    }

    public ShoutFeed getShoutFeed() {
        return shoutFeed;
    }

    public boolean isLikeProcessing() {
        return likeProcessing;
    }

    public void setLikeProcessing(boolean likeProcessing) {
        this.likeProcessing = likeProcessing;
    }
}