package com.digbuzzi.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.digbuzzi.R;
import com.digbuzzi.event.PlaceOfInterestGroupSelectedEvent;
import com.digbuzzi.model.PlaceOfInterestGroup;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import de.greenrobot.event.EventBus;

/**
 * Created by U on 7/20/2016.
 */

public class PlaceGroupAdapter extends RecyclerView.Adapter<PlaceGroupAdapter.PlaceGroupViewHolder> {
    private LayoutInflater mInflater;
    private List<PlaceOfInterestGroup> mGroups;
    private Context mContext;

    public PlaceGroupAdapter(Context context, List<PlaceOfInterestGroup> groups) {
        mContext = context;
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mGroups = groups;
    }

    @Override
    public PlaceGroupViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new PlaceGroupViewHolder(mInflater.inflate(R.layout.place_group_item, parent, false));
    }

    @Override
    public void onBindViewHolder(PlaceGroupViewHolder holder, int position) {
        final PlaceOfInterestGroup group = mGroups.get(position);

        String groupImageName = group.getImageName();
        String groupName = group.getPlaceGroupName();

        holder.ivImage.setImageResource(
                getGroupImageDrawableResourceIdByImageName(groupImageName));
        holder.tvTitle.setText(groupName);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EventBus.getDefault().post(new PlaceOfInterestGroupSelectedEvent(group));
            }
        });
    }

    private int getGroupImageDrawableResourceIdByImageName(String imageName) {
        return mContext.getResources().getIdentifier(imageName, "drawable", mContext.getPackageName());
    }

    @Override
    public int getItemCount() {
        return mGroups.size();
    }

    class PlaceGroupViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.ivImage)
        ImageView ivImage;

        @Bind(R.id.tvTitle)
        TextView tvTitle;

        public PlaceGroupViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
