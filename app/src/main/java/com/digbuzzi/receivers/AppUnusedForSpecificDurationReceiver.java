package com.digbuzzi.receivers;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.app.NotificationCompat;

import com.digbuzzi.PrimaryNavigatorActivity;
import com.digbuzzi.R;
import com.digbuzzi.ui.NavDrawerActivity;

public class AppUnusedForSpecificDurationReceiver extends BroadcastReceiver {
    public AppUnusedForSpecificDurationReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent i) {
        Intent intent = new Intent(context, PrimaryNavigatorActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(context,
                0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context)
                .setSmallIcon(R.mipmap.ic_small_icon_notification)
                .setLargeIcon(bitmap)
                .setContentTitle(context.getString(R.string.are_you_okay))
                .setContentText(context.getString(R.string.app_unused_message_for_certain_period))
                .setAutoCancel(true)
                .setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(context.getString(R.string.app_unused_message_for_certain_period)))
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(999, notificationBuilder.build());
    }
}
